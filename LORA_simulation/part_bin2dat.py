# Converts binary CORSIKA thinned particle-data file into ASCII format.
# Syntax: python part_bin2dat.py <infile> <outfile>

# Replicates functionality of test_With_Thining.cxx, obtained from Anne Zilles (Mar 2017), originally provided by Stijn Buitink?  Core functionality has been placed in partdatlib.py.

# Justin Bray, Feb 2018


from partdatlib import readpartdat
from numpy import *
import sys

assert len(sys.argv) == 3, 'Must specify input and output files.'
infile = sys.argv[1]
outfile = sys.argv[2]


header, cid, pid, p, xy, t, wt = readpartdat(infile)

print 'Event No.=%d' % header['event_num']
print 'Primary particle ID=%d' % header['part_id']
print 'Energy=%e eV' % (header['energy']*1e9)
print 'Zenith=%f radian = %f deg' % (header['zenith'], header['zenith']*180/pi)
print 'Azimuth=%f radian = %f deg' % (header['azimuth'], header['azimuth']*180/pi)


outfile = open(outfile, 'w')

outfile.write('%5d\t%4d\t%e\t%e\t%e\n' % (header['event_num'], header['part_id'], header['energy'], header['zenith'], header['azimuth']))

for i in range(len(cid)):
  outfile.write('%5d\t%4d\t%e\t%e\t%e\t%e\t%e\t%5d\t%e\n' % (cid[i], pid[i], p[i,0], p[i,1], p[i,2], xy[i,0], xy[i,1], t[i], wt[i]))

outfile.close()
