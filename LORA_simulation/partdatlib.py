# Functions for reading particle data output files from CORSIKA when run with thinning.

# Primary function for external use is readpartdat(filename).

# Data files are organised into blocks, which are divided into rows, each of which contains either a header/footer or descriptions for multiple particles.
# Blocks consist of 21 rows, with a 1-word (4B) header at start and end.  Rows consist of 312 words (of 4B each).  Each particle description is contained in 8 words (of 4B each).

# Based on test_With_Thining.cxx, obtained from Anne Zilles (Mar 2017), originally provided by Stijn Buitink?  Ported from C++ to Python, and substantially rewritten.

# Justin Bray, Feb 2018


from numpy import *
import struct
import sys



def readblock(infile):
  infile.read(4) # padding
  block = infile.read(21*312*4)
  infile.read(4) # padding
  assert len(block) in [0, 21*312*4], 'Should read complete block or nothing.'
  return block


def readblocks(infile):
  blocks = []
  block = readblock(infile)
  while block:
    blocks.append(block)
    block = readblock(infile)
  return blocks


def blocks2rows(blocks):
  rows = []
  for block in blocks:
    for irow in range(21):
      rows.append( block[irow*312*4:(irow+1)*312*4] )
  return rows


# Parse header row.
def parse_header(row):
  assert not len(row)%4
  words = [row[4*i:4*i+4] for i in range(len(row)/4)]

  header = {}
  header['event_num'] = int(struct.unpack('f', words[1])[0])
  header['part_id']   = int(struct.unpack('f', words[2])[0])
  header['energy']    = struct.unpack('f', words[3])[0] # GeV
  header['zenith']    = struct.unpack('f', words[10])[0] # rad
  header['azimuth']   = struct.unpack('f', words[11])[0] # rad

  return header



def readpartdat(infile):
  # Parse file as blocks.
  infile = open(infile, "rb")
  blocks = readblocks(infile)
  infile.close()

  # Parse blocks into rows (subblocks).
  rows = blocks2rows(blocks)

  # Strip final empty rows.
  while len(rows) and rows[-1][:4] == '\0\0\0\0':
    rows = rows[:-1]
  # Strip run header and footer.
  assert rows[0][:4] == 'RUNH' and rows[-1][:4] == 'RUNE'
  rows = rows[1:-1]
  # Strip event footer.
  assert rows[0][:4] == 'EVTH' and rows[-1][:4] == 'EVTE'
  header = parse_header(rows[0])
  rows = rows[1:-1]

  # Merge rows into floating-point data.
  rows = [fromstring(row, dtype=float32) for row in rows]
  data = concatenate(rows)
  assert not len(data) % 8 # 8 values per entry
  data = reshape(data, [len(data)/8,8])

  # Select entries on the basis of initial values.
  vals = data[:,0].astype(int)
  # Rows with values in the range [75000,77000] contain superfluous muon data.
  inds = where((vals < 75000) + (77000 < vals))[0]
  data = data[inds,:] # exclude them

  # Strip final empty entries.
  while len(data) and not int(data[-1,0]):
    data = data[:-1,:]

  cid = data[:,0].astype(int) # ID allocated by CORSIKA
  pid = data[:,0].astype(int) / 1000 # particle-type ID (see CORSIKA user manual; table 4)
  p = data[:,1:4] # momenta in x/y/z; units of GeV/c
  xy = data[:,4:6] # position in x/y; units of cm
  t = data[:,6] # time since first interaction; units of ns
  wt = data[:,7] # particle weights (from thinning)

  return header, cid, pid, p, xy, t, wt
