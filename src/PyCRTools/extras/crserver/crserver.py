"""
Serve CR events dynamically from database.
"""

import os
import numpy as np
import re
import sqlite3
import SocketServer
import SimpleHTTPServer
import matplotlib
import sys
import traceback
import httplib
import urllib

matplotlib.use("Agg")
import matplotlib.pyplot as plt

from pycrtools.crdatabase import unpickle_parameter

try:
    import psycopg2
    have_psycopg2 = True
except ImportError:
    have_psycopg2 = False

from datetime import datetime
from optparse import OptionParser
from xml.etree.cElementTree import ElementTree, Element, SubElement, ProcessingInstruction
from StringIO import StringIO

# Parse commandline options
parser = OptionParser()
parser.add_option("-p", "--port", default = 8000, type = "int", help="port to bind to")
parser.add_option("-n", "--hostname", default = "localhost", help = "hostname or IP of server")
parser.add_option("-d", "--database", default = "crdb.sqlite", help = "filename of database")
parser.add_option("--host", default=None, help="PostgreSQL host.")
parser.add_option("--user", default=None, help="PostgreSQL user.")
parser.add_option("--password", default=None, help="PostgreSQL password.")
parser.add_option("--dbname", default=None, help="PostgreSQL dbname.")

(options, args) = parser.parse_args()

def get_weather_station_efield(year, month, day, hour, minute, second, store_as="weather_station_efield.png", elfield_dir="./"):
    
    filename = '{0}-{1:02d}-{2:02d}-CR1000_EFM1sec.dat'.format(year, month, day)
    combined = os.path.join(elfield_dir, filename)
    
    if not os.path.exists(combined):
        urllib.urlretrieve('ftp://ftp.astron.nl/outgoing/Cosmic/{0}/{0}-{1:02d}-{2:02d}-CR1000_EFM1sec.dat'.format(year, month, day), combined)

    f = open(combined, "r")
    
    timestamp = []
    seconds = []
    value = []
    
    start = datetime(year, month, day, 0, 0, 0)
    mark = datetime(year, month, day, hour, minute, second)
    
    for line in f:
        
        l = line.split(",")
        
        try:
            d = datetime.strptime(l[0].strip('"'), '%Y-%m-%d %H:%M:%S')
        except ValueError:
            continue
    
        timestamp.append(d)

        delta = d - start
        seconds.append(delta.total_seconds())

        value.append(float(l[2]))
    
    dm = mark - start
    dms = dm.total_seconds() / 3600.
    
    vmin = 1.1 * np.min(value)
    vmax = 1.1 * np.max(value)
    
    plt.clf()
    plt.plot(np.asarray(seconds) / 3600., value)
    plt.plot((dms, dms), (vmin, vmax), 'r-')
    plt.xlim(0, 24)
    plt.ylim(vmin, vmax)
    plt.xlabel(r"$t\, [h]$")
    plt.ylabel(r"$E\, [V\cdot m^{-1}]$")
    plt.xticks(np.arange(0, 24+1, 2.0))
    plt.savefig(store_as)



def get_knmi_thunderstorm_picture(year, month, day, store_as = "knmi_thunderstorm.png"):

    params = urllib.urlencode({'language' : 'nl',
              'year' : '{0}'.format(year),
              'month' : '{0}'.format(month),
              'day' : '{0}'.format(day),
              'submit' : 'toon'})
    
    headers = {"Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
               "Accept-Encoding" : "gzip, deflate",
               "Accept-Language" : "en-US,en;q=0.5",
               "Connection" : "keep-alive",
               "DNT" : "1",
               "Host" : "www.knmi.nl",
               "Referer" : "http://www.knmi.nl/klimatologie/daggegevens/onweer/index.cgi",
               "User-Agent" : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:25.0) Gecko/20100101 Firefox/25.0"}
    
    conn = httplib.HTTPConnection("knmi.nl:80")
    
    conn.request("POST", "/klimatologie/daggegevens/onweer/index.cgi",
                 params, headers)
    
    response = conn.getresponse()
    
    data = response.read()
    
    conn.close()
    
    m = re.search('(20.*\.png)', data)
    if m:
        urllib.urlretrieve ("http://www.knmi.nl/klimatologie/daggegevens/onweer/data2/{0}".format(m.group(1)), store_as)
    else:
        urllib.urlretrieve ("http://www.knmi.nl/klimatologie/daggegevens/onweer/data2/empty.png", store_as)
        
def good_lora_reconstruction(core_x, core_y, moliere, elevation):
    """
    """

    quality = False

    try:
        if np.sqrt(core_x**2 + core_y**2) < 150:
            if moliere < 100 and moliere > 20:
                if elevation > 55:
                    quality = True
    except:
        pass

    return quality

def event_header(cursor, eventID, station=None, polarization=None, datafile=None):
    """
    """

    header = Element("header")

    # Get event header
    sql = "SELECT timestamp, antennaset, status, alt_status, statusmessage, alt_statusmessage, simulation_status, simulation_statusmessage, flagged, flagged_reason FROM events WHERE eventID={0}".format(eventID)
    cursor.execute(sql)

    e = cursor.fetchone()
    SubElement(header, "id").text = str(eventID)
    SubElement(header, "timestamp").text = str(datetime.utcfromtimestamp(e[0]))
    SubElement(header, "antennaset").text = str(e[1])
    SubElement(header, "status").text = str(e[2])
    SubElement(header, "alt_status").text = str(e[3])
    SubElement(header, "statusmessage").text = str(e[4])
    SubElement(header, "alt_statusmessage").text = str(e[5])
    SubElement(header, "simulation_status").text = str(e[6])
    SubElement(header, "simulation_statusmessage").text = str(e[7])
    SubElement(header, "flagged").text = str(e[7])
    SubElement(header, "flagged_reason").text = str(e[8])

    if station:
        s = SubElement(header, "station")
        SubElement(s, "name").text = str(station)

        sql = """SELECT s.status, s.alt_status, s.statusmessage, s.alt_statusmessage FROM
        event_datafile AS ed INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
        INNER JOIN stations AS s ON (ds.stationID=s.stationID)
        WHERE (eventID={0} AND stationName='{1}')
        """.format(eventID, station)
        cursor.execute(sql)

        e = cursor.fetchone()

        SubElement(s, "status").text = e[0]
        SubElement(s, "alt_status").text = e[1]
        SubElement(s, "statusmessage").text = e[2]
        SubElement(s, "alt_statusmessage").text = e[3]

    if polarization:
        p = SubElement(header, "polarization")
        SubElement(p, "name").text = str(polarization)

        sql = """SELECT p.status, p.alt_status, p.statusmessage, p.alt_statusmessage FROM
        event_datafile AS ed INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
        INNER JOIN stations AS s ON (ds.stationID=s.stationID)
        INNER JOIN station_polarization AS sp ON (ds.stationID=sp.stationID)
        INNER JOIN polarizations AS p ON (sp.polarizationID=p.polarizationID)
        WHERE (ed.eventID={0} AND s.stationname='{1}' AND p.direction='{2}')""".format(eventID, station, polarization)
        cursor.execute(sql)

        e = cursor.fetchone()

        SubElement(p, "status").text = e[0]
        SubElement(p, "alt_status").text = e[1]
        SubElement(p, "statusmessage").text = e[2]
        SubElement(p, "alt_statusmessage").text = e[3]

    # Get all stations in event
    sql = """SELECT s.stationname, s.status, s.alt_status FROM
    event_datafile AS ed INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
    INNER JOIN stations AS s ON (ds.stationID=s.stationID)
    WHERE eventID={0}
    """.format(eventID)
    cursor.execute(sql)

    stations = SubElement(header, "stations")
    for e in cursor.fetchall():
        s = SubElement(stations, "station")
        SubElement(s, "name").text = e[0]
        if e[0] == station:
            s.set("current", "true")
        SubElement(s, "status").text = e[1]
        SubElement(s, "alt_status").text = e[2]

        # Get all polarizations for this station
        sql = """SELECT p.direction, p.status, p.alt_status FROM
        event_datafile AS ed INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
        INNER JOIN stations AS s ON (ds.stationID=s.stationID)
        INNER JOIN station_polarization AS sp ON (ds.stationID=sp.stationID)
        INNER JOIN polarizations AS p ON (sp.polarizationID=p.polarizationID)
        WHERE (ed.eventID={0} AND s.stationname='{1}')""".format(eventID, e[0])
        cursor.execute(sql)

        polarizations = SubElement(s, "polarizations")
        for k in cursor.fetchall():
            p = SubElement(polarizations, "polarization")
            SubElement(p, "name").text = k[0]
            SubElement(p, "status").text = k[1]
            SubElement(p, "alt_status").text = k[2]

    # Get all datafiles in event
    sql = """SELECT filename FROM
    event_datafile AS ed INNER JOIN datafiles AS df ON (ed.datafileID=df.datafileID)
    WHERE ed.eventID={0}""".format(eventID)
    cursor.execute(sql)

    datafiles = SubElement(header, "datafiles")
    for e in cursor.fetchall():
        d = SubElement(datafiles, "datafile")
        SubElement(d, "name").text = e[0]

    return header

def statistics_handler():
    """Handle statistics overview page.
    """

    # Connect to database
    if have_psycopg2 and options.host:
        # Open PostgreSQL database
        print "Opening connection to PostgreSQL database"
        conn = psycopg2.connect(host=options.host, user=options.user, password=options.password, dbname=options.dbname)
    else:
        # Open SQLite database
        print "Opening Sqlite database"
        conn = sqlite3.connect(options.database, timeout=60.0)

    # Create cursor
    c = conn.cursor()

    # Create temporary directory for statistics plot if needed
    statdir = "statistics"
    if not os.path.exists(statdir):
        os.makedirs(statdir)

    # Generate empty XML
    elements = Element("elements")

    # status
    if True: # only for indent clarity, perhaps later turned into a function

        info = SubElement(elements, "info")
        SubElement(info, "caption").text = "Event status"
        data = SubElement(info, "data")

        # Get total number of events
        c.execute("""SELECT COUNT(status) FROM events""")
        nof_events = c.fetchone()[0]

        # Get status count
        c.execute("""SELECT status, COUNT(*) FROM events GROUP BY status""")

        fraction = []
        labels = []
        for e in c.fetchall():
            fraction.append(float(e[1]) / nof_events)
            labels.append("{0} {1}={2:.1f}%".format(e[0], e[1], 100 * float(e[1]) / nof_events))

            record = SubElement(data, "record")
            SubElement(record, "key").text = e[0]
            SubElement(record, "value").text = str(e[1])

        total = SubElement(data, "total").text = str(nof_events)

        fig = plt.figure()
        fig.add_subplot(111, aspect='equal')

        plt.pie(fraction, labels=labels, shadow=True)

        figname = statdir+"/event_status_pie.png"
        fig.savefig(figname)

        graph = SubElement(info, "graph")
        SubElement(graph, "path").text = figname

    # Histograms of total event count over time
    if True:
        info = SubElement(elements, "info")
        SubElement(info, "caption").text = "Histogram of ALL events over time"
        data = SubElement(info, "data")

        c.execute("""SELECT eventid FROM events""")
        events = c.fetchall()
        full_eventlist = [int(id[0]) for id in events]
        full_eventlist.sort()
        full_eventlist = np.array(full_eventlist)
        full_eventtimes = (2010.0 + full_eventlist / (24*3600*365.25)) # ignore nitpicking details of leap years

        first_year = np.floor(np.min(full_eventtimes))
        latest_year = 1 + np.floor(np.max(full_eventtimes)) # for final bin
        bins_per_year = 4
        histo_bins = np.linspace(first_year, latest_year, 1 + (latest_year - first_year) * bins_per_year)
        
        #fig, ax = plt.subplots(1, 2)
        plt.figure()
        hist_total_events, bin_edges = np.histogram(full_eventtimes, bins=histo_bins)
        width = histo_bins[1] - histo_bins[0]
        plt.bar(histo_bins[:-1], hist_total_events, width=width, align='edge')
        #plt.hist(full_eventtimes, bins=histo_bins)
        plt.xlabel('Event time [ year ]')
        plt.ylabel('# events')
        ticks = np.linspace(first_year, latest_year, 1 + (latest_year - first_year))
        plt.xticks(ticks)
        
        figname = statdir+"/histogram_all_events.png"
        plt.savefig(figname, dpi=300)
        fignameBOE = statdir+"/histogram_all_events_BOE.png"
        plt.savefig(figname, dpi=300)
        
        graph = SubElement(info, "graph")
        SubElement(graph, "path").text = figname
        graph = SubElement(info, "graph")
        SubElement(graph, "path").text = fignameBOE


    # Histograms of percentage of events with each status, over time
    if True:
        c.execute("""SELECT status FROM events""")
        all_status = c.fetchall()
        all_status = np.array([id[0] for id in all_status])
        status_list = np.unique(all_status)


        for status in status_list:
            info = SubElement(elements, "info")
            SubElement(info, "caption").text = "Percentage of %s events over time" % status
            data = SubElement(info, "data")

            # Get event list for events with this status
            c.execute("SELECT eventid FROM events WHERE status='%s'" % status)
            events_with_status = c.fetchall()
            eventlist_status = [int(id[0]) for id in events_with_status]
            eventlist_status.sort()
            eventlist_status = np.array(eventlist_status)
            eventtimes_status = (2010.0 + eventlist_status / (24*3600*365.25))
            print 'There are %d events with status %s' % (len(eventlist_status), status)

            plt.figure()
            hist_status_events, bin_edges = np.histogram(eventtimes_status, bins=histo_bins)
            #width = histo_bins[1] - histo_bins[0]
            plt.bar(histo_bins[:-1], 100.0 * hist_status_events.astype('float') / hist_total_events, width=width, align='edge')
            #plt.hist(full_eventtimes, bins=histo_bins)
            plt.xlabel('Event time [ year ]')
            plt.ylabel('Percentage of events %s [ %% ]' % status)
            #ticks = np.linspace(first_year, latest_year, 1 + (latest_year - first_year))
            plt.xticks(ticks)

            figname = statdir+"/histogram_%s_events.png" % status
            plt.savefig(figname)

            graph = SubElement(info, "graph")
            SubElement(graph, "path").text = figname



    # alt_status
    if True: # only for indent clarity, perhaps later turned into a function

        info = SubElement(elements, "info")
        SubElement(info, "caption").text = "Event status (alternative)"
        data = SubElement(info, "data")

        # Get total number of events
        c.execute("""SELECT COUNT(alt_status) FROM events""")
        nof_events = c.fetchone()[0]

        # Get status count
        c.execute("""SELECT alt_status, COUNT(*) FROM events GROUP BY alt_status""")

        fraction = []
        labels = []
        for e in c.fetchall():
            fraction.append(float(e[1]) / nof_events)
            labels.append("{0} {1}={2:.1f}%".format(e[0], e[1], 100 * float(e[1]) / nof_events))

            record = SubElement(data, "record")
            SubElement(record, "key").text = e[0]
            SubElement(record, "value").text = str(e[1])

        total = SubElement(data, "total").text = str(nof_events)

        fig = plt.figure()
        fig.add_subplot(111, aspect='equal')

        plt.pie(fraction, labels=labels, shadow=True)

        figname = statdir+"/event_alt_status_pie.png"
        fig.savefig(figname)

        graph = SubElement(info, "graph")
        SubElement(graph, "path").text = figname

    # simulation_status
    if True: # only for indent clarity, perhaps later turned into a function

        info = SubElement(elements, "info")
        SubElement(info, "caption").text = "Simulation status"
        data = SubElement(info, "data")

        # Get total number of events
        c.execute("""SELECT COUNT(simulation_status) FROM events WHERE (simulation_status!='NOT_DESIRED')""")
        nof_events = c.fetchone()[0]

        # Get simulation_status count
        c.execute("""SELECT simulation_status, COUNT(*) FROM events WHERE (simulation_status!='NOT_DESIRED') GROUP BY simulation_status""")

        fraction = []
        labels = []
        for e in c.fetchall():
            fraction.append(float(e[1]) / nof_events)
            labels.append("{0} {1}={2:.1f}%".format(e[0], e[1], 100 * float(e[1]) / nof_events))

            record = SubElement(data, "record")
            SubElement(record, "key").text = e[0]
            SubElement(record, "value").text = str(e[1])

        total = SubElement(data, "total").text = str(nof_events)

        fig = plt.figure()
        fig.add_subplot(111, aspect='equal')

        plt.pie(fraction, labels=labels, shadow=True)

        figname = statdir+"/event_simulation_status_pie.png"
        fig.savefig(figname)

        graph = SubElement(info, "graph")
        SubElement(graph, "path").text = figname

    # antennaset
    if True: # only for indent clarity, perhaps later turned into a function

        info = SubElement(elements, "info")
        SubElement(info, "caption").text = "Antennaset"
        data = SubElement(info, "data")

        # Get total number of events
        c.execute("""SELECT COUNT(antennaset) FROM events""")
        nof_events = c.fetchone()[0]

        # Get status count
        c.execute("""SELECT antennaset, COUNT(*) FROM events GROUP BY antennaset""")

        fraction = []
        labels = []

        tpa = {'LBA' : 0, 'HBA' : 0}
        for e in c.fetchall():
            fraction.append(float(e[1]) / nof_events)

            if 'LBA' in e[0]:
                tpa['LBA'] += int(e[1])
            elif 'HBA' in e[0]:
                tpa['HBA'] += int(e[1])
            
            labels.append("{0} {1}={2:.1f}%".format(e[0], e[1], 100 * float(e[1]) / nof_events))

            record = SubElement(data, "record")
            SubElement(record, "key").text = e[0]
            SubElement(record, "value").text = str(e[1])

        total = SubElement(data, "total").text = str(nof_events)

        fig = plt.figure()
        fig.add_subplot(111, aspect='equal')

        plt.pie(fraction, labels=labels, shadow=True)

        figname = statdir+"/antennaset_pie.png"
        fig.savefig(figname)

        graph = SubElement(info, "graph")
        SubElement(graph, "path").text = figname

        # cumulative plot for LBA and HBA
        info = SubElement(elements, "info")
        SubElement(info, "caption").text = "Antennaset Cumulative"
        data = SubElement(info, "data")

        record = SubElement(data, "record")
        SubElement(record, "key").text = 'LBA'
        SubElement(record, "value").text = str(tpa['LBA'])

        record = SubElement(data, "record")
        SubElement(record, "key").text = 'HBA'
        SubElement(record, "value").text = str(tpa['HBA'])

        total = SubElement(data, "total").text = str(nof_events)

        fig = plt.figure()
        fig.add_subplot(111, aspect='equal')

        plt.pie([float(tpa['LBA']) / nof_events, float(tpa['HBA']) / nof_events], labels=["{0} {1}={2:.1f}%".format(k, tpa[k], 100 * float(tpa[k]) / nof_events) for k in ['LBA', 'HBA']], shadow=True)

        figname = statdir+"/antennaset_cumulative_pie.png"
        fig.savefig(figname)

        graph = SubElement(info, "graph")
        SubElement(graph, "path").text = figname

    # nof stations per event
    if True: # only for indent clarity, perhaps later turned into a function

        info = SubElement(elements, "info")
        SubElement(info, "caption").text = "Number of superterp stations per event with status CR_FOUND"
        data = SubElement(info, "data")

        # Get statistic
        c.execute("""select count(distinct(stationname)) from events as e inner join event_datafile as ed on (e.eventID=ed.eventID) inner join datafile_station as ds on (ed.datafileID=ds.datafileID) inner join stations as s on (s.stationID=ds.stationID) where e.status='CR_FOUND' and s.stationname in ('CS002','CS003','CS004','CS005','CS006','CS007') group by e.eventID""")

        count = []
        for e in c.fetchall():
            count.append(int(e[0]))

        fig = plt.figure()
        fig.add_subplot(111)

        temp = plt.hist(count, bins=np.arange(8)-0.5)

        for i in range(len(temp[0])):
            record = SubElement(data, "record")
            SubElement(record, "key").text = str(i + 1)
            SubElement(record, "value").text = str(temp[0][i])

        plt.xlim(-0.5, 6.5)

        figname = statdir+"/superterp_stations_per_event.png"
        fig.savefig(figname)

        graph = SubElement(info, "graph")
        SubElement(graph, "path").text = figname

    # nof stations per event
    if True: # only for indent clarity, perhaps later turned into a function

        info = SubElement(elements, "info")
        SubElement(info, "caption").text = "Number of good superterp stations per event with status CR_FOUND"
        data = SubElement(info, "data")

        # Get statistic
        c.execute("""select count(distinct(stationname)) from events as e inner join event_datafile as ed on (e.eventID=ed.eventID) inner join datafile_station as ds on (ed.datafileID=ds.datafileID) inner join stations as s on (s.stationID=ds.stationID) where e.status='CR_FOUND' and s.stationname in ('CS002','CS003','CS004','CS005','CS006','CS007') and (s.status='GOOD') group by e.eventID""")

        count = []
        for e in c.fetchall():
            count.append(int(e[0]))

        fig = plt.figure()
        fig.add_subplot(111)

        temp = plt.hist(count, bins=np.arange(8)-0.5)

        for i in range(len(temp[0])):
            record = SubElement(data, "record")
            SubElement(record, "key").text = str(i + 1)
            SubElement(record, "value").text = str(temp[0][i])

        plt.xlim(-0.5, 6.5)

        figname = statdir+"/good_superterp_stations_per_event.png"
        fig.savefig(figname)

        graph = SubElement(info, "graph")
        SubElement(graph, "path").text = figname

    # nof stations per event
    if True: # only for indent clarity, perhaps later turned into a function

        info = SubElement(elements, "info")
        SubElement(info, "caption").text = "Number of good superterp stations per LBA_OUTER event with status CR_FOUND"
        data = SubElement(info, "data")

        # Get statistic
        c.execute("""select count(distinct(stationname)) from events as e inner join event_datafile as ed on (e.eventID=ed.eventID) inner join datafile_station as ds on (ed.datafileID=ds.datafileID) inner join stations as s on (s.stationID=ds.stationID) where e.status='CR_FOUND' and e.antennaset='LBA_OUTER' and s.stationname in ('CS002','CS003','CS004','CS005','CS006','CS007') and (s.status='GOOD') group by e.eventID""")

        count = []
        for e in c.fetchall():
            count.append(int(e[0]))

        fig = plt.figure()
        fig.add_subplot(111)

        temp = plt.hist(count, bins=np.arange(8)-0.5)

        for i in range(len(temp[0])):
            record = SubElement(data, "record")
            SubElement(record, "key").text = str(i + 1)
            SubElement(record, "value").text = str(temp[0][i])

        plt.xlim(-0.5, 6.5)

        figname = statdir+"/good_superterp_stations_per_event_lba_outer.png"
        fig.savefig(figname)

        graph = SubElement(info, "graph")
        SubElement(graph, "path").text = figname

    # nof stations per event
    if True: # only for indent clarity, perhaps later turned into a function

        info = SubElement(elements, "info")
        SubElement(info, "caption").text = "Number of good superterp stations per LBA_INNER event with status CR_FOUND"
        data = SubElement(info, "data")

        # Get statistic
        c.execute("""select count(distinct(stationname)) from events as e inner join event_datafile as ed on (e.eventID=ed.eventID) inner join datafile_station as ds on (ed.datafileID=ds.datafileID) inner join stations as s on (s.stationID=ds.stationID) where e.status='CR_FOUND' and e.antennaset='LBA_INNER' and s.stationname in ('CS002','CS003','CS004','CS005','CS006','CS007') and (s.status='GOOD') group by e.eventID""")

        count = []
        for e in c.fetchall():
            count.append(int(e[0]))

        fig = plt.figure()
        fig.add_subplot(111)

        temp = plt.hist(count, bins=np.arange(8)-0.5)

        for i in range(len(temp[0])):
            record = SubElement(data, "record")
            SubElement(record, "key").text = str(i + 1)
            SubElement(record, "value").text = str(temp[0][i])

        plt.xlim(-0.5, 6.5)

        figname = statdir+"/good_superterp_stations_per_event_lba_inner.png"
        fig.savefig(figname)

        graph = SubElement(info, "graph")
        SubElement(graph, "path").text = figname

    # Open string file descriptor for output
    f = StringIO()

    # Write header information
    f.write('<?xml version="1.0" ?>')
    f.write('<?xml-stylesheet type="text/xsl" href="/layout/statistics.xsl"?>')

    # Write XML DOM to string file descriptor
    ElementTree(elements).write(f)

    c.close()

    return f.getvalue()

def events_handler(do_all=True):
    """Handle summary of events.
    """

    # Connect to database
    if have_psycopg2 and options.host:
        # Open PostgreSQL database
        print "Opening connection to PostgreSQL database"
        conn = psycopg2.connect(host=options.host, user=options.user, password=options.password, dbname=options.dbname)
    else:
        # Open SQLite database
        print "Opening Sqlite database"
        conn = sqlite3.connect(options.database, timeout=60.0)

    # Create cursor
    c = conn.cursor()

    # Fetch all event IDs
    if do_all:
        c.execute("""SELECT e.eventID, e.timestamp, e.antennaset, e.status, e.alt_status, lora_energy, lora_core_x, lora_core_y, lora_azimuth, lora_elevation, lora_moliere, last_processed, e.flagged, e.flagged_reason, e.simulation_status, e.simulation_statusmessage FROM events AS e LEFT JOIN eventparameters AS ep ON (e.eventID=ep.eventID)""")
    else:
        c.execute("""SELECT e.eventID, e.timestamp, e.antennaset, e.status, e.alt_status, lora_energy, lora_core_x, lora_core_y, lora_azimuth, lora_elevation, lora_moliere, last_processed, e.flagged, e.flagged_reason, e.simulation_status, e.simulation_statusmessage FROM events AS e LEFT JOIN eventparameters AS ep ON (e.eventID=ep.eventID) WHERE ((status='CR_FOUND') or (alt_status='CR_FOUND'))""")

    all_events = c.fetchall()

    # for all good events fetch the number of good antennas
    c.execute("""SELECT e.eventID, COUNT(s.status) FROM events AS e LEFT JOIN event_datafile AS ed ON (e.eventID=ed.eventID) INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID) INNER JOIN stations AS s ON (ds.stationID=s.stationID) WHERE (s.status='GOOD') GROUP BY (e.eventID)""")

    good_station_count = {}
    for row in c.fetchall():
        good_station_count[str(row[0])] = str(row[1])

    # Generate empty XML
    elements = Element("elements")

    for e in all_events:

        event = SubElement(elements, "event")

        SubElement(event, "id").text = str(e[0])
        SubElement(event, "timestamp").text = str(datetime.utcfromtimestamp(e[1]))
        SubElement(event, "antennaset").text = str(e[2])
        SubElement(event, "status").text = str(e[3])
        SubElement(event, "alt_status").text = str(e[4])
        SubElement(event, "last_processed").text = str(unpickle_parameter(e[11]))
        SubElement(event, "flagged").text = str(e[12])
        SubElement(event, "flagged_reason").text = str(e[13])
        SubElement(event, "simulation_status").text = str(e[14])
        SubElement(event, "simulation_statusmessage").text = str(e[15])
        if str(e[3]) == "CR_FOUND":
            SubElement(event, "nof_good_stations").text = good_station_count[str(e[0])]

        lora = SubElement(event, "lora")

        energy = unpickle_parameter(e[5])
        core_x = unpickle_parameter(e[6])
        core_y = unpickle_parameter(e[7])
        azimuth = unpickle_parameter(e[8])
        elevation = unpickle_parameter(e[9])
        moliere = unpickle_parameter(e[10])

        if good_lora_reconstruction(core_x, core_y, moliere, elevation):
            lora.attrib['good_reconstruction'] = "true"

        SubElement(lora, "energy").text = str(energy)
        SubElement(lora, "log_energy").text = str(np.log10(energy)) if isinstance(energy, float) else ""
        SubElement(lora, "core_x").text = str(core_x)
        SubElement(lora, "core_y").text = str(core_y)
        SubElement(lora, "azimuth").text = str(azimuth)
        SubElement(lora, "elevation").text = str(elevation)
        SubElement(lora, "moliere").text = str(moliere)

    # Open string file descriptor for output
    f = StringIO()

    # Write header information
    f.write('<?xml version="1.0" ?>')

    if do_all:
        f.write('<?xml-stylesheet type="text/xsl" href="/layout/all_events.xsl"?>')
    else:
        f.write('<?xml-stylesheet type="text/xsl" href="/layout/events.xsl"?>')

    # Write XML DOM to string file descriptor
    ElementTree(elements).write(f)

    c.close()

    return f.getvalue()

def event_handler(eventID):
    """Handle a single event.
    """

    # Connect to database
    if have_psycopg2 and options.host:
        # Open PostgreSQL database
        print "Opening connection to PostgreSQL database"
        conn = psycopg2.connect(host=options.host, user=options.user, password=options.password, dbname=options.dbname)
    else:
        # Open SQLite database
        print "Opening Sqlite database"
        conn = sqlite3.connect(options.database, timeout=60.0)

    # Create cursor
    c = conn.cursor()

    # Generate empty XML
    elements = Element("elements")

    # Add event header
    elements.append(event_header(c, eventID))

    # Fetch event parameter keys
    parameters = SubElement(elements, "parameters")
    figures = SubElement(elements, "figures")

    if options.host:
        # PostgreSQL
        c.execute("SELECT column_name FROM information_schema.columns WHERE table_name ='eventparameters'")

        keys = [str(e[0]) for e in c.fetchall()[1:]]
    else:
        # Sqlite
        c.execute("PRAGMA table_info(eventparameters)")

        keys = [str(e[1]) for e in c.fetchall()[1:]]

    # Fetch event parameter values
    sql = "SELECT * FROM eventparameters WHERE eventID={0}".format(eventID)
    c.execute(sql)

    v = c.fetchone()
    if v is not None and len(v) > 1:

        values = [unpickle_parameter(e) for e in v[1:]]

        for e in zip(keys, values):

            parameter = SubElement(parameters, "parameter")

            SubElement(parameter, "key").text = e[0]
            SubElement(parameter, "value").text = str(e[1])

    # Fetch event level figures
    sql = "SELECT plotfiles, crp_plotfiles FROM eventparameters WHERE eventID={0}".format(eventID)
    c.execute(sql)

    v = c.fetchone()
    if v is not None and len(v) > 1:

        values = [unpickle_parameter(e) for e in v]

        for e in zip(keys, values):

            for p in e[1]:
                figure = SubElement(figures, "figure")
                SubElement(figure, "path").text = "/results"+str(p).split("results")[1]

    # Get KNMI thunderstorm map
    sql = "SELECT timestamp FROM events WHERE eventID={0}".format(eventID)
    c.execute(sql)

    e = c.fetchone()
    d = datetime.utcfromtimestamp(e[0])

    # Create temporary directory for statistics plot if needed
    statdir = "statistics"
    if not os.path.exists(statdir):
        os.makedirs(statdir)

    thunderstorm_picture = statdir + "/knmi_thunderstorm-{0}.png".format(d.date().isoformat())
    if not os.path.exists(thunderstorm_picture):
        print "getting {0} from KNMI website".format(thunderstorm_picture)
        get_knmi_thunderstorm_picture(d.year, d.month, d.day, store_as = thunderstorm_picture)

    figure = SubElement(figures, "figure")
    SubElement(figure, "path").text = "/" + thunderstorm_picture

    # Get electric field measurements
    ws_picture = "weather_station/weather_station_efield-{0}.png".format(eventID)
    try:
        if not os.path.exists(ws_picture):
            get_weather_station_efield(d.year, d.month, d.day, d.hour, d.minute, d.second, store_as=ws_picture, elfield_dir="weather_station")
        
        figure = SubElement(figures, "figure")
        SubElement(figure, "path").text = "/" + ws_picture
    except:
        print "no weather station data for {0}".format(ws_picture)

    sql = "SELECT lora_ldf FROM eventparameters WHERE eventID={0}".format(eventID)
    c.execute(sql)
    v = c.fetchone()

    if v is not None and len(v) > 0:
        figure = SubElement(figures, "figure")
        SubElement(figure, "path").text = "/LORA/"+unpickle_parameter(v[0]).split("/")[-1]

    # Open string file descriptor for output
    f = StringIO()

    # Write header information
    f.write('<?xml version="1.0" ?>')
    f.write('<?xml-stylesheet type="text/xsl" href="/layout/event.xsl"?>')

    # Write XML DOM to string file descriptor
    ElementTree(elements).write(f)

    c.close()

    return f.getvalue()

def station_handler(eventID, station_name):
    """Handle a single event.
    """

    # Connect to database
    if have_psycopg2 and options.host:
        # Open PostgreSQL database
        print "Opening connection to PostgreSQL database"
        conn = psycopg2.connect(host=options.host, user=options.user, password=options.password, dbname=options.dbname)
    else:
        # Open SQLite database
        print "Opening Sqlite database"
        conn = sqlite3.connect(options.database, timeout=60.0)

    # Create cursor
    c = conn.cursor()

    # Generate empty XML
    elements = Element("elements")

    # Add event header
    header = event_header(c, eventID, station=station_name)
    elements.append(header)

    # Get all polarizations for this station
    sql = """SELECT p.direction, p.status, p.alt_status FROM
    event_datafile AS ed INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
    INNER JOIN stations AS s ON (ds.stationID=s.stationID)
    INNER JOIN station_polarization AS sp ON (ds.stationID=sp.stationID)
    INNER JOIN polarizations AS p ON (sp.polarizationID=p.polarizationID)
    WHERE (ed.eventID={0} AND s.stationname='{1}')""".format(eventID, station_name)
    c.execute(sql)

    polarizations = SubElement(header, "polarizations")
    for e in c.fetchall():
        s = SubElement(polarizations, "polarization")
        SubElement(s, "name").text = e[0]
        SubElement(s, "status").text = e[1]
        SubElement(s, "alt_status").text = e[1]

    # Fetch all station parameters
    parameters = SubElement(elements, "parameters")
    figures = SubElement(elements, "figures")

    if options.host:
        # PostgreSQL
        c.execute("SELECT column_name FROM information_schema.columns WHERE table_name ='stationparameters'")

        keys = [str(e[0]) for e in c.fetchall()[1:]]
    else:
        # Sqlite
        c.execute("PRAGMA table_info(stationparameters)")

        keys = [str(e[1]) for e in c.fetchall()[1:]]

    # Fetch all station parameter values
    sql = """SELECT sp.* FROM
    event_datafile AS ed INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
    INNER JOIN stations AS s ON (ds.stationID=s.stationID)
    INNER JOIN stationparameters AS sp ON (s.stationID=sp.stationID)
    WHERE (ed.eventID={0} AND s.stationname='{1}')""".format(eventID, station_name)
    c.execute(sql)

    v = c.fetchone()
    if v is not None and len(v) > 1:

        values = [unpickle_parameter(e) for e in v[1:]]

        for e in zip(keys, values):

            parameter = SubElement(parameters, "parameter")

            SubElement(parameter, "key").text = e[0]
            SubElement(parameter, "value").text = str(e[1])

    # Fetch station level figures
    sql = """SELECT sp.plotfiles, sp.crp_plotfiles FROM
    event_datafile AS ed INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
    INNER JOIN stations AS s ON (ds.stationID=s.stationID)
    INNER JOIN stationparameters AS sp ON (s.stationID=sp.stationID)
    WHERE (ed.eventID={0} AND s.stationname='{1}')""".format(eventID, station_name)
    c.execute(sql)

    v = c.fetchone()
    if v is not None and len(v) > 1:

        values = [unpickle_parameter(e) for e in v]

        for e in zip(keys, values):

            for p in e[1]:
                figure = SubElement(figures, "figure")
                SubElement(figure, "path").text = "/results"+str(p).split("results")[1]

    # Open string file descriptor for output
    f = StringIO()

    # Write header information
    f.write('<?xml version="1.0" ?>')
    f.write('<?xml-stylesheet type="text/xsl" href="/layout/station.xsl"?>')

    # Write XML DOM to string file descriptor
    ElementTree(elements).write(f)

    c.close()

    return f.getvalue()

def polarization_handler(eventID, station_name, polarization_direction):
    """Handle a single event.
    """

    # Connect to database
    if have_psycopg2 and options.host:
        # Open PostgreSQL database
        print "Opening connection to PostgreSQL database"
        conn = psycopg2.connect(host=options.host, user=options.user, password=options.password, dbname=options.dbname)
    else:
        # Open SQLite database
        print "Opening Sqlite database"
        conn = sqlite3.connect(options.database, timeout=60.0)

    # Create cursor
    c = conn.cursor()

    # Generate empty XML
    elements = Element("elements")

    # Add event header
    header = event_header(c, eventID, station=station_name, polarization=polarization_direction)
    elements.append(header)

    # Get all polarizations for this station
    sql = """SELECT direction FROM
    event_datafile AS ed INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
    INNER JOIN stations AS s ON (ds.stationID=s.stationID)
    INNER JOIN station_polarization AS sp ON (ds.stationID=sp.stationID)
    INNER JOIN polarizations AS p ON (sp.polarizationID=p.polarizationID)
    WHERE (ed.eventID={0} AND s.stationname='{1}')""".format(eventID, station_name)
    c.execute(sql)

    polarizations = SubElement(header, "polarizations")
    for e in c.fetchall():
        s = SubElement(polarizations, "polarization")
        SubElement(s, "name").text = e[0]
        if e[0] == polarization_direction:
            s.set("current", "true")

    # Fetch polarization parameter keys
    parameters = SubElement(elements, "parameters")
    figures = SubElement(elements, "figures")

    if options.host:
        # PostgreSQL
        c.execute("SELECT column_name FROM information_schema.columns WHERE table_name ='polarizationparameters'")

        keys = [str(e[0]) for e in c.fetchall()[1:]]
    else:
        # Sqlite
        c.execute("PRAGMA table_info(polarizationparameters)")

        keys = [str(e[1]) for e in c.fetchall()[1:]]

    # Fetch polarization parameter values
    sql = """SELECT pp.* FROM
    event_datafile AS ed INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
    INNER JOIN stations AS s ON (ds.stationID=s.stationID)
    INNER JOIN station_polarization AS sp ON (ds.stationID=sp.stationID)
    INNER JOIN polarizations AS p ON (sp.polarizationID=p.polarizationID)
    INNER JOIN polarizationparameters AS pp ON (p.polarizationID=pp.polarizationID)
    WHERE (ed.eventID={0} AND s.stationname='{1}' AND p.direction='{2}')
    """.format(eventID, station_name, polarization_direction)
    c.execute(sql)

    v = c.fetchone()
    if v is not None and len(v) > 1:

        values = [unpickle_parameter(e) for e in v[1:]]

        for e in zip(keys, values):

            parameter = SubElement(parameters, "parameter")

            SubElement(parameter, "key").text = e[0]
            SubElement(parameter, "value").text = str(e[1])

    # Fetch polarization level figures
    sql = """SELECT pp.plotfiles, pp.crp_plotfiles FROM
    event_datafile AS ed INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
    INNER JOIN stations AS s ON (ds.stationID=s.stationID)
    INNER JOIN station_polarization AS sp ON (ds.stationID=sp.stationID)
    INNER JOIN polarizations AS p ON (sp.polarizationID=p.polarizationID)
    INNER JOIN polarizationparameters AS pp ON (p.polarizationID=pp.polarizationID)
    WHERE (ed.eventID={0} AND s.stationname='{1}' AND p.direction='{2}')
    """.format(eventID, station_name, polarization_direction)
    c.execute(sql)

    v = c.fetchone()
    if v is not None and len(v) > 1:

        values = [unpickle_parameter(e) for e in v]

        for e in zip(keys, values):

            for p in e[1]:
                figure = SubElement(figures, "figure")
                SubElement(figure, "path").text = "/results"+str(p).split("results")[1]

    # Open string file descriptor for output
    f = StringIO()

    # Write header information
    f.write('<?xml version="1.0" ?>')
    f.write('<?xml-stylesheet type="text/xsl" href="/layout/polarization.xsl"?>')

    # Write XML DOM to string file descriptor
    ElementTree(elements).write(f)

    c.close()

    return f.getvalue()

class CustomHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def do_GET(self):

        try:

            if self.path == '/statistics':

                s = statistics_handler()

                self.send_response(200)
                self.send_header('Content-type','text/xml')
                self.end_headers()
                self.wfile.write(s)

            elif self.path == '/all_events':

                s = events_handler(do_all=True)

                self.send_response(200)
                self.send_header('Content-type','text/xml')
                self.end_headers()
                self.wfile.write(s)

            elif self.path == '/events':

                s = events_handler(do_all=False)

                self.send_response(200)
                self.send_header('Content-type','text/xml')
                self.end_headers()
                self.wfile.write(s)

            elif re.match(r'/events/[0-9]+$', self.path):

                m = re.match(r'/events/([0-9]+)$', self.path)

                s = event_handler(int(m.group(1)))

                self.send_response(200)
                self.send_header('Content-type','text/xml')
                self.end_headers()
                self.wfile.write(s)

            elif re.match(r'/events/([0-9]+)/([A-Z][A-Z][0-9][0-9][0-9])$', self.path):

                m = re.match(r'/events/([0-9]+)/([A-Z][A-Z][0-9][0-9][0-9])$', self.path)

                s = station_handler(int(m.group(1)), m.group(2))

                self.send_response(200)
                self.send_header('Content-type','text/xml')
                self.end_headers()
                self.wfile.write(s)

            elif re.match(r'/events/([0-9]+)/([A-Z][A-Z][0-9][0-9][0-9])/(\w+)$', self.path):

                m = re.match(r'/events/([0-9]+)/([A-Z][A-Z][0-9][0-9][0-9])/(\w+)$', self.path)

                s = polarization_handler(int(m.group(1)), m.group(2), m.group(3))

                self.send_response(200)
                self.send_header('Content-type','text/xml')
                self.end_headers()
                self.wfile.write(s)

            else:
                # serve files, and directory listings by following self.path from
                # current working directory
                SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)

        except Exception:

            etype, value, tb = sys.exc_info()
            error = ''.join(traceback.format_exception(etype, value, tb))

            s = "<html><head><title>Error</title></head><body><h1>Error</h1>"+error+"</body></html>"

            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()
            self.wfile.write(s)

httpd = SocketServer.ThreadingTCPServer((options.hostname, options.port),CustomHandler)

print "serving at port", options.port
httpd.serve_forever()

