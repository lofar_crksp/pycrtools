<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <head>
    <link rel="stylesheet" type="text/css" href="layout/database.css" />
    <title>LOFAR Cosmic Ray Events</title>
    <script src="layout/sorttable.js"></script>
  </head>
  <body>
  <h1>LOFAR Cosmic Ray Events</h1>
  <nav>
      <ul class="cf">
          <li><a class="dropdown" href="#">Section</a>
              <ul>
                  <li><a href="/events">Good events</a></li>
                  <li><a href="/all_events">All events</a></li>
              </ul>
          </li>
          <li><a href="/events">Good events</a></li>
      </ul>
  </nav>
  <table class="sortable">
    <caption id="all" class="parameters">All events</caption>
    <tr>
      <th>Id</th>
      <th>Timestamp</th>
      <th>Antennaset</th>
      <th>Event status</th>
      <th>Event alt_status</th>
      <th>Last processed</th>
      <th>Simulation status</th>
      <th>Simulation statusmessage</th>
      <th>Flagged</th>
      <th>Flagged reason</th>
      <th>Energy (eV)</th>
      <th>Energy (log10(eV))</th>
      <th>Core x (m)</th>
      <th>Core y (m)</th>
      <th>Azimuth (deg)</th>
      <th>Elevation (deg)</th>
      <th>Moliere radius (m)</th>
    </tr>
    <xsl:for-each select="/elements/event">
      <tr>
        <td>
          <a><xsl:attribute name="href">
          events/<xsl:value-of select="id"/></xsl:attribute>
          <xsl:value-of select="id"/>
          </a>
        </td>
        <td><xsl:value-of select="timestamp"/></td>
        <td><xsl:value-of select="antennaset"/></td>
        <td><xsl:value-of select="status"/></td>
        <td><xsl:value-of select="alt_status"/></td>
        <td><xsl:value-of select="last_processed"/></td>
        <td><xsl:value-of select="simulation_status"/></td>
        <td><xsl:value-of select="simulation_statusmessage"/></td>
        <td><xsl:value-of select="flagged"/></td>
        <td><xsl:value-of select="flagged_reason"/></td>
        <td><xsl:value-of select="lora/energy"/></td>
        <td><xsl:value-of select="lora/log_energy"/></td>
        <td><xsl:value-of select="lora/core_x"/></td>
        <td><xsl:value-of select="lora/core_y"/></td>
        <td><xsl:value-of select="lora/azimuth"/></td>
        <td><xsl:value-of select="lora/elevation"/></td>
        <td><xsl:value-of select="lora/moliere"/></td>
      </tr>
    </xsl:for-each>
  </table>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>

