import os
import optparse
import subprocess 
import psycopg2

# Parse commandline options
parser = optparse.OptionParser()

parser.add_option("--host", default=None, help="PostgreSQL host.")
parser.add_option("--user", default=None, help="PostgreSQL user.")
parser.add_option("--password", default=None, help="PostgreSQL password.")
parser.add_option("--dbname", default=None, help="PostgreSQL dbname.")
parser.add_option("--skip-conex", default=False, action="store_true")
parser.add_option("--skip-coreas", default=False, action="store_true")
parser.add_option("--skip-analysis", default=False, action="store_true")

(options, args) = parser.parse_args()

# Connect to database
conn = psycopg2.connect(host=options.host, user=options.user, password=options.password, dbname=options.dbname)

# Create cursor
c = conn.cursor()

# find events to start simulations for
sql = "SELECT eventid FROM events WHERE (simulation_status='DESIRED' OR simulation_status='CONEX_STARTED' OR simulation_status='CONEX_DONE' OR simulation_status='COREAS_STARTED') ORDER BY eventid" #  OR simulation_status='COREAS_DONE'
c.execute(sql)

events = [int(e[0]) for e in c.fetchall()]

# HACK
# eventlist_file = '/vol/astro3/lofar/sim/pipeline/eventlist_Nature.txt'
# with open(eventlist_file) as f:
#    Nature_events = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
# Nature_eventlist = [int(x) for x in Nature_events]

# events = [id for id in events if id in Nature_eventlist]

#print '%d events to be simulated which are also in Nature set'
#print events
#print '------'

# 1/0
skip = ""
if options.skip_conex:
    skip += "--skip-conex "
if options.skip_coreas:
    skip += "--skip-coreas "
if options.skip_analysis:
    skip += "--skip-analysis "

for eventID in events:
    print eventID
    subprocess.call("python " + os.environ["LOFARSOFT"] + "/src/PyCRTools/pipelines/cr_simulation.py --nof-conex-proton=450 --nof-conex-iron=150 --host={1} --user={2} --password={3} --dbname={4} --id={0} {5}".format(eventID, options.host, options.user, options.password, options.dbname, skip), shell=True)
# AC: included --ignore-suspended-jobs to force processing through.
# AC: Set nof-conex-proton to 450, and 150 for iron.
# AC: Added --iteration=0 for first production run of Corsika (version Sept 2017).
# AC: Removed --iteration=0 for iterating simulations with new energy level
# AC (Dec 2019): remove --ignore-suspended-jobs

# Close connection to database
conn.close()

#proc = subprocess.Popen(['chgrp -R lofar /vol/astro7/lofar/sim/pipeline/*'], shell=True)
#proc.wait()
