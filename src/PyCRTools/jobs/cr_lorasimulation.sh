#! /bin/bash
#SBATCH --time=1-23:50:00
#SBATCH -p normal
# # SBATCH -x coma[21-45]
#SBATCH -N 1 -n 16
# Run lora simulation pipeline.

hostname

export LOFARSOFT=/vol/optcoma/pycrtools
export PYTHONPATH=$LOFARSOFT/release/lib/python:$PYTHONPATH
export LD_LIBRARY_PATH=$LOFARSOFT/release/lib:$LD_LIBRARY_PATH

# source geant4make.sh from acorstanje
# and set G4WORKDIR to /vol/optcoma/cr-simulations/LORAtools_GeVfix
. /vol/astro2/users/acorstanje/geant-install/share/Geant4-9.6.4/geant4make/geant4make.sh
export G4WORKDIR=/vol/optcoma/cr-simulations/LORAtools_GeVfix

PYCRTOOLS=$LOFARSOFT/src/PyCRTools

#DB_PATH=$BASE_PATH
#DB_FILE=$DB_PATH/bogus
#DATA_PATH=$BASE_PATH/data
#RESULTS_PATH=$BASE_PATH/results
#LORA_PATH=$BASE_PATH/LORA
LOG_PATH=/vol/astro7/lofar/sim/pipeline/run/output_lorasim

# Get event id for current task and mark it as NEXT
EVENT_ID=$(awk "NR==$SLURM_ARRAY_TASK_ID" $HOME/eventlist_coreasdone.txt)
LOGFILE=$LOG_PATH/cr_lorasimulation-$EVENT_ID.txt

# Set permissions on resulting files
umask 002

echo $EVENT_ID

# Run the pipeline
/usr/bin/python -u $PYCRTOOLS/pipelines/cr_lorasimulation.py --id=$EVENT_ID --host=coma00.science.ru.nl --user=crdb --password=crdb --dbname=crdb &> $LOGFILE

