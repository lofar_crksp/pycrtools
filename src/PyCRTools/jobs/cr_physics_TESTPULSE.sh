#! /bin/bash
#SBATCH --time=12:00:00
#SBATCH -p normal
#SBATCH --mem=16G

hostname

export LOFARSOFT=/vol/optcoma/pycrtools
export PYTHONPATH=$LOFARSOFT/release/lib/python:$PYTHONPATH
export LD_LIBRARY_PATH=$LOFARSOFT/release/lib:$LD_LIBRARY_PATH

PYCRTOOLS=$LOFARSOFT/src/PyCRTools

BASE_PATH=/vol/astro3/lofar/vhecr/lora_triggered
DB_PATH=$BASE_PATH
DB_FILE=$DB_PATH/bogus
DATA_PATH=$BASE_PATH/data
RESULTS_PATH=$BASE_PATH/results_with_TESTPULSE
LORA_PATH=$BASE_PATH/LORA
LOG_PATH=$BASE_PATH/log

# Get event id for current task and mark it as NEXT
EVENT_ID=$(awk "NR==$SLURM_ARRAY_TASK_ID" $HOME/cr_physics_new)
LOGFILE=$LOG_PATH/cr_physics-$EVENT_ID.txt

# Set permissions on resulting files
umask 002

echo $EVENT_ID

# Run the pipeline
# Use --debug-test-pulse
# Added option --only-inner-core-stations, to take only CS0??
/usr/bin/python -u $PYCRTOOLS/pipelines/cr_physics.py --id=$EVENT_ID --debug-test-pulse --only-inner-core-stations --database=$DB_FILE -l $LORA_PATH --host=coma00.science.ru.nl --user=crdb --password=crdb --dbname=crdb --output-dir=$RESULTS_PATH --gain-calibration-type=galaxy --store-calibrated-pulse-block --initial-direction-from-db --run-polarization --run-ldf --run-wavefront &> $LOGFILE

