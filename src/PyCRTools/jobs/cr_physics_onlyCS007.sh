#! /bin/bash
#SBATCH --time=00:04:59
#SBATCH -p debug
# Note debug!
hostname

export LOFARSOFT=/vol/optcoma/pycrtools
export PYTHONPATH=$LOFARSOFT/release/lib/python:$PYTHONPATH
export LD_LIBRARY_PATH=$LOFARSOFT/release/lib:$LD_LIBRARY_PATH

PYCRTOOLS=$LOFARSOFT/src/PyCRTools

BASE_PATH=/vol/astro3/lofar/vhecr/lora_triggered
DB_PATH=$BASE_PATH
DB_FILE=$DB_PATH/bogus
DATA_PATH=$BASE_PATH/data
RESULTS_PATH=$BASE_PATH/results
LORA_PATH=$BASE_PATH/LORA
LOG_PATH=$BASE_PATH/log

# Get event id for current task and mark it as NEXT
EVENT_ID=$(awk "NR==$SLURM_ARRAY_TASK_ID" $HOME/cr_physics_new)
LOGFILE=$LOG_PATH/cr_physics-$EVENT_ID.txt

# Set permissions on resulting files
umask 002

echo $EVENT_ID

# Run the pipeline
# Only on CS003, hence the --station=CS003 flag
/usr/bin/python -u $PYCRTOOLS/pipelines/cr_physics.py --id=$EVENT_ID --database=$DB_FILE -l $LORA_PATH --host=coma00.science.ru.nl --user=crdb --password=crdb --dbname=crdb --output-dir=$RESULTS_PATH --station=CS007 --gain-calibration-type=crane --store-calibrated-pulse-block --initial-direction-from-db --run-polarization --run-ldf --run-wavefront &> $LOGFILE

