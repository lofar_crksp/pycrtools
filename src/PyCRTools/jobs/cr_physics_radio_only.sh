#! /bin/bash
#SBATCH --time=12:00:00
#SBATCH -p normal
#SBATCH --mem=12G

hostname

export LOFARSOFT=/vol/optcoma/pycrtools
export PYTHONPATH=$LOFARSOFT/release/lib/python:$PYTHONPATH
export LD_LIBRARY_PATH=$LOFARSOFT/release/lib:$LD_LIBRARY_PATH

PYCRTOOLS=$LOFARSOFT/src/PyCRTools

BASE_PATH=/vol/astro3/lofar/vhecr/lora_triggered
DB_PATH=$BASE_PATH
DB_FILE=$DB_PATH/bogus
DATA_PATH=$BASE_PATH/data
RESULTS_PATH=$BASE_PATH/results
LORA_PATH=$BASE_PATH/LORA
LOG_PATH=$BASE_PATH/log

# Get event id for current task and mark it as NEXT
#EVENT_ID=$(awk "NR==$SLURM_ARRAY_TASK_ID" $HOME/cr_physics_new)
EVENT_ID=268585710
LOGFILE=$LOG_PATH/cr_physics-$EVENT_ID.txt

# Set permissions on resulting files
umask 002

echo $EVENT_ID

# Run the pipeline
/usr/bin/python -u /vol/astro2/users/acorstanje/lofarsoft/src/PyCRTools/pipelines/cr_physics_radio_only.py --id=$EVENT_ID --blocksize=24000 --initial-direction-az=45.0 --initial-direction-el=80.98 --initial-pulse-time=65500 --database=$DB_FILE -l $LORA_PATH --host=coma00.science.ru.nl --user=crdb --password=crdb --dbname=crdb --output-dir=$RESULTS_PATH --gain-calibration-type=crane --store-calibrated-pulse-block  --run-polarization &> $LOGFILE

