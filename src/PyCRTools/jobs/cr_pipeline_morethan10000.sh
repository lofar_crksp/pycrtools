#! /bin/bash

MAX_SIMULTANEOUS_JOBS=64

export LOFARSOFT=/vol/optcoma/lofarsoft
export PYTHONPATH=$LOFARSOFT/release/lib/python:$PYTHONPATH
export LD_LIBRARY_PATH=$LOFARSOFT/release/lib:$LD_LIBRARY_PATH

PYCRTOOLS=$LOFARSOFT/src/PyCRTools

# Run cr_physics pipeline
/usr/bin/python -u $PYCRTOOLS/extras/get_event_ids.py --host coma00.science.ru.nl --user crdb --password crdb --dbname crdb -s NEW > $HOME/cr_physics_new

JOBS=$(sed -n '$=' $HOME/cr_physics_new)

echo "processing $JOBS NEW events"

/usr/local/slurm/bin/sbatch -p short --array 1-6000%${MAX_SIMULTANEOUS_JOBS} $PYCRTOOLS/jobs/cr_physics.sh

# Run cr_event pipeline
#/usr/bin/python -u $PYCRTOOLS/extras/get_event_ids.py --host coma00.science.ru.nl --user crdb --password crdb --dbname crdb -a -s NEW > $HOME/cr_event_new
#
#JOBS=$(sed -n '$=' $HOME/cr_event_new)
#
#echo "processing $JOBS NEW events"
#
#/usr/local/slurm/bin/sbatch --array 1-${JOBS}%${MAX_SIMULTANEOUS_JOBS} $PYCRTOOLS/jobs/cr_event.sh

