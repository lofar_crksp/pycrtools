#! /bin/bash

MAX_SIMULTANEOUS_JOBS=24 # 64 gives Postgres server overload?? (Oct 2019)

export LOFARSOFT=/vol/optcoma/pycrtools
export PYTHONPATH=$LOFARSOFT/release/lib/python:$PYTHONPATH
export LD_LIBRARY_PATH=$LOFARSOFT/release/lib:$LD_LIBRARY_PATH

PYCRTOOLS=$LOFARSOFT/src/PyCRTools

# Run cr_physics pipeline
/usr/bin/python -u $PYCRTOOLS/extras/get_event_ids.py --host coma00.science.ru.nl --user crdb --password crdb --dbname crdb -s NEW | head -n9999 > $HOME/cr_physics_new
sleep 5

JOBS=$(sed -n '$=' $HOME/cr_physics_new)

echo "processing $JOBS NEW events"

/usr/local/slurm/bin/sbatch -p normal --array 1-${JOBS}%${MAX_SIMULTANEOUS_JOBS} $PYCRTOOLS/jobs/cr_physics_test_cal.sh

# Run cr_event pipeline
#/usr/bin/python -u $PYCRTOOLS/extras/get_event_ids.py --host coma00.science.ru.nl --user crdb --password crdb --dbname crdb -a -s NEW > $HOME/cr_event_new
#
#JOBS=$(sed -n '$=' $HOME/cr_event_new)
#
#echo "processing $JOBS NEW events"
#
#/usr/local/slurm/bin/sbatch --array 1-${JOBS}%${MAX_SIMULTANEOUS_JOBS} $PYCRTOOLS/jobs/cr_event.sh

