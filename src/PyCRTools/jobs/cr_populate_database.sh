#! /bin/bash

#SBATCH --time=12:00:00

export LOFARSOFT=/vol/optcoma/lofarsoft
export PYTHONPATH=$LOFARSOFT/release/lib/python:$PYTHONPATH
export LD_LIBRARY_PATH=$LOFARSOFT/release/lib:$LD_LIBRARY_PATH

PYCRTOOLS=$LOFARSOFT/src/PyCRTools

BASE_PATH=/vol/astro3/lofar/vhecr/lora_triggered
DB_PATH=$BASE_PATH
DB_FILE=$DB_PATH/bogus
DATA_PATH=$BASE_PATH/data
RESULTS_PATH=$BASE_PATH/results
LORA_PATH=$BASE_PATH/LORA
LOG_PATH=$BASE_PATH/log

# Populate database (with new files only)
/usr/bin/python $PYCRTOOLS/scripts/run_crdb_populate.py -d $DATA_PATH -r $RESULTS_PATH -l $LORA_PATH --host coma00.science.ru.nl --user crdb --password crdb --dbname crdb  --logpath $LOG_PATH $DB_FILE

/usr/bin/python /opt/lofarsoft/src/PyCRTools/scripts/add_beam_direction.py

