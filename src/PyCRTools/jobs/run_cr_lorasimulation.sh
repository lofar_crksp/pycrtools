#! /bin/bash

MAX_SIMULTANEOUS_JOBS=20

export LOFARSOFT=/vol/optcoma/pycrtools
export PYTHONPATH=$LOFARSOFT/release/lib/python:$PYTHONPATH
export LD_LIBRARY_PATH=$LOFARSOFT/release/lib:$LD_LIBRARY_PATH

PYCRTOOLS=$LOFARSOFT/src/PyCRTools

# Run cr_lorasimulation pipeline
/usr/bin/python -u $PYCRTOOLS/extras/get_event_ids.py --host coma00.science.ru.nl --user crdb --password crdb --dbname crdb --simulation-status=COREAS_DONE > $HOME/eventlist_coreasdone.txt

sleep 5 # file cr_physics_new seems not to be available instantly???? (AC)

JOBS=$(sed -n '$=' $HOME/eventlist_coreasdone.txt)

echo "processing $JOBS COREAS_DONE events for cr_lorasimulation"

/usr/local/slurm/bin/sbatch -p normal --array 1-${JOBS}%${MAX_SIMULTANEOUS_JOBS} $PYCRTOOLS/jobs/cr_lorasimulation.sh

# Run cr_event pipeline
#/usr/bin/python -u $PYCRTOOLS/extras/get_event_ids.py --host coma00.science.ru.nl --user crdb --password crdb --dbname crdb -a -s NEW > $HOME/cr_event_new
#
#JOBS=$(sed -n '$=' $HOME/cr_event_new)
#
#echo "processing $JOBS NEW events"
#
#/usr/local/slurm/bin/sbatch --array 1-${JOBS}%${MAX_SIMULTANEOUS_JOBS} $PYCRTOOLS/jobs/cr_event.sh

