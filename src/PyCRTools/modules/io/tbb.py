"""This module implements the primary interface for reading LOFAR TBB data.

It contains one function `open` that is used to open an HDF5 file containing LOFAR TBB data and returns a :class:`~TBBData` file object.

.. moduleauthor:: Pim Schellart <P.Schellart@astro.ru.nl>

This module assumes that correct ICD specified values are set for the folowing attributes:

* ANTENNA_SET
* ANTENNA_POSITION
* ANTENNA_POSITION_UNIT
* NYQUIST_ZONE

If this is not true for your files use the ``fix_metadata.py`` script to fix this.

"""

import os
import numpy as np
import pycrtools as cr
from datetime import datetime
from pycrtools import metadata as md

# This class implements the IO interface
from interfaces import IOInterface, TBBDataInterface

import h5py

nyquist_zone = {'LBA_10_90' : 1, 'LBA_30_90' : 1, 'HBA_110_190' : 2, 'HBA_170_230' : 3, 'HBA_210_250' : 3}

conversiondict = {"": 1, "kHz": 1000, "MHz": 10 ** 6, "GHz": 10 ** 9, "THz": 10 ** 12}

class TBBDataInterface_TMP(TBBDataInterface):
    """
    Temporary TBDataInterface base class with functions common to all h5py based classed.
    To be merged with TBBDataINterface when old DAL C++ implementation is removed.
    """
    def __init__(self, filename, blocksize=1024, block=0):
        TBBDataInterface.__init__(self, blocksize=1024, block=0)
        self._filename = filename
        self._dipoleNames = []
        self._stationKeys = []
        self._antennaPositions = []
        self._shift = 0

        self.__referenceAntenna = None

        # Define Keyword dict with keywords that have to be present.
        self._keyworddict = {
            "ALIGNMENT_REFERENCE_ANTENNA": self._alignmentReferenceAntenna,
            "SCR_ANTENNA_POSITION": self.getStationCenterRelativeAntennaPosition,
            "LCR_ANTENNA_POSITION": self.getLofarCenterRelativeAntennaPosition,
            "ITRF_ANTENNA_POSITION": self.getItrfAntennaPosition,
            "TIMESERIES_DATA": lambda: (lambda x: x if self.getTimeseriesData(x) else x)(self.empty("TIMESERIES_DATA")),
            "TIME_DATA": self.getTimeData,
            "FREQUENCY_DATA": self.getFrequencies,
            "FFT_DATA": lambda: (lambda x: x if self.getFFTData(x) else x)(self.empty("FFT_DATA")),
            "EMPTY_TIMESERIES_DATA": lambda: self.empty("TIMESERIES_DATA"),
            "EMPTY_FFT_DATA": lambda: self.empty("FFT_DATA"),
            "SAMPLE_FREQUENCY": lambda: [v * conversiondict[u] for v, u in zip(self['SAMPLE_FREQUENCY_VALUE'],self['SAMPLE_FREQUENCY_UNIT'])],
            "SAMPLE_INTERVAL": lambda: [1. / (v * conversiondict[u]) for v, u in zip(self['SAMPLE_FREQUENCY_VALUE'],self['SAMPLE_FREQUENCY_UNIT'])],
            "FREQUENCY_INTERVAL": lambda: [v * conversiondict[u] / self["BLOCKSIZE"] for v, u in zip(self['SAMPLE_FREQUENCY_VALUE'],self['SAMPLE_FREQUENCY_UNIT'])],
            "FREQUENCY_RANGE": lambda: [(1/f / 2 * (n - 1), 1/f / 2 * n) for f, n in zip(self["SAMPLE_INTERVAL"], self["NYQUIST_ZONE"])],
            "STATION_NAME": lambda: [md.idToStationName(i / 1000000) for i in self["CHANNEL_ID"]],
            "FFTSIZE": lambda: self["BLOCKSIZE"] / 2 + 1,
            "BLOCKSIZE": lambda: self._blocksize,
            "BLOCK": lambda: self._block,
            "MAXIMUM_READ_LENGTH": self._notImplemented,
            "TIME_HR": lambda: [str(datetime.utcfromtimestamp(t)) for t in self["TIME"]],
            "SELECTED_DIPOLES_INDEX": lambda: [i for i, n in enumerate(self["DIPOLE_NAMES"]) if n in self["SELECTED_DIPOLES"]],

            # ICD KEYWORDS
            "FILENAME": self._filename,
            "ANTENNA_SET": self._notImplemented,
            "ANTENNA_POSITION": self._antennaPositions,
            "ANTENNA_POSITION_ITRF": self.__antennaPositionsITRF,
            "NYQUIST_ZONE": lambda: [nyquist_zone[self["FILTER_SELECTION"]] for i in self["SELECTED_DIPOLES"]],
            "TIME": self._notImplemented,
            "SAMPLE_NUMBER": self._notImplemented,
            "SAMPLE_FREQUENCY_VALUE": self._notImplemented,
            "SAMPLE_FREQUENCY_UNIT": self._notImplemented,
#            "CABLE_DELAY": self._get_dipoleAttributes('CABLE_DELAY', 'f'),
#            "CABLE_DELAY_UNIT":self._get_dipoleAttributes('CABLE_DELAY_UNIT', '|S5'),
            "DATA_LENGTH":  self._notImplemented,
            "NOF_STATION_GROUPS": lambda: len(self._stationKeys),
            "NOF_DIPOLE_DATASETS": lambda: len(self._dipoleNames),
            "NOF_SELECTED_DATASETS": lambda: len(self.selectedDipoles()),
            "DIPOLE_NAMES": self._dipoleNames,
            "SELECTED_DIPOLES": self.selectedDipoles,
            "CHANNEL_ID": self._channelId,
            "FILETYPE":self._notImplemented,
            "FILEDATE":self._notImplemented,
            "TELESCOPE":self._notImplemented,
            "OBSERVER": self._notImplemented,
            "CLOCK_OFFSET": self.getClockOffset,
            "CLOCK_FREQUENCY": self._notImplemented,
            "CLOCK_FREQUENCY_UNIT":self._notImplemented,
            "FILTER_SELECTION":self._notImplemented,
            "TARGET":self._notImplemented,
            "SYSTEM_VERSION":self._notImplemented,
            "PIPELINE_NAME": self._notImplemented,
            "PIPELINE_VERSION":self._notImplemented,
            "NOTES":self._notImplemented,
            "PROJECT_ID":self._notImplemented,
            "PROJECT_TITLE":self._notImplemented,
            "PROJECT_PI":self._notImplemented,
            "PROJECT_CO_I":self._notImplemented,
            "PROJECT_CONTACT":self._notImplemented,
            "OBSERVATION_ID":self._notImplemented,
            "OBSERVATION_START_MJD": self._notImplemented,
            "OBSERVATION_START_TAI":self._notImplemented,
            "OBSERVATION_START_UTC": self._notImplemented,
            "OBSERVATION_END_MJD": self._notImplemented,
            "OBSERVATION_END_TAI": self._notImplemented,
            "OBSERVATION_END_UTC":self._notImplemented,
            "OBSERVATION_NOF_STATIONS": self._notImplemented,
            "OBSERVATION_STATION_LIST": self._notImplemented,
            "OBSERVATION_FREQUENCY_MIN": self._notImplemented,
            "OBSERVATION_FREQUENCY_MAX": self._notImplemented,
            "OBSERVATION_FREQUENCY_CENTER": self._notImplemented,
            "OBSERVATION_FREQUENCY_UNIT":self._notImplemented,
            "CABLE_LENGTH": lambda: md.get("CableLength", self.__selectedDipoles, self.antenna_set, True),
            "CABLE_ATTENUATION": lambda: md.get("CableAttenuation", self.__selectedDipoles, self.antenna_set, True),
#            "STATION_PHASE_CALIBRATION": lambda: md.get("StationPhaseCalibration", self.__selectedDipoles, self.antenna_set, True),
            "STATION_GAIN_CALIBRATION": lambda: md.get("StationGainCalibration", self.__selectedDipoles, self.antenna_set, True),
            # TODO: Remove nonsense transform from numpy.flaot32 to float for
            # hArray
            "DIPOLE_CALIBRATION_DELAY": self._notImplemented,
            "DIPOLE_CALIBRATION_DELAY_UNIT": self._notImplemented,
                }

    def __antennaPositionsITRF(self):
        # The antenna positions are already in ITRF in the h5 file
        return self._antennaPositions

    def getDipoleInfo(self):
        """
        """
        raise NotImplementedError


    def _alignmentReferenceAntenna(self):
        # The reference antenna is the antenna that gets data last
        #if self.__referenceAntenna is None:
        # NB. Ref. antenna can / should change with selection. So not just set once.
        t = self['TIME']
        sn = self['SAMPLE_NUMBER']
        f = self['SAMPLE_FREQUENCY_VALUE']
        ts = t + sn / f
        self.__referenceAntenna = int(np.argmax(ts))  # TODO: remove  int conversion, introduced only for type check
        return self.__referenceAntenna

    def _channelId(self):
        # channel IDs are the dipole names as int
        cids = [int(s) for s in self.__selectedDipoles]
        return cids

    def _calcalignment_offset(self, referenceAntenna):
        return -1 * self._sample_offset(referenceAntenna)

    def _sample_offset(self, referenceAntenna):
        refTime = self['TIME'][referenceAntenna]
        refSample = self['SAMPLE_NUMBER'][referenceAntenna]
        clock = self['CLOCK_FREQUENCY'] * conversiondict[self['CLOCK_FREQUENCY_UNIT']]
        offset = (self['TIME'] - refTime) / clock + self['SAMPLE_NUMBER'] - refSample
        return offset



    def getFFTData(self, data, block=-1, hanning=True, hanning_fraction=1.0, datacheck=False):
        """Writes FFT data for selected antennas to data array.

        Required Arguments:

        ================== =================================================
        Parameter          Description
        ================== =================================================
        *data*             data array to write FFT data to.
        *block*            index of block to return data from.
        *hanning*          apply Hannnig filter to timeseries data before
                           the FFT.
        *hanning_fraction* fraction of the window to hanning filter at start
                           and end
        *datacheck*        check for blocks of consecutive zeros indicating
                           data loss
        ================== =================================================

        Output:
        a two dimensional array containing the FFT data of the
        specified block for each of the selected antennae and
        for the selected frequencies.
        So that if `a` is the returned array `a[i]` is an array of
        length (number of frequencies) of antenna i.

        """
        block = cr.asval(block)

        if block < 0:
            block = self._block
        else:
            self._block = block

        # Get timeseries data
        self.getTimeseriesData(self._scratch, block, datacheck=datacheck)

        # Apply Hanning filter
        if hanning:
            self._scratch[...].applyhanningfilter(0.5, int(self._blocksize * (1.0 - hanning_fraction)))

        # Perform FFT
        cr.hFFTWExecutePlan(data, self._scratch, self._plan)

        # Swap Nyquist zone if needed
        data[...].nyquistswap(self["NYQUIST_ZONE"][0])

    def setAntennaSelection(self, selection):
        """Sets the antenna selection used in subsequent calls to
        `getAntennaPosition`, `getFFTData`, `getTimeseriesData`.

        Required Arguments:

        =========== =================================================
        Parameter   Description
        =========== =================================================
        *selection* Either Python list (or hArray, Vector)
                    with index of the antenna as
                    known to self (integers (e.g. ``[1, 5, 6]``))
                    Or list of IDs to specify a LOFAR dipole
                    (e.g. ``['142000005', '3001008']``)
                    or say ``odd`` or ``even`` to select odd or even
                    antennas.
        =========== =================================================

        Output:
        This method does not return anything.

        Raises:
        It raises a `ValueError` if antenna selection cannot be set
        to requested value (e.g. specified antenna not in file).

        Example:
           file["SELECTED_DIPOLES"]="odd"
        """
        if isinstance(selection, str):
            if selection.upper() == "ODD1" or selection.upper() == "ODD2" or selection.upper() == "EVEN1" or selection.upper() == "EVEN2":
                dipole_names_index =  [i for i, n in enumerate(self["DIPOLE_NAMES"])]
                if 'HBA' not in self['ANTENNA_SET']:
                    raise ValueError("This selection is only used during HBA observations.")
            if selection.upper() == "ODD":
                selection = list(cr.hArray(self["DIPOLE_NAMES"]).Select("odd"))
            elif selection.upper() == "EVEN":
                selection = list(cr.hArray(self["DIPOLE_NAMES"]).Select("even"))
            elif selection.upper() == "ALL":
                selection = list(cr.hArray(self["DIPOLE_NAMES"]))
            elif selection.upper() == "ODD1":
                selection = list(cr.hArray(dipole_names_index).Select("odd").Select("<",48))
            elif selection.upper() == "ODD2":
                selection = list(cr.hArray(dipole_names_index).Select("odd").Select(">",47))
            elif selection.upper() == "EVEN1":
                selection = list(cr.hArray(dipole_names_index).Select("even").Select("<",48))
            elif selection.upper() == "EVEN2":
                selection = list(cr.hArray(dipole_names_index).Select("even").Select(">",47))
            else:
                raise ValueError("Selection needs to be a list of IDs or 'odd' or 'even'.")
        elif type(selection) in cr.hAllContainerTypes:
            selection = list(selection.vec())

        if not isinstance(selection, list):
            raise ValueError("Selection needs to be a list.")

        if not len(selection) > 0:
            raise ValueError("No antennas selected.")

        if isinstance(selection[0], int) or isinstance(selection[0], long):
            # Selection by antenna number
            self.__selectedDipoles = [self._dipoleNames[i] for i in selection if i < len(self._dipoleNames)]

        elif isinstance(selection[0], str) or isinstance(selection[0], unicode):
            # Selection by antenna ID
            self.__selectedDipoles = [antennaID for antennaID in selection if antennaID in self._dipoleNames]

        else:
            raise ValueError("No antenna ID or number found.")

        self.reInitializeKeys()
        self._initSelection()

    def _initSelection(self):
        """Selection dependent initialization.
        """
        self._alignment_offset = cr.hArray(self._calcalignment_offset(self['ALIGNMENT_REFERENCE_ANTENNA']))

        # Generate scrach arrays
        self._scratch = cr.hArray(float, dimensions=(self["NOF_SELECTED_DATASETS"], self._blocksize))
        self._scratchFFT = cr.hArray(complex, dimensions=(self["NOF_SELECTED_DATASETS"], self._blocksize / 2 + 1))
        self.nof_consecutive_zeros = cr.hArray(int,self["NOF_SELECTED_DATASETS"])

        # Generate FFTW plan
        self._plan = cr.FFTWPlanManyDftR2c(self._blocksize, self["NOF_SELECTED_DATASETS"], 1, self._blocksize, 1, self._blocksize / 2 + 1, cr.fftw_flags.ESTIMATE)

    def reInitializeKeys(self):
        raise NotImplementedError
    
    def selectedDipoles(self):
        return self.__selectedDipoles



class TBBData_DAL2(TBBDataInterface_TMP):
    def __init__(self, filename, blocksize=1024, block=0):
        TBBDataInterface_TMP.__init__(self, filename, blocksize=1024, block=0)
        # DAL2 has 'DIPOLE_' in front of the number. We have to remove this
        self.__dipoleKeyPrefix = 'DIPOLE_'
        self.__file = h5py.File(filename, "r")
        self.antenna_set = self.__file.attrs['ANTENNA_SET'][0]
        self.__maximumReadLength = None

        #Fill the lists self._dipoleNames, self._stationKeys and self._antennaPositions.
        self.getStationKeys()
        self.__setKeywordDict()

        # select all dipoles by default
        self.setAntennaSelection(self._dipoleNames)



    def getStationKeys(self):
        self._stationKeys = [s for s in self.__file.keys() if s.startswith('STATION')]
        for station in self._stationKeys:
            self._dipoleNames.extend([d.strip(self.__dipoleKeyPrefix) for d in self.__file[station].keys()])
            for dipole in self.__file[station].keys():
                if 'ANTENNA_POSITION' in self.__file[station][dipole].attrs.keys():
                    self._antennaPositions.append(self.__file[station][dipole].attrs['ANTENNA_POSITION'])
                else:
                    print('WARNING: ANTENNA_POSITION NOT SET. Fixing on-the fly ...')
                    self._antennaPositions.append(['TODO: SET CORRECT ANTENNA POSITION'])

    def __getMaximumReadLength(self):
        #if self.__maximumReadLength is None:
        # (AC) Max read length may change if selection of dipoles changes. So is not set-once.
        length = self._get_dipoleAttributes('DATA_LENGTH', 'i')
        offset = self._sample_offset(self._alignmentReferenceAntenna())
        corLength = length + offset
        self.__maximumReadLength = int(corLength.min()) - self._shift
        
        return self.__maximumReadLength

    def _get_dipoleAttributes(self, attribute, dtype):
        """
        Loop over all dipoles and return array of attributes.
        """
        attributearray = np.zeros(len(self._dipoleNames), dtype=dtype)
        i = 0
        try:
            for station in self._stationKeys:
                for dipole in self.__file[station].keys():
                    attributearray[i] = self.__file[station][dipole].attrs[attribute]
                    i+=1
        except:
            print('WARNING: KEYWORD', attribute, 'NOT PRESENT')
        return attributearray


    def __setKeywordDict(self):
        print 'Set keyword dict (TBBData_DAL2)'
        self._keyworddict["MAXIMUM_READ_LENGTH"] = self.__getMaximumReadLength

        # ICD KEYWORDS
        self._keyworddict["ANTENNA_SET"] = self.antenna_set
        self._keyworddict["ANTENNA_POSITION"] = self._antennaPositions
        self._keyworddict["TIME"] = self._get_dipoleAttributes('TIME', 'i')
        self._keyworddict["SAMPLE_NUMBER"] = self._get_dipoleAttributes('SAMPLE_NUMBER', 'i')
        self._keyworddict["SAMPLE_FREQUENCY_VALUE"] = list(self._get_dipoleAttributes('SAMPLE_FREQUENCY', 'f'))

        self._keyworddict["SAMPLE_FREQUENCY_UNIT"] = list(self._get_dipoleAttributes('SAMPLE_FREQUENCY_UNIT', '|S5'))
  
        self._keyworddict["DATA_LENGTH"] =  self._get_dipoleAttributes('DATA_LENGTH', 'i')

        self._keyworddict["FILETYPE"] =self.__file.attrs['FILETYPE'][0]
        self._keyworddict["FILEDATE"] =self.__file.attrs['FILEDATE'][0]
        self._keyworddict["TELESCOPE"] =self.__file.attrs['TELESCOPE'][0]
        self._keyworddict["OBSERVER"] = "NOT AVAILABLE IN DAL2"

        self._keyworddict["CLOCK_FREQUENCY"] = float(self.__file.attrs['CLOCK_FREQUENCY'])
        self._keyworddict["CLOCK_FREQUENCY_UNIT"] =self.__file.attrs['CLOCK_FREQUENCY_UNIT']
        self._keyworddict["FILTER_SELECTION"] =self.__file.attrs['FILTER_SELECTION']
        self._keyworddict["TARGET"] =self.__file.attrs['TARGETS']
        self._keyworddict["SYSTEM_VERSION"] =self.__file.attrs['SYSTEM_VERSION']
        self._keyworddict["PIPELINE_NAME"] = "NOT AVAILABLE IN DAL2"
        self._keyworddict["PIPELINE_VERSION"] = "NOT AVAILABLE IN DAL2"
        self._keyworddict["NOTES"] =self.__file.attrs['NOTES']
        self._keyworddict["PROJECT_ID"] =self.__file.attrs['PROJECT_ID']
        self._keyworddict["PROJECT_TITLE"] =self.__file.attrs['PROJECT_TITLE']
        self._keyworddict["PROJECT_PI"] =self.__file.attrs['PROJECT_PI']
        self._keyworddict["PROJECT_CO_I"] =self.__file.attrs['PROJECT_CO_I']
        self._keyworddict["PROJECT_CONTACT"] =self.__file.attrs['PROJECT_CONTACT']

        self._keyworddict["OBSERVATION_ID"] =self.__file.attrs['OBSERVATION_ID']
        self._keyworddict["OBSERVATION_START_MJD"] = self.__file.attrs['OBSERVATION_START_MJD']
        self._keyworddict["OBSERVATION_START_TAI"] = "NOT AVAILABLE IN DAL2"
        self._keyworddict["OBSERVATION_START_UTC"] = self.__file.attrs['OBSERVATION_START_UTC']
        self._keyworddict["OBSERVATION_END_MJD"] = self.__file.attrs['OBSERVATION_END_MJD']
        self._keyworddict["OBSERVATION_END_TAI"] = "NOT AVAILABLE IN DAL2"
        self._keyworddict["OBSERVATION_END_UTC"] =self.__file.attrs['OBSERVATION_END_UTC']

        self._keyworddict["OBSERVATION_NOF_STATIONS"] =int(self.__file.attrs['OBSERVATION_NOF_STATIONS'])
        self._keyworddict["OBSERVATION_STATION_LIST"] = list(self.__file.attrs['OBSERVATION_STATIONS_LIST'])
        self._keyworddict["OBSERVATION_FREQUENCY_MIN"] =float(self.__file.attrs['OBSERVATION_FREQUENCY_MIN'])
        self._keyworddict["OBSERVATION_FREQUENCY_MAX"] = float(self.__file.attrs['OBSERVATION_FREQUENCY_MAX'])
        self._keyworddict["OBSERVATION_FREQUENCY_CENTER"] = float(self.__file.attrs['OBSERVATION_FREQUENCY_CENTER'])
        self._keyworddict["OBSERVATION_FREQUENCY_UNIT"] = self.__file.attrs['OBSERVATION_FREQUENCY_UNIT']


        self._keyworddict["DIPOLE_CALIBRATION_DELAY"] = self._notImplemented
        self._keyworddict["DIPOLE_CALIBRATION_DELAY_UNIT"] = self._notImplemented

    def close(self):
        self.__file.close()

    def getTimeseriesData(self, data, block=-1, sample_offset=0, datacheck=False):
        """Returns timeseries data for selected antennas.

        Required Arguments:

        ============= =================================================
        Parameter     Description
        ============= =================================================
        *data*        data array to write timeseries data to.
        *block*       index of block to return data from. Use last set
                      block if not provided or None
        ============= =================================================

        Output:
        a two dimensional array containing the timeseries data of the
        specified block for each of the selected antennae.
        So that if `a` is the returned array `a[i]` is an array of
        length blocksize of antenna i.

        """
        block = cr.asval(block)

        if block < 0:
            block = self._block
        else:
            self._block = block

        # (AC, Oct 2019) Apply selection from SELECTED_DIPOLES to start/stop positions

        selected_indices = self["SELECTED_DIPOLES_INDEX"]
    
        startIdx = self._alignment_offset + block * self._blocksize + self._shift + sample_offset
        stopIdx = startIdx + self._blocksize
        
        #startIdx = startIdx[selected_indices]
        #stopIdx = stopIdx[selected_indices]
        # Selection is now automatically applied to all relevant keywords
        
        i = 0
        for station in self._stationKeys:
            for dipole in self.__file[station].keys():
                if dipole.strip(self.__dipoleKeyPrefix) in self.__selectedDipoles:
                    # read hdf5 data, returns a numpy array. Could be read
                    # directly into N dimensional final numpy structure
                    # fix read length in case of negative start 
                    # h5py
                    td = np.zeros(self._blocksize)
                    if startIdx[i] >= 0:
                        td[:] = self.__file[station][dipole][int(startIdx[i]):int(stopIdx[i])]
                    else:
                        start = 0
                        offset = -1 * int(startIdx[i])
                        length = self._blocksize - offset
                        td[:length] = self.__file[station][dipole][start:start+length]

                    # convert to hArray and copy to 2d hArray
                    hA = cr.hArray(np.asarray(td, dtype=float))
                    data[i] = hA
                    i+=1

        if datacheck:
            cr.hNumberOfConsecutiveZeros(self.nof_consecutive_zeros[...], data[...])


class TBBData_DAL1(TBBDataInterface_TMP):
    def __init__(self, filename, blocksize=1024, block=0):
        TBBDataInterface_TMP.__init__(self, filename, blocksize=1024, block=0)
        self.__file = h5py.File(filename, "r")
        self.antenna_set = self.__file.attrs['ANTENNA_SET'][0]
        self.__maximumReadLength = None

        #Fill the lists self._dipoleNames, self._stationKeys and self._antennaPositions.
        self.getStationKeys()

        # select all dipoles by default
        self.__setKeywordDict()
        self.setAntennaSelection(self._dipoleNames)

    def getStationKeys(self):
        self._stationKeys = [s for s in self.__file.keys() if s.startswith('Station')]
        for station in self._stationKeys:
            self._dipoleNames.extend(self.__file[station].keys())
            for dipole in self.__file[station].keys():
                if 'ANTENNA_POSITION_VALUE' in self.__file[station][dipole].attrs.keys():
                    self._antennaPositions.extend(self.__file[station][dipole].attrs['ANTENNA_POSITION_VALUE'])
                else:
                    print('WARNING: ANTENNA_POSITION NOT SET. Fixing on-the fly ...')
                    self._antennaPositions.append(['TODO: SET CORRECT ANTENNA POSITION'])

    def __getMaximumReadLength(self): # Want to have maximum read length over current selection of dipoles!
        #if self.__maximumReadLength is None:
        # (AC) Max read length may change if selection of dipoles changes. So is not set-once.

        length = self._get_dipoleAttributes('DATA_LENGTH', 'i')
        offset = self._sample_offset(self._alignmentReferenceAntenna())
        corLength = length + offset
        self.__maximumReadLength = int(corLength.min()) - self._shift

        return self.__maximumReadLength

    def _get_dipoleAttributes(self, attribute, dtype):
        """
        Loop over all dipoles and return array of attributes.
        """
        attributearray = np.zeros(len(self._dipoleNames), dtype=dtype)
        i = 0
        for station in self._stationKeys:
            for dipole in self.__file[station].keys():
                try:
                    attributearray[i] = self.__file[station][dipole].attrs[attribute][0]
                except KeyError:
                    print('WARNING: KEYWORD', attribute, 'NOT PRESENT at station {},  dipole {}'.format(station, dipole))
                i+=1
    
        # (AC, Oct 2019) Apply selection i.e. SELECTED_DIPOLES ! But only if it already exists.
        
        #if hasattr(self, "__selectedDipoles"):
        try:
            #self.selectedDipoles()
            attributearray = attributearray[self["SELECTED_DIPOLES_INDEX"]]
            #print 'Applying selection!!'
        except:
            #print 'First time!!'
            pass # At first time, selected dipoles is not set yet.
        
        return attributearray



    def __setKeywordDict(self):
            #print 'Set keyword dict (TBBData_DAL1)'
            self._keyworddict["MAXIMUM_READ_LENGTH"] = self.__getMaximumReadLength

            # ICD KEYWORDS
            self._keyworddict["ANTENNA_SET"] = self.antenna_set
            self._keyworddict["ANTENNA_POSITION"] = self._antennaPositions
            self._keyworddict["TIME"] = self._get_dipoleAttributes('TIME', 'i')
            self._keyworddict["SAMPLE_NUMBER"] = self._get_dipoleAttributes('SAMPLE_NUMBER', 'i')
            self._keyworddict["SAMPLE_FREQUENCY_VALUE"] = list(self._get_dipoleAttributes('SAMPLE_FREQUENCY_VALUE', 'f'))
            self._keyworddict["SAMPLE_FREQUENCY_UNIT"] = list(self._get_dipoleAttributes('SAMPLE_FREQUENCY_UNIT', '|S5'))
#           self._keyworddict[ "CABLE_DELAY"] = self._get_dipoleAttributes('CABLE_DELAY', 'f')
#           self._keyworddict[ "CABLE_DELAY_UNIT"] =self._get_dipoleAttributes('CABLE_DELAY_UNIT', '|S5')
            self._keyworddict["DATA_LENGTH"] =  self._get_dipoleAttributes('DATA_LENGTH', 'i')
            self._keyworddict["FILETYPE"] =self.__file.attrs['FILETYPE'][0]
            self._keyworddict["FILEDATE"] =self.__file.attrs['FILEDATE'][0]
            self._keyworddict["TELESCOPE"] =self.__file.attrs['TELESCOPE'][0]
            self._keyworddict["OBSERVER"] = self.__file.attrs['OBSERVER'][0]
            self._keyworddict["CLOCK_FREQUENCY"] = float(self.__file.attrs['CLOCK_FREQUENCY'][0])
            self._keyworddict["CLOCK_FREQUENCY_UNIT"] =self.__file.attrs['CLOCK_FREQUENCY_UNIT'][0]
            self._keyworddict["FILTER_SELECTION"] =self.__file.attrs['FILTER_SELECTION'][0]
            self._keyworddict["TARGET"] =self.__file.attrs['TARGET'][0]
            self._keyworddict["SYSTEM_VERSION"] =self.__file.attrs['SYSTEM_VERSION'][0]
            self._keyworddict["PIPELINE_NAME"] = self.__file.attrs['PIPELINE_NAME'][0]
            self._keyworddict["PIPELINE_VERSION"] =self.__file.attrs['PIPELINE_VERSION'][0]
            self._keyworddict["NOTES"] =self.__file.attrs['NOTES'][0]
            self._keyworddict["PROJECT_ID"] =self.__file.attrs['PROJECT_ID'][0]
            self._keyworddict["PROJECT_TITLE"] =self.__file.attrs['PROJECT_TITLE'][0]
            self._keyworddict["PROJECT_PI"] =self.__file.attrs['PROJECT_PI'][0]
            self._keyworddict["PROJECT_CO_I"] =self.__file.attrs['PROJECT_CO_I'][0]
            self._keyworddict["PROJECT_CONTACT"] =self.__file.attrs['PROJECT_CONTACT'][0]
            self._keyworddict["OBSERVATION_ID"] =self.__file.attrs['OBSERVATION_ID'][0]
            self._keyworddict["OBSERVATION_START_MJD"] = self.__file.attrs['OBSERVATION_START_MJD'][0]
            self._keyworddict["OBSERVATION_START_TAI"] =self.__file.attrs['OBSERVATION_START_TAI'][0]
            self._keyworddict["OBSERVATION_START_UTC"] = self.__file.attrs['OBSERVATION_START_UTC'][0]
            self._keyworddict["OBSERVATION_END_MJD"] = self.__file.attrs['OBSERVATION_END_MJD'][0]
            self._keyworddict["OBSERVATION_END_TAI"] = self.__file.attrs['OBSERVATION_END_TAI'][0]
            self._keyworddict["OBSERVATION_END_UTC"] =self.__file.attrs['OBSERVATION_END_UTC'][0]
            self._keyworddict["OBSERVATION_NOF_STATIONS"] =int(self.__file.attrs['OBSERVATION_NOF_STATIONS'][0])
            self._keyworddict["OBSERVATION_STATION_LIST"] = list(self.__file.attrs['OBSERVATION_STATIONS_LIST'])
            self._keyworddict["OBSERVATION_FREQUENCY_MIN"] =float(self.__file.attrs['OBSERVATION_FREQUENCY_MIN'][0])
            self._keyworddict["OBSERVATION_FREQUENCY_MAX"] = float(self.__file.attrs['OBSERVATION_FREQUENCY_MAX'][0])
            self._keyworddict["OBSERVATION_FREQUENCY_CENTER"] = float(self.__file.attrs['OBSERVATION_FREQUENCY_CENTER'][0])
            self._keyworddict["OBSERVATION_FREQUENCY_UNIT"] =self.__file.attrs['OBSERVATION_FREQUENCY_UNIT'][0]
            # TODO: Remove nonsense transform from numpy.flaot32 to float for
            # hArray
            self._keyworddict["DIPOLE_CALIBRATION_DELAY"] = [float(f) for f in self._get_dipoleAttributes("DIPOLE_CALIBRATION_DELAY_VALUE", 'f')]
            self._keyworddict["DIPOLE_CALIBRATION_DELAY_UNIT"] = list(self._get_dipoleAttributes("DIPOLE_CALIBRATION_DELAY_UNIT", '|S5'))


    def getTimeseriesData(self, data, block=-1, sample_offset=0, datacheck=False):
        """Returns timeseries data for selected antennas.

        Required Arguments:

        ============= =================================================
        Parameter     Description
        ============= =================================================
        *data*        data array to write timeseries data to.
        *block*       index of block to return data from. Use last set
                      block if not provided or None
        ============= =================================================

        Output:
        a two dimensional array containing the timeseries data of the
        specified block for each of the selected antennae.
        So that if `a` is the returned array `a[i]` is an array of
        length blocksize of antenna i.

        """
        block = cr.asval(block)

        if block < 0:
            block = self._block
        else:
            self._block = block

        # (AC, Oct 2019) Apply selection from SELECTED_DIPOLES to start/stop positions
        
        selected_indices = self["SELECTED_DIPOLES_INDEX"]
        
        startIdx = self._alignment_offset + block * self._blocksize + self._shift + sample_offset
        stopIdx = startIdx + self._blocksize
        
        #import pdb; pdb.set_trace()
        #startIdx = startIdx[selected_indices]
        #stopIdx = stopIdx[selected_indices]
        # Selection is now automatically applied to relevant keywords
        
        i = 0
        for station in self._stationKeys:
            for dipole in self.__file[station].keys():
                if dipole in self['SELECTED_DIPOLES']:
                    # read hdf5 data, returns a numpy array. Could be read
                    # directly into N dimensional final numpy structure
                    # fix read length in case of negative start 
                    # h5py
                    #print 'Dipole %d: %s; startIdx = %d, stopIdx = %d' % (i, dipole, int(startIdx[i]), int(stopIdx[i]))
                    td = np.zeros(self._blocksize)
                    if startIdx[i] >= 0:
                        #print 'Reading from startidx %d to stopidx %d' % (int(startIdx[i]), int(stopIdx[i]))
                        td[:] = self.__file[station][dipole][int(startIdx[i]):int(stopIdx[i])]
                    else:
                        start = 0
                        offset = -1 * int(startIdx[i])
                        length = self._blocksize - offset
                        #print 'Else. Length = %d' % length
                        td[:length] = self.__file[station][dipole][start:start+length]

                    # convert to hArray and copy to 2d hArray
                    hA = cr.hArray(np.asarray(td, dtype=float))
                    data[i] = hA
                    i+=1

        if datacheck:
            cr.hNumberOfConsecutiveZeros(self.nof_consecutive_zeros[...], data[...])

    #    def setAntennaSelection(self, selection):
    #        TBBDataInterface_TMP.setAntennaSelection(self, selection)
    #        print 'Done antenna selection superclass'
    #        self.__setKeywordDict()

    def reInitializeKeys(self):
        #print 'Reinitialize keys!'
        self.__setKeywordDict()

    def close(self):
        self.__file.close()


class MultiTBBData(IOInterface):
    """This class provides an interface to single file Transient Buffer
    Board data.
    """

    def __init__(self, filenames, blocksize=1024, block=0):
        """Constructor.
        """

        self.__files = [TBBData(fname, blocksize, block) for fname in filenames]
        self._blocksize = blocksize
        self._block = block
        self.__subsample_clockoffsets = None
        self.__override_clockoffsets = None
        self._shift = 0


        self._initSelection()

    def _initSelection(self):
        """ Set the alignment offsets to the actual values, on construction, and after changing antenna selections, blocksizes etc. i.e. whenever the lower TBBData._initSelection is called. The alignment offsets set here will override TBBData's __alignment_offset (which is a bit ugly).

        How can this be improved?
        """
        allStartSamples = np.array(self["SAMPLE_NUMBER"])
        station_startindex = self["STATION_STARTINDEX"]
        station_list = self["STATION_LIST"]

        self.nof_consecutive_zeros = cr.hArray(int, len(allStartSamples))

        alignment_offsets = [int(x) for x in (allStartSamples.max() - allStartSamples)]  # need to avoid np.int64's in list for hArray conversion
        for i in range(len(station_list)):
            start = station_startindex[i]
            end = station_startindex[i + 1]
            # import pdb; pdb.set_trace()
            thisStationsOffsets = cr.hArray(alignment_offsets[start:end])
            self.__files[i]._TBBData__alignment_offset = thisStationsOffsets

        self.applyClockOffsets()

    def __getitem__(self, key):
        """Implements keyword access.
        """

        if key in ["DIPOLE_NAMES", "SAMPLE_NUMBER", "DATA_LENGTH", "TIME", "DIPOLE_CALIBRATION_DELAY", "SELECTED_DIPOLES"]:
            ret = []
            for f in self.__files:
                ret.extend(f[key])
            return ret
        elif (key == "LCR_ANTENNA_POSITION") or (key == "ITRF_ANTENNA_POSITION"):
            perStation = [stat[key].toNumpy() for stat in self.__files]
            return cr.hArray(np.concatenate(perStation, axis=0))

        elif key == "MAXIMUM_READ_LENGTH":
            print 'Warning: just returning minimum MAXIMUM_READ_LENGTH over all included files.'
            print 'This may not be correct if alignment offsets are large...'
            ret = []
            for f in self.__files:
                ret.append(f[key])
            return min(ret)
        elif key == "CLOCK_OFFSET":
            return [f["CLOCK_OFFSET"][0] for f in self.__files]  # assume one station per file; return one number per station
        elif key == "SAMPLE_FREQUENCY_VALUE":
            return self.__files[0]["SAMPLE_FREQUENCY_VALUE"]
        elif key == "NYQUIST_ZONE":
            return [f["NYQUIST_ZONE"][0] for f in self.__files]
        elif key == "SUBSAMPLE_CLOCK_OFFSET":
            return self.__subsample_clockoffsets
        elif key == "FREQUENCY_DATA":
            return self.__files[0]["FREQUENCY_DATA"]
#        elif key == "TIMESERIES_DATA":
#            y = self.empty("TIMESERIES_DATA")
#            self.getTimeseriesData(y, block = self._block)
#            return y # memory leak!
        elif key == "ANTENNA_SET":
            return [f["ANTENNA_SET"] for f in self.__files]
        elif key == "NOF_SELECTED_DATASETS":
            return sum([f[key] for f in self.__files])
        elif key == "BLOCKSIZE":
            return self._blocksize
        # Extra key-words specific for multi-station TBB
        elif key == "STATION_LIST":
            ret = []
            for f in self.__files:  # assuming one station per file! Just taking RCU 0
                stationID = int(f["DIPOLE_NAMES"][0]) / 1000000
                stationName = md.idToStationName(stationID)
                ret.append(stationName)
            return ret
        elif key == "STATION_STARTINDEX":  # start (RCU) index of station i in dipole list / timeseries etc.
            ret = []
            nofDatasets = [f["NOF_SELECTED_DATASETS"] for f in self.__files]
            index = 0
            for nofForThisStation in nofDatasets:
                ret.append(index)
                index += nofForThisStation
            ret.append(index)  # Last item is end-index + 1, to be used in e.g. data[start:end] with end = station_startindex[i+1]
            return ret
        else:
            raise KeyError("Unsupported key " + key)

    setable_keywords = set(["BLOCKSIZE", "BLOCK", "SELECTED_DIPOLES"])  # ANTENNA_SET not implemented.

    def __setitem__(self, key, value):
        if key not in self.setable_keywords:
            raise KeyError("Invalid keyword '" + str(key) + "' - available keywords: " + str(list(self.setable_keywords)))

        elif key is "BLOCKSIZE":
            self._blocksize = value
            print 'Warning: user-applied alignment offsets are reset after setting blocksize!'
            print 'Should be fixed now... check'
            for f in self.__files:
                f["BLOCKSIZE"] = value
            self._initSelection()
        elif key is "BLOCK":
            self._block = value
        elif key is "SELECTED_DIPOLES":
            self.setAntennaSelection(value)
            self._initSelection()
            # self.applyClockOffsets()
#        elif key is "ANTENNA_SET":
#            self.antenna_set=value
        else:
            raise KeyError(str(key) + " cannot be set. Available keywords: " + str(list(self.setable_keywords)))

    def empty(self, key):
        """Return empty array for keyword data.
        Known keywords are: "TIMESERIES_DATA", "TIME_DATA", "FREQUENCY_DATA", "FFT_DATA".
        """

        nof_datasets = 0

        for f in self.__files:
            nof_datasets += f["NOF_SELECTED_DATASETS"]

        if key == "TIMESERIES_DATA":
            return cr.hArray(float, dimensions=(nof_datasets, self._blocksize), name="E-Field(t)", units=("", "Counts"))
        elif key == "FFT_DATA":
            return cr.hArray(complex, dimensions=(nof_datasets, self._blocksize / 2 + 1), name="fft(E-Field)", xvalues=self["FREQUENCY_DATA"], logplot="y")
        else:
            raise KeyError("Unknown key: " + str(key))

    def getTimeseriesData(self, data, block=-1, sample_offset=0):
        """Returns timeseries data for selected antennas.

        Required Arguments:

        ============= =================================================
        Parameter     Description
        ============= =================================================
        *data*        data array to write timeseries data to.
        *block*       index of block to return data from. Use last set
                      block if not provided or None
        ============= =================================================

        Output:
        a two dimensional array containing the timeseries data of the
        specified block for each of the selected antennae.
        So that if `a` is the returned array `a[i]` is an array of
        length blocksize of antenna i.

        """

        nof = [f["NOF_SELECTED_DATASETS"] for f in self.__files]

#        if not sample_offset:
#            sample_offset = [0 for i in self.__files]
        start = 0
        end = 0
        for i, f in enumerate(self.__files):
            end = end + nof[i]

#            f["BLOCKSIZE"] = self._blocksize # Removed: implicitly resets alignment offsets as well...

            if sample_offset != 0:
                print "reading data offset by", sample_offset, "samples"
            f.getTimeseriesData(data[start:end], block, sample_offset)

            start = end

    def shiftTimeseriesData(self, sample_offset):
        # needed to be able to call getFFTData per file from self.getFFTData.
        """ Apply integer-sample shifts to all opened files to compensate clock offsets.
        A positive number k shifts forward through the data, i.e. the first k samples are skipped.
        Required Arguments:

        ================= =================================================
        Parameter         Description
        ================= =================================================
        *sample_offset*   List containing integer sample offset for each opened file.
        ================= =================================================
        """

        for i, f in enumerate(self.__files):
            print 'Applying offset %d to file %s' % (sample_offset[i], self.__files[i]["FILENAME"])
            f.shiftTimeseriesData(sample_offset[i])

    def overrideClockOffsets(self, clockoffsets_dict):
        self.__override_clockoffsets = clockoffsets_dict
        print 'Set clock offsets to given dict values instead of metadata.py'
        self._initSelection()


    def applyClockOffsets(self):
        """ Get clock offsets from opened files; subtract off the smallest one; shift alignment offsets accordingly.
            No parameters.
        """
        if self.__override_clockoffsets is None:
            clockoffsets = np.array(self["CLOCK_OFFSET"])
        else:
            clockoffsets = []
            for i, station in enumerate(self["STATION_LIST"]):
                old_offset = self["CLOCK_OFFSET"][i]
                offset = self.__override_clockoffsets[station] if station in self.__override_clockoffsets.keys() else old_offset
                clockoffsets.append(offset)
            clockoffsets = np.array(clockoffsets)

        clockoffsets *= -1.0  # Get the sign right...!
        clockoffsets -= min(clockoffsets)  # make them all positive

        sample_offset = [int(x / 5.0e-9) for x in clockoffsets]
        self.__subsample_clockoffsets = clockoffsets - np.array(sample_offset) * 5.0e-9

        print 'Clock offsets, smallest one subtracted: '
        print clockoffsets
        print 'Sample offsets: '
        print sample_offset
        print 'Remaining sub-sample offsets: '
        print self.__subsample_clockoffsets

        self.shiftTimeseriesData(sample_offset)

    def getFFTData(self, data, block=-1, hanning=True, hanning_fraction=1.0, datacheck=False):
        """Writes FFT data for selected antennas to data array.
           Calls TBBData.getFFTData(...) per file and merges the output.
        Required Arguments:

        ============= =================================================
        Parameter     Description
        ============= =================================================
        *data*        data array to write FFT data to.
        *block*       index of block to return data from.
        *hanning*     apply Hannnig filter to timeseries data before
                      the FFT.
        ============= =================================================

        Output:
        a two dimensional array containing the FFT data of the
        specified block for each of the selected antennae and
        for the selected frequencies.
        So that if `a` is the returned array `a[i]` is an array of
        length (number of frequencies) of antenna i.

        """
        nof = [f["NOF_SELECTED_DATASETS"] for f in self.__files]

#        if not sample_offset:
#            sample_offset = [0 for i in self.__files]

        start = 0
        end = 0
        if datacheck:
            print 'Warning: datacheck for missing packets not implemented for MultiTBBData'

        for i, f in enumerate(self.__files):
            end = end + nof[i]

#            f["BLOCKSIZE"] = self._blocksize

 #           print "reading data offset by", sample_offset[i], "samples"
            f.getFFTData(data[start:end], block, hanning, hanning_fraction)

            start = end

    def setAntennaSelection(self, selection):
        """Sets the antenna selection used in subsequent calls to
        `getFFTData`, `getTimeseriesData`, `getItrfAntennaPosition`, etc.

        Required Arguments:

        =========== =================================================
        Parameter   Description
        =========== =================================================
        *selection* Either Python list (or hArray, Vector)
                    with index of the antenna as
                    known to self (integers (e.g. ``[1, 5, 6]``))
                    Or list of IDs to specify a LOFAR dipole
                    (e.g. ``['142000005', '3001008']``)
                    or say ``odd`` or ``even`` to select odd or even
                    antennas.
        =========== =================================================

        Output:
        This method does not return anything.

        Raises:
        It raises a `ValueError` if antenna selection cannot be set
        to requested value (e.g. specified antenna not in file).

        Example:
           file["SELECTED_DIPOLES"]="odd"
        """
        if isinstance(selection, str):
            if selection.upper() == "ODD":
                selection = list(cr.hArray(self["DIPOLE_NAMES"]).Select("odd"))
            elif selection.upper() == "EVEN":
                selection = list(cr.hArray(self["DIPOLE_NAMES"]).Select("even"))
            else:
                raise ValueError("Selection needs to be a list of IDs or 'odd' or 'even'.")
        elif type(selection) in cr.hAllContainerTypes:
            selection = list(selection.vec())

        if not isinstance(selection, list):
            raise ValueError("Selection needs to be a list.")

        if not len(selection) > 0:
            raise ValueError("No antennas selected.")

        if isinstance(selection[0], int) or isinstance(selection[0], long):
            # Selection by antenna number
            # convert selection to list of dipole names
            selection = [self["DIPOLE_NAMES"][i] for i in selection if i < len(self["DIPOLE_NAMES"])]  # self.__nofDipoleDatasets - implement?

        elif isinstance(selection[0], str):
            pass  # selection will be checked below
            # Selection by antenna ID
#            self.__selectedDipoles = [antennaID for antennaID in selection if antennaID in self["DIPOLE_NAMES"]]

        else:
            raise ValueError("No antenna ID or number found.")

        # Apply selection
        # For every file, this is the subset of the overall 'selection' that is present in file._dipoleNames
        # Count to see if the conjunction of subsets matches the overall set.
        count = 0
        for f in self.__files:
            thisSelection = [x for x in selection if x in f["DIPOLE_NAMES"]]
            f.setAntennaSelection(thisSelection)  # call TBBData's method
            count += len(thisSelection)

        if count != len(selection):  # assume count < len(selection)...
            raise Exception("One or more antennas in selection are in none of the files.")


def open(filename, *args, **kwargs):
    """Open file with LOFAR TBB data.
    """

    if isinstance(filename, list) and len(filename)>1:
        return MultiTBBData(filename, *args, **kwargs)
    else:
        if isinstance(filename, list):
            filename = filename[0]
        if not os.path.isfile(filename):
            raise IOError("No such file or directory: " + filename)
        else:
            f = h5py.File(filename, "r")
            doc_version = f.attrs.get('DOC_VERSION')
            f.close()
            if doc_version is None:
                return TBBData_DAL1(filename, *args, **kwargs)
            else:
                return TBBData_DAL2(filename, *args, **kwargs)
