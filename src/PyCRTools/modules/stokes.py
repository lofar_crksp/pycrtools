import numpy as np

def stokes_parameters(x, y, hx, hy):
    """Stokes parameters given timeseries *x*, *y* in two orthogonal polarizations.
    """
    n = x.shape[0]

    I = (1./n) * np.sum(x*x + hx*hx + y*y + hy*hy)
    Q = (1./n) * np.sum(x*x + hx*hx - y*y - hy*hy)
    U = (2./n) * np.sum(x*y + hx*hy)
    V = (2./n) * np.sum(hx*y - x*hy)

    return np.array([I, Q, U, V])

def polarization_angle(S):

    Q = S[1]
    U = S[2]

    psi = (1./2.)*np.arctan2(U, Q)

    return psi

def degree_of_polarization(S):

    I = S[0]
    Q = S[1]
    U = S[2]
    V = S[3]

    p = np.sqrt(Q*Q + U*U + V*V) / I

    return p

def cov(x, y, i=0, j=0):
    """Autocovariance between *x* and *y* for time samples *i* and *j*.
    """

    N = x.shape[0]
    mu_x = np.mean(x)
    mu_y = np.mean(y)
    s = 0.0

    n = np.abs(i - j) # NB. Strictly speaking, taking the abs here assumes Cov(x_i, y_j) = Cov(x_j, y_i) which need not 
    # be true if x != y... right?

    # Do cases n >= 0 and n < 0 separately.
    
    if n >= 0:
        for k in range(N - n):
            
            s += (x[k+n] - mu_x) * (y[k] - mu_y) # i-j positive so x has a higher index than y
    else:
        for k in range(N - n):
            
            s += (x[k] - mu_x) * (y[k+n] - mu_y)
    

    if (N - n) - 1 == 0:
        # The sample mean is now determined almost independently (over all N samples)
        # so this might be a valid approximation
        return s / (N - n)
    else:
        return s / ((N - n) - 1)

def stokes_parameters_uncertainties(x, y, hx, hy):
    """Uncertainties on Stokes parameters given timeseries in both polarizations *x*, *y* and their Hilbert transforms *hx* and *hy*.
       See PhD Thesis of Harm Schoorlemmer, page 72.
    """

    n = x.shape[0]
    sigma_s = np.array([0.0, 0.0, 0.0, 0.0])

    for i in range(n):
        for j in range(n):
            sigma_s[0] += x[i]*x[j]*cov(x,x,i,j) + y[i]*y[j]*cov(y,y,i,j) + x[i]*y[j]*cov(x,y,i,j) + x[j]*y[i]*cov(x,y,j,i)
            sigma_s[1] += x[i]*x[j]*cov(x,x,i,j) + y[i]*y[j]*cov(y,y,i,j) - x[i]*y[j]*cov(x,y,i,j) - x[j]*y[i]*cov(x,y,j,i)
            sigma_s[2] += y[i]*y[j]*cov(x,x,i,j) + x[i]*x[j]*cov(y,y,i,j) + x[j]*y[i]*cov(x,y,i,j) + x[i]*y[j]*cov(x,y,j,i)
            sigma_s[3] += hy[i]*hy[j]*cov(x,x,i,j) + hx[i]*hx[j]*cov(y,y,i,j) + hx[i]*hy[j]*cov(x,y,i,j) + hx[j]*hy[i]*cov(x,y,j,i)

    return np.sqrt( (16./(n*n)) * sigma_s )

def polarization_angle_uncertainty(S, sigma_S):
    """Uncertainty on angle of polarization, given Stokes parameters *S* and *sigma_S* as calculated by stokes_parameters_uncertainties.
    """

    sigma_psi = (sigma_S[1]*S[2]*S[2] + sigma_S[2]*S[1]*S[1]) / (4*(S[2]*S[2] + S[1]*S[1])**2)

    return sigma_psi

