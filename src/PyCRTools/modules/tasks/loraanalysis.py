"""
Module documentation
====================

Fitting LORA data for new shower parameters. Currently implemented for different directions and core positions. Defaults are for set-up pf 20 LORA stations.
Run as (all parameters are of shape as default in database):

cr.trun("LoraAnalysis",particles_lora=particles_lora, azimuth=azimuth, elevation=elevation,core=core, core_type=core_type,use_title=True,save_plots=True,plot_prefix='./',plotlist=[])

Will return fit parameters as well as plots when they are stored by save_plots

.. moduleauthor:: Anna Nelles <a.nelles@astro.ru.nl>

"""

from pycrtools.tasks import Task
import pycrtools as cr
from pycrtools import lora
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import gamma
from scipy.optimize import leastsq

def get_distance(core, direction, positions):
    """
    Calculate distance in shower axis
    """

    distances = np.copy(positions)
    theta = np.deg2rad(direction[1]) # spherical coordinates: 0 = horizon, 90 = vertical
    phi = np.deg2rad(450 - direction[0]) # recalculate to 0 = east counter-clockwise

    distances = positions - core
    axis = np.array([np.cos(theta) * np.cos(phi), np.cos(theta) * np.sin(phi), np.sin(theta)])
    finaldistance = np.cross(distances, axis)
    dist = np.apply_along_axis(np.linalg.norm, 1, finaldistance)

    return dist


def get_uvw(pos, cx, cy, zen, az):
    relpos = pos - np.array([cx, cy, 0])
    inc = 1.1837
    B = np.array([0, np.cos(inc), -np.sin(inc)])
    v = np.array([-np.cos(az) * np.sin(zen), -np.sin(az) * np.sin(zen), -np.cos(zen)])
    vxB = np.array([v[1] * B[2] - v[2] * B[1], v[2] * B[0] - v[0] * B[2], v[0] * B[1] - v[1] * B[0]])
    vxB = vxB / np.linalg.norm(vxB)
    vxvxB = -np.array([v[1] * vxB[2] - v[2] * vxB[1], v[2] * vxB[0] - v[0] * vxB[2], v[0] * vxB[1] - v[1] * vxB[0]])

    return np.array([np.inner(vxB, relpos), np.inner(vxvxB, relpos), np.inner(v, relpos)]).T


lora_fit_function = lambda p, x: p[0] / 2 / np.pi / p[1] / p[1] * np.power(x / p[1], p[2] - 2) * np.power(1 + x / p[1], p[2] - 4.5) * gamma(4.5 - p[2]) / gamma(p[2]) / gamma(4.5 - 2 * p[2])


lora_error_function = lambda p, data, err, lpos, cx, cy, az, zen: (lora_ldf(p[0], p[1], 1.7, lpos, cx, cy, az, zen) - data) / err


def lora_ldf(nch, rm, s, lpos, cx, cy, az, zen):
    pos_lora_uvw = get_uvw(lpos, cx, cy, zen, az)
    r = np.sqrt(pos_lora_uvw[:, 0] * pos_lora_uvw[:, 0] + pos_lora_uvw[:, 1] * pos_lora_uvw[:, 1])
    return lora_fit_function([nch, rm, s], r)


def fit_lora_ldf(core_x, core_y, azimuth, elevation, particles, lora_x=[11.21, -29.79, -57.79, -3.79, -120.79, -82.79, -162.79, -134.79, 78.21, 155.21, 112.21, 133.21, 74.21, 118.21, 41.21, 80.21, -53.79, 3.21, -2.79, -48.79], lora_y=[-94.07, -83.07, -125.07, -158.07, 3.93, -40.07, -26.07, -74.07, 75.93, 50.93, 24.93, 100.93, -121.07, -93.07, -76.07, -40.07, 129.93, 111.93, 61.93, 56.93], lora_z=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]):

    # Gathering data in correct format
    core = np.array([core_x, core_y, 0.])
    positions = zip(lora_x, lora_y, lora_z)
    pos = np.array(positions)
    dat = np.array(particles)

    # Checking for zeros
    idx = np.argwhere(dat > 0.).ravel()

    data = dat[idx]
    err = np.sqrt(data)
    lpos = pos[idx]

    az = np.pi / 2. - np.pi / 180. * (azimuth)
    zen = np.pi / 2. - np.pi / 180. * (elevation)

    distances = get_distance(core, [azimuth, elevation], lpos)

    # Fitting
    fitargs = (data, err, lpos, core_x, core_y, az, zen)
    initguess = [1e7, 30.]  # inital guess
    q = leastsq(lora_error_function, initguess, args=fitargs)

    # Calculating chi2
    ef = lora_error_function(q[0], data, err, lpos, core_x, core_y, az, zen)
    chi = np.sum(ef ** 2)
    ndof = ef.shape[0] - 2
    red_chi = chi / ndof

    return q[0], chi, red_chi, ndof, distances


class LoraAnalysis(Task):

    """
    Refitting particle data using different shower parameters
    """

    parameters = dict(
        # Input parameters
        particles_lora=dict(default=None,
            doc="Database parameter lora_particle_density__m2"),
        lora_x=dict(default=[11.21, -29.79, -57.79, -3.79, -120.79, -82.79, -162.79, -134.79, 78.21, 155.21, 112.21, 133.21, 74.21, 118.21, 41.21, 80.21, -53.79, 3.21, -2.79, -48.79],
            doc="Database parameter lora_posx, Set to default to 20 stations set-up"),
        lora_y=dict(default=[-94.07, -83.07, -125.07, -158.07, 3.93, -40.07, -26.07, -74.07, 75.93, 50.93, 24.93, 100.93, -121.07, -93.07, -76.07, -40.07, 129.93, 111.93, 61.93, 56.93],
            doc="Database parameter lora_posy, Set to default to 20 stations set-up"),
        lora_z=dict(default=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            doc="Database parameter lora_posz, Set to default to 20 stations set-up"),
        azimuth=dict(default=None,
            doc="Azimuth of arriving cosmic ray (LOFAR convention)"),
        elevation=dict(default=None,
            doc="Elevation of arriving cosmic ray (LOFAR convention)"),
        core=dict(default=None,
            doc="New core position that should be fitted"),
        core_type=dict(default="LORA",
            doc="Type of core that is fitted, label on axis"),
        save_plots=dict(default=False,
            doc="Save the new particle LDF"),
        plot_prefix=dict(default='./',
            doc="Where to store the figures"),
        use_title=dict(default="True",
            doc="Plot with title"),
        plotlist=dict(default=[],
            doc="Plot list for website"),
        plot_type=dict(default=lambda self: "pdf" if self.plot_publishable else "png",
            doc="Plot type (e.g. png, jpeg, pdf)"),
        # OUPUT parameters
        lora_fit_parameters=dict(default=None,
            doc="Best fitting parameters of fit, N_charged, R_moliere", output=True),
        lora_red_chi=dict(default=None,
            doc="Reduced chi square of fit with new core", output=True),
        lora_chi=dict(default=None,
            doc="Chi square of fit with new core", output=True),
        lora_ndof=dict(default=None,
            doc="Number of degrees of freedom of fit", output=True)
    )

    def run(self):
        """Run the task.
        """

        self.particles = np.array(self.particles_lora)
        self.lora_fit_parameters, self.lora_chi, self.lora_red_chi, self.lora_ndof, self.distances = fit_lora_ldf(self.core[0], self.core[1], self.azimuth, self.elevation, self.particles, self.lora_x, self.lora_y, self.lora_z)

        # Plotting the new LDF
        plot_par = [self.lora_fit_parameters[0], self.lora_fit_parameters[1], 1.7]
        r = np.arange(500)

        p = plt.figure()
        plt.plot(lora_fit_function(plot_par, r), color='r', linewidth=2)

        idx = np.argwhere(self.particles > 0.).ravel()

        plt.errorbar(self.distances, self.particles[idx], yerr=np.sqrt(self.particles[idx]), marker='o', linestyle=' ', color='k')
        plt.yscale('log')
        plt.ylabel(r'Charged particle density [$\mathrm{m}^2]$')
        plt.xlabel('Distance from shower axis [m]')

        if self.use_title:
            plt.title("Particle LDF for " + self.core_type + " core")

        if self.save_plots:
            p = self.plot_prefix + "lora_ldf_new_core_" + self.core_type + ".{0}".format(self.plot_type)
            plt.savefig(p)
            self.plotlist.append(p)

