"""
Polarization
============

.. moduleauthor:: Pim Schellart <p.schellart@astro.ru.nl>

"""

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

import pickle

import pytmf

import pycrtools as cr

from pycrtools.tasks import Task
from pycrtools.tasks import antennaresponse
from pycrtools.tasks import pulseenvelope
from pycrtools.tasks import shower

from pycrtools import crdatabase as crdb
from pycrtools import stokes

from scipy.signal import hilbert

class Polarization(Task):
    """Calculates the angle of polarization :math:`\psi` for each antenna from the Stokes parameters (I, Q, U and V). A quiver plot, with arrows representing the polarization angles, is optionally created and stored.

    .. note:: The task in its current form assumes that the calibrated pulse block is stored by the pipeline in the standard location.
    
    .. seealso:: Schellart et al., Polarized radio emission from extensive air showers measured with LOFAR, Journal of Cosmology and Astroparticle Physics, Issue 10, article id. 014, pp. (2014).
    """

    parameters = dict(
        eventid=dict(default = None, output = False,
            doc = "Event ID"),
        event=dict(default = lambda self : crdb.Event(db=self.db, id=self.eventid), output = False,
            doc = "Event object"),
        data_path = dict(default = lambda self : "/vol/astro3/lofar/vhecr/lora_triggered/results/{0}".format(self.eventid), output = False,
            doc = "Path where xyz_calibrated_pulse_block is located and where output will be stored"),
        include_stations = dict(default=["CS001", "CS002", "CS003", "CS004", "CS005", "CS006", "CS007", "CS011", "CS013", "CS017", "CS021"], output = False,
            doc = "List of stations to include"),
        pulse_integration_half_width = dict(default=2, output = False,
            doc = "Number of samples to the left and right of the pulse sample to integrate over for obtaining the Stokes parameters"),
        core=dict(default = [0., 0., 0.], output = False,
            doc = "Core position ``(x, y, z)`` in meters."),
        core_uncertainties=dict(default = [5., 5., 0.], output = False,
            doc = "Uncertainties on core position."),
        direction=dict(default=None, output = False,
            doc = "Direction ``(az, el)`` in LOFAR convention to use for polarization, i.e. cr_average_direction"),
        dbManager=dict(default = lambda self : crdb.CRDatabase("crdb", host="coma00.science.ru.nl", user="crdb", password="crdb", dbname="crdb"), output = False,
            doc = "Instance of database manager (i.e. :class:`crdatabase.CRDatabase.db`)"),
        db=dict(default = lambda self : self.dbManager.db, output = False,
            doc = "Instance of database (i.e. :class:`crdatabase.CRDatabase`)"),
        save_plots=dict(default = False, output = False,
            doc = "Store plots"),
        plot_prefix=dict(default = "", output = False,
            doc = "Prefix for plots"),
        plot_type=dict(default = "png", output = False,
            doc = "Plot type (e.g. png, jpeg, pdf)"),
        plotlist=dict(default = [], output = False,
            doc = "List of plots"),
        plot_title=dict(default=True, output = False,
            doc = "Plot title, turn off for publication ready figures"),
        mean_vxvxb=dict(default=None, output=True,
            doc = "Mean of the absolute value of the :math:`\\vec{v}\\times\\vec{v}\\times\\vec{B}` polarization component of the polarization vector"),
        mean_vxb=dict(default=None, output=True,
            doc = "Mean of the absolute value of the :math:`\\vec{v}\\times\\vec{B}` polarization component of the polarization vector"),
        std_vxvxb=dict(default=None, output=True,
            doc = "Standard deviation of the absolute value of the :math:`\\vec{v}\\times\\vec{v}\\times\\vec{B}` polarization component of the polarization vector"),
        std_vxb=dict(default=None, output=True,
            doc = "Standard deviation of the absolute value of the :math:`\\vec{v}\\times\\vec{B}` polarization component of the polarization vector"),
        stokes_I=dict(default=None, output=True,
            doc = "Stokes I for each antenna."),
        stokes_Q=dict(default=None, output=True,
            doc = "Stokes Q for each antenna."),
        stokes_U=dict(default=None, output=True,
            doc = "Stokes U for each antenna."),
        stokes_V=dict(default=None, output=True,
            doc = "Stokes V for each antenna."),
        stokes_I_uncertainty_MC=dict(default=None, output=True,
            doc = "Stokes I for each antenna."),
        stokes_Q_uncertainty_MC=dict(default=None, output=True,
            doc = "Stokes Q for each antenna."),
        stokes_U_uncertainty_MC=dict(default=None, output=True,
            doc = "Stokes U for each antenna."),
        stokes_V_uncertainty_MC=dict(default=None, output=True,
            doc = "Stokes V for each antenna."),
        stokes_I_uncertainty_analytic=dict(default=None, output=True,
            doc = "Stokes I for each antenna."),
        stokes_Q_uncertainty_analytic=dict(default=None, output=True,
            doc = "Stokes Q for each antenna."),
        stokes_U_uncertainty_analytic=dict(default=None, output=True,
            doc = "Stokes U for each antenna."),
        stokes_V_uncertainty_analytic=dict(default=None, output=True,
            doc = "Stokes V for each antenna."),
            
            
            
        polarization_angle=dict(default=None, output=True,
            doc = "Polarization angle :math:`\\psi` with the :math:`\\vec{v}\\times\\vec{B}` axis, positive towards :math:`\\vec{v}\\times\\vec{v}\\times\\vec{B}` in the shower plane, for each antenna"),
    )

    def run(self):
        """Run the task.
        """

        crp_average_direction = self.direction
        az = pytmf.deg2rad(90. - crp_average_direction[0]) # Definition????
        zen = pytmf.deg2rad(90. - crp_average_direction[1])

        inc=1.1837 # Earth magnetic field inclination at observer location
        B = np.array([0,np.cos(inc),-np.sin(inc)])
        v = np.array([-np.cos(az)*np.sin(zen),-np.sin(az)*np.sin(zen),-np.cos(zen)])
        vxB = np.array([v[1]*B[2]-v[2]*B[1],v[2]*B[0]-v[0]*B[2],v[0]*B[1]-v[1]*B[0]])
        vxB = vxB/np.linalg.norm(vxB)
        vxvxB = -np.array([v[1]*vxB[2]-v[2]*vxB[1],v[2]*vxB[0]-v[0]*vxB[2],v[0]*vxB[1]-v[1]*vxB[0]])

        talpha = np.arccos(np.inner(np.asarray(B), np.asarray(v)) / (np.linalg.norm(B) * np.linalg.norm(v)))

        all_station_antenna_positions = []
        all_station_pulse_peak_amplitude = []
        all_station_rms = []

        all_station_vxB_timeseries = []
        all_station_vxvxB_timeseries = []
        all_station_v_timeseries = []

        alpha = [] # Geomagnetic angle
        angle = []
        r = []
        R = []
        I = []
        Q = []
        U = []
        V = []
        sigma_I_MC = []
        sigma_Q_MC = []
        sigma_U_MC = []
        sigma_V_MC = []

        self.stokes_I_uncertainty_MC = []
        self.stokes_Q_uncertainty_MC = []
        self.stokes_U_uncertainty_MC = []
        self.stokes_V_uncertainty_MC = []

        sigma_I_analytic = []
        sigma_Q_analytic = []
        sigma_U_analytic = []
        sigma_V_analytic = []
        psi = []
        sigma_psi = []
        p = []
        x = []
        y = []
        na = []
        rp = []
        snr = []
        p_p = []
        p_n = []
        zen_angle = []

        n_included_stations = 0
        for station in self.event.stations:

            if not station.status == "GOOD":
                continue

            if station.flagged:
                print "Station", station.stationname, "flagged, skipping station"
                continue
            else:
                print "Polarization adding station:", station.stationname

            if not station.stationname in self.include_stations:
                print "excluding station", station.stationname
                continue

            # Get information from files and database
            datafile = "{0}/xyz_calibrated_pulse_block-{1}-{2}.npy".format(self.data_path, self.eventid, station.stationname)
            temp = np.load(datafile)
            nantennas = temp.shape[0] / 3

            pulse_direction = station['crp_pulse_direction']
            antenna_positions = np.asarray(station['local_antenna_positions'])

            # Check whether datafiles were split
            if antenna_positions.shape[0]/2 != nantennas:
                "Number of antennas from datafile did not agree with value in database, skipping station"
                continue
            else:
                n_included_stations += 1

            relpos = antenna_positions - self.core
            relpos = relpos.reshape((relpos.shape[0] / 2, 2, 3))[:, 0, :]

            rp.append(relpos.T)

            # Project antenna positions into the shower plane
            projected_antenna_positions = np.array([np.inner(vxB,relpos),np.inner(vxvxB,relpos),np.inner(v,relpos)]).T

            phi = np.arctan2(projected_antenna_positions[:, 1], projected_antenna_positions[:, 0])
            angle.append(phi)
            r.append(np.sqrt(projected_antenna_positions[:, 1]**2 + projected_antenna_positions[:, 0]**2))

            x.append(projected_antenna_positions[:, 0])
            y.append(projected_antenna_positions[:, 1])

            alpha.extend([talpha for i in range(nantennas)])
            zen_angle.extend([zen for i in range(nantennas)])

            # Project measured signal into the shower plane
            temp2 = temp.copy()
            blocksize = temp2.size / (nantennas * 3)
            temp2 = temp2.reshape(nantennas, 3, blocksize)
            vxb_timeseries_data = np.zeros(nantennas*3*blocksize).reshape(nantennas, 3, blocksize)

        # Still in loop for..station
            vxb_timeseries_data[:, 0, :] = vxB[0] * temp2[:, 0] + vxB[1] * temp2[:, 1] + vxB[2] * temp2[:, 2]
            vxb_timeseries_data[:, 1, :] = vxvxB[0] * temp2[:, 0] + vxvxB[1] * temp2[:, 1] + vxvxB[2] * temp2[:, 2]
            vxb_timeseries_data[:, 2, :] = v[0] * temp2[:, 0] + v[1] * temp2[:, 1] + v[2] * temp2[:, 2]
        
            print 'DEBUG: shape of vxb_timeseries_data[:, 0, :]'
            print ' '
            print vxb_timeseries_data[:, 0, :].shape
            print ' '
            print '------'
        
            vxb_timeseries_data = vxb_timeseries_data.reshape(nantennas * 3, blocksize)
            
            # Find location of the pulse in the block
            broad_search_window_width = 2**13
            narrow_search_window_width = 2**8
            pulse_search_window_start = (blocksize / 2) - (broad_search_window_width / 2)
            pulse_search_window_end = (blocksize / 2) + (broad_search_window_width / 2)
            bf_position = station['crp_bf_pulse_position']
            pulse_start = pulse_search_window_start + bf_position - narrow_search_window_width / 2
            pulse_end = pulse_search_window_start + bf_position + narrow_search_window_width / 2 # AC: why pulse_window_start here again?

            xyz_timeseries_data = cr.hArray(temp)
            pulse_envelope_xyz = cr.trun("PulseEnvelope", timeseries_data=cr.hArray(vxb_timeseries_data), pulse_start=pulse_start, pulse_end=pulse_end, resample_factor=16, npolarizations=3, save_plots=False, extra=True)

            snr.append(pulse_envelope_xyz.snr.toNumpy().reshape((nantennas, 3))[:,0])

            # Integrate pulse and noise power
            p_p.append(pulse_envelope_xyz.integrated_pulse_power.toNumpy().reshape((nantennas, 3)).sum(axis=1))
            p_n.append(pulse_envelope_xyz.integrated_noise_power.toNumpy().reshape((nantennas, 3)).sum(axis=1))

            maxpos = pulse_envelope_xyz.maxpos.toNumpy() / 16
            maxpos = maxpos.reshape((nantennas, 3))[:,0]
            
            print 'DEBUG: Maxpos'
            print maxpos
            print 'bf_position = %d' % int(bf_position)
            print 'DEBUG: Min and max maxpos'
            print 'Min = %d, max = %d' % (int(min(maxpos)), int(max(maxpos)) )
            print '----'
            #print "nantennas", nantennas, "maxpos", maxpos.shape[0]
           
            vxb_timeseries_data_to_cutout = vxb_timeseries_data.reshape(nantennas, 3, blocksize)
            all_station_vxB_timeseries.append(vxb_timeseries_data_to_cutout[:, 0, :][:, pulse_start:(pulse_start+1024)])
            all_station_vxvxB_timeseries.append(vxb_timeseries_data_to_cutout[:, 1, :][:, pulse_start:(pulse_start+1024)])
            all_station_v_timeseries.append(vxb_timeseries_data_to_cutout[:, 2, :][:, pulse_start:(pulse_start+1024)])
            # Cutting out a 1024 sample window around the pulse, for storing it later (below)

            ##### Calculate Stokes parameters on regular pulse
            newdata = vxb_timeseries_data.reshape((nantennas, 3, blocksize))
            hnewdata = hilbert(newdata).imag

            print 'DEBUG POLARIZATION TASK: newdata vs hnewdata'
            print np.sum(np.abs(newdata)**2)
            print np.sum(np.abs(hnewdata)**2)

            Rtmp = np.zeros(nantennas)

            plt.figure()
            for i in range(maxpos.shape[0]):
                pulse_vxb = newdata[i, 0, pulse_start+maxpos[i]-self.pulse_integration_half_width:pulse_start+maxpos[i]+self.pulse_integration_half_width+1]
                pulse_vxvxb = newdata[i, 1, pulse_start+maxpos[i]-self.pulse_integration_half_width:pulse_start+maxpos[i]+self.pulse_integration_half_width+1]
        #        pulse_v = newdata[i, 2, pulse_start+maxpos[i]-self.pulse_integration_half_width:pulse_start+maxpos[i]+self.pulse_integration_half_width]

                hpulse_vxb = hnewdata[i, 0, pulse_start+maxpos[i]-self.pulse_integration_half_width:pulse_start+maxpos[i]+self.pulse_integration_half_width+1]
                hpulse_vxvxb = hnewdata[i, 1, pulse_start+maxpos[i]-self.pulse_integration_half_width:pulse_start+maxpos[i]+self.pulse_integration_half_width+1]
        #        hpulse_v = hnewdata[i, 2, pulse_start+maxpos[i]-self.pulse_integration_half_width:pulse_start+maxpos[i]+self.pulse_integration_half_width]

                n = 2 * self.pulse_integration_half_width + 1

                S = stokes.stokes_parameters(pulse_vxb, pulse_vxvxb, hpulse_vxb, hpulse_vxvxb)
                
                if i == 10:
                    print pulse_vxb
                    print '----------'
                    print hpulse_vxb
                    print 'xxxxxxxxxx'
                    print pulse_vxvxb
                    print 'yyyyyyyyyy'
                    print hpulse_vxvxb
                    print 'zzzzzzzzzz'
                    
                I.append(S[0])
                Q.append(S[1])
                U.append(S[2])
                V.append(S[3])

                psi.append(stokes.polarization_angle(S))
                p.append(stokes.degree_of_polarization(S))

                Rtmp[i] = 2*np.sum(pulse_vxb * pulse_vxvxb) / np.sum(pulse_vxb*pulse_vxb + pulse_vxvxb*pulse_vxvxb)

                # Calculate uncertainties from noise window offset by...
                offset = 1000 # was 500.
                # Try a larger noise block, length 1000 instead of 5 or 11.
                noise_integration_half_width = 20
                
                npulse_vxb = newdata[i, 0, offset+pulse_start+maxpos[i]-noise_integration_half_width:offset+pulse_start+maxpos[i]+noise_integration_half_width]
                npulse_vxvxb = newdata[i, 1, offset+pulse_start+maxpos[i]-noise_integration_half_width:offset+pulse_start+maxpos[i]+noise_integration_half_width]

                nhpulse_vxb = hnewdata[i, 0, offset+pulse_start+maxpos[i]-noise_integration_half_width:offset+pulse_start+maxpos[i]+noise_integration_half_width]
                nhpulse_vxvxb = hnewdata[i, 1, offset+pulse_start+maxpos[i]-noise_integration_half_width:offset+pulse_start+maxpos[i]+noise_integration_half_width]
                
                print 'Shapes of noise block arrays: '
                print npulse_vxb.shape
                print npulse_vxvxb.shape
                print nhpulse_vxb.shape
                print nhpulse_vxvxb.shape
                print '-----'

                """                npulse_vxb = newdata[i, 0, offset+pulse_start+maxpos[i]-self.pulse_integration_half_width:offset+pulse_start+maxpos[i]+self.pulse_integration_half_width]
                                npulse_vxvxb = newdata[i, 1, offset+pulse_start+maxpos[i]-self.pulse_integration_half_width:offset+pulse_start+maxpos[i]+self.pulse_integration_half_width]

                                nhpulse_vxb = hnewdata[i, 0, offset+pulse_start+maxpos[i]-self.pulse_integration_half_width:offset+pulse_start+maxpos[i]+self.pulse_integration_half_width]
                                nhpulse_vxvxb = hnewdata[i, 1, offset+pulse_start+maxpos[i]-self.pulse_integration_half_width:offset+pulse_start+maxpos[i]+self.pulse_integration_half_width]
                """
                sigma_S = stokes.stokes_parameters_uncertainties(npulse_vxb, npulse_vxvxb, nhpulse_vxb, nhpulse_vxvxb)
                sigma_I_analytic.append(sigma_S[0])
                sigma_Q_analytic.append(sigma_S[1])
                sigma_U_analytic.append(sigma_S[2])
                sigma_V_analytic.append(sigma_S[3])
                                
                sigma_psi.append(stokes.polarization_angle_uncertainty(S, sigma_S))

            all_station_antenna_positions.append(antenna_positions)
            all_station_pulse_peak_amplitude.append(cr.hArray(pulse_envelope_xyz.peak_amplitude).toNumpy().reshape((nantennas, 3)))
            all_station_rms.append(cr.hArray(pulse_envelope_xyz.rms).toNumpy().reshape((nantennas, 3)))

            R.append(Rtmp)

            ##### Calculate Stokes parameters on regular pulse + noise for uncertainties
            nnoiseblocks = 200
            start = 100
            extra_spacing = 50

            noise_angle = np.zeros((maxpos.shape[0], nnoiseblocks + 3)) # store phi, alpha, r, psi_0, psi_1, ..., psi_n
            # phi = polarization angle, alpha = geomagn angle, r = distance to shower core, psi_i = i-th MC trial angle.
            sigma_I_MC = np.zeros((maxpos.shape[0], nnoiseblocks))
            sigma_Q_MC = np.zeros((maxpos.shape[0], nnoiseblocks))
            sigma_U_MC = np.zeros((maxpos.shape[0], nnoiseblocks))
            sigma_V_MC = np.zeros((maxpos.shape[0], nnoiseblocks))
            for j in range(nnoiseblocks):
                shift = start+(self.pulse_integration_half_width * 2 + 1 + extra_spacing) * (j+1)
                for i in range(maxpos.shape[0]):
                    rstart = pulse_start+maxpos[i]-self.pulse_integration_half_width
                    rend = pulse_start+maxpos[i]+self.pulse_integration_half_width

                    pulse_vxb = newdata[i, 0, rstart:rend] + newdata[i, 0, shift+rstart:shift+rend]

                    pulse_vxvxb = newdata[i, 1, rstart:rend] + newdata[i, 1, shift+rstart:shift+rend]

                    pulse_v = newdata[i, 2, rstart:rend] + newdata[i, 2, shift+rstart:shift+rend]

                    hpulse_vxb = hnewdata[i, 0, rstart:rend] + hnewdata[i, 0, shift+rstart:shift+rend]

                    hpulse_vxvxb = hnewdata[i, 1, rstart:rend] + hnewdata[i, 1, shift+rstart:shift+rend]

                    S = stokes.stokes_parameters(pulse_vxb, pulse_vxvxb, hpulse_vxb, hpulse_vxvxb)

                    sigma_I_MC[i][j] = S[0]
                    sigma_Q_MC[i][j] = S[1]
                    sigma_U_MC[i][j] = S[2]
                    sigma_V_MC[i][j] = S[3]
                    
                    noise_angle[i][j+3] = stokes.polarization_angle(S)

            noise_angle[:,0] = phi
            noise_angle[:,1] = talpha
            print noise_angle.shape

            na.append(noise_angle)
            # Zit nog in ee nfor lus over alle stations! Moet samenvoegen erbuiten...
            self.stokes_I_uncertainty_MC.extend(list(np.std(sigma_I_MC, axis=1)))
            self.stokes_Q_uncertainty_MC.extend(list(np.std(sigma_Q_MC, axis=1)))
#            print 'DEBUG: Stokes Q uncertainty MC.'
#            print self.stokes_Q_uncertainty_MC.shape
#            print ' '
#            print self.stokes_Q_uncertainty_MC
#            print '---'
            self.stokes_U_uncertainty_MC.extend(list(np.std(sigma_U_MC, axis=1)))
            self.stokes_V_uncertainty_MC.extend(list(np.std(sigma_V_MC, axis=1)))

        # end for .. station

        if n_included_stations == 0:
            print "aborting, no good core stations for polarization plot"
            return
        
        self.stokes_I_uncertainty_analytic = np.asarray(sigma_I_analytic)
        self.stokes_Q_uncertainty_analytic = np.asarray(sigma_Q_analytic)
        self.stokes_U_uncertainty_analytic = np.asarray(sigma_U_analytic)
        self.stokes_V_uncertainty_analytic = np.asarray(sigma_V_analytic)

        self.stokes_I_uncertainty_MC = np.array(self.stokes_I_uncertainty_MC)
        self.stokes_Q_uncertainty_MC = np.array(self.stokes_Q_uncertainty_MC)
        self.stokes_U_uncertainty_MC = np.array(self.stokes_U_uncertainty_MC)
        self.stokes_V_uncertainty_MC = np.array(self.stokes_V_uncertainty_MC)
        
        print '# DEBUG ### Stokes U uncertainty MC in polarization.py'
        print self.stokes_U_uncertainty_MC.shape
        print '   '
        print self.stokes_U_uncertainty_MC
        print '-------'
        
        # Combine arrays for each station into one
        all_station_vxB_timeseries = np.vstack(all_station_vxB_timeseries)
        all_station_vxvxB_timeseries = np.vstack(all_station_vxvxB_timeseries)
        all_station_v_timeseries = np.vstack(all_station_v_timeseries)
               
        print 'DEBUG: shape of all_station_vxB_timeseries: '
        print ' '
        print all_station_vxB_timeseries.shape
        print ' '
        print '-----'        
                        
        na = np.vstack(na)
        na[:,2] = np.hstack(r)

        np.savetxt(self.data_path+"/relpos-"+str(self.eventid)+".txt", np.hstack(rp))

        print "shape of noise_angle", na.shape
        np.savetxt(self.data_path+"/"+str(self.pulse_integration_half_width)+"-noise_angle-"+str(self.eventid)+".txt", na)

        all_station_antenna_positions = np.vstack(all_station_antenna_positions)
        all_station_pulse_peak_amplitude = np.vstack(all_station_pulse_peak_amplitude)
        all_station_rms = np.vstack(all_station_rms)

        # Convert to contiguous array of correct shape
        shape = all_station_antenna_positions.shape
        all_station_antenna_positions = all_station_antenna_positions.reshape((shape[0] / 2, 2, shape[1]))[:, 0]
        all_station_antenna_positions = all_station_antenna_positions.copy()

        core_uncertainties = self.core_uncertainties

        direction_uncertainties = [3., 3., 0]

        ldf = cr.trun("Shower", ldf_logplot=False, core_uncertainties=core_uncertainties, direction_uncertainties=direction_uncertainties, positions=all_station_antenna_positions, core=self.core, direction=crp_average_direction, signals=all_station_pulse_peak_amplitude, signals_uncertainties=all_station_rms, ldf_enable=True, footprint_enable=False, skyplot_of_directions_enable=False, vxb=True, ldf_scale=False, save_plots=self.save_plots, plot_prefix=self.plot_prefix, plot_type=self.plot_type, plotlist=self.plotlist, plot_title=self.plot_title)
        plt.clf()

        np.savetxt(self.data_path+"/angle-"+str(self.eventid)+".txt", np.vstack([np.hstack(angle), np.hstack(r), np.hstack(R), np.asarray(alpha), np.asarray(I), np.asarray(Q), np.asarray(U), np.asarray(V), np.asarray(psi), np.asarray(p), np.hstack(x), np.hstack(y), np.hstack(sigma_psi), np.hstack(snr), np.asarray(zen_angle), np.hstack(p_p), np.hstack(p_n)]).T)

        x = np.hstack(x)
        y = np.hstack(y)

        plt.figure()
        plt.quiver(x, y, p*np.cos(np.asarray(psi)), p*np.sin(np.asarray(psi)))
        plt.scatter(0, 0, marker='o', color='k')
        plt.xlabel(r'$\mathrm{Distance\, along}\, \mathbf{\hat{e}}_{\vec{v}\times\vec{B}}\, [\mathrm{m}]$')
        plt.ylabel(r'$\mathrm{Distance\, along}\, \mathbf{\hat{e}}_{\vec{v}\times\vec{v}\times\vec{B}}\, [\mathrm{m}]$')

        ax = plt.gca()
        ax.set_aspect('equal')
        plt.tight_layout()
        plotname = self.plot_prefix + "polarization-footprint.{0}".format(self.plot_type)
        plt.savefig(plotname)
        self.plotlist.append(plotname)

        # Save time series of vxB, vx(vxB) and v components
        # along with antenna positions. 
        
        outDict = dict(vxB_timeseries=all_station_vxB_timeseries, vxvxB_timeseries=all_station_vxvxB_timeseries, v_timeseries=all_station_v_timeseries, antennapositions=all_station_antenna_positions)
        outfile = "{0}/vxB_timeseries-{1}.pickle".format(self.data_path, self.eventid)

        pickle.dump(outDict, open(outfile, "wb"))

        # Store parameters
        self.mean_vxvxb = np.mean(np.abs(np.sin(np.asarray(psi))))
        self.mean_vxb = np.mean(np.abs(np.cos(np.asarray(psi))))

        self.std_vxvxb = np.std(np.abs(np.sin(np.asarray(psi))))
        self.std_vxb = np.std(np.abs(np.cos(np.asarray(psi))))

        self.stokes_I = np.asarray(I)
        self.stokes_Q = np.asarray(Q)
        self.stokes_U = np.asarray(U)
        self.stokes_V = np.asarray(V)

        self.polarization_angle = np.asarray(psi)

