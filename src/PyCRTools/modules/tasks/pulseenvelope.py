"""
Pulse envelope
==============

.. moduleauthor:: Pim Schellart <p.schellart@astro.ru.nl>

"""

from pycrtools.tasks import Task
import numpy as np
import pycrtools as cr
import matplotlib.pyplot as plt


class PulseEnvelope(Task):
    """Calculate pulse envelope using a Hilbert transform.

    Optionally the signal will be first (up/down)sampled using the *resample_factor* and the *delays* between the
    peak_amplitude will be computed with respect to a reference antenna that is either given or taken to be the one
    with the highest signal to noise.

    """

    parameters = dict(
        timeseries_data=dict(default=None, output=False,
            doc="Timeseries data for all antennas,  hArray of shape: [nAntennas,nSamples]"),
        nantennas=dict(default=lambda self: self.timeseries_data.shape()[0], output=False,
            doc="Number of antennas."),
        Efield_unit=dict(default=False, output=False, doc="Set to True when measuring pulses in timeseries with units of E-field (V/m); False (default) for voltage time series"),
        pulse_start=dict(default=0, output=False,
            doc="Start of pulse window (as sample number)."),
        pulse_end=dict(default=lambda self: self.timeseries_data.shape()[1], output=False,
            doc="End of pulse window (as sample number)."),
        pulse_width=dict(default=lambda self: self.pulse_end - self.pulse_start, output=False,
            doc="Width of pulse window (as sample number)."),
        window_start=dict(default=lambda self: max(self.pulse_start - self.pulse_width, 0), output=False,
            doc="Start of window (as sample number)."),
        window_end=dict(default=lambda self: min(self.pulse_end + self.pulse_width, self.timeseries_data.shape()[1]), output=False,
            doc="End of window (as sample number)."),
        window_width=dict(default=lambda self: self.window_end - self.window_start, output=False,
            doc="Width of full window (as sample number)."),
        resample_factor=dict(default=1, output=False,
            doc="Factor with which the timeseries will be resampled, needs to be integer > 0"),
        window_width_resampled=dict(default=lambda self: self.window_width * self.resample_factor, output=False,
            doc="Width of pulse window after resampling (as sample number)."),
        timeseries_data_resampled=dict(default=lambda self: cr.hArray(float, dimensions=(self.nantennas, self.window_width_resampled)), output=False,
            doc="Resampled timeseries data."),
        fft_data = dict(default=lambda self: cr.hArray(complex, dimensions=(self.nantennas, self.window_width_resampled / 2 + 1)), output=False,
            doc = "Fourier transform of timeseries_data_resampled."),
        sampling_frequency=dict(default = 200.e6, output=False,
            doc = "Sampling frequency of timeseries_data (in Hz)."),
        hilbertt=dict(default = lambda self: self.timeseries_data_resampled.new(), workarray = True, output=True,
            doc = "Hilbert transform of *fft_data*."),
        envelope=dict(default = lambda self: self.timeseries_data_resampled.new(), workarray = True, output=True,
            doc = "Envelope calculated using Hilbert transform."),
        snr=dict(default = lambda self: cr.hArray(float, self.nantennas), output=True,
            doc = "Signal to noise ratio of pulse maximum."),
        rms=dict(default = lambda self: cr.hArray(float, self.nantennas), output=True,
            doc = "RMS of noise."),
        mean=dict(default = lambda self: cr.hArray(float, self.nantennas), output=True,
            doc = "Mean of noise."),
        peak_amplitude=dict(default = lambda self: cr.hArray(float, self.nantennas), output=True,
            doc = "Pulse maximum."),
        integrated_pulse_power=dict(default = lambda self: cr.hArray(float, self.nantennas), output=True,
            doc = "Integrated pulse power."),
        integrated_noise_power=dict(default = lambda self: cr.hArray(float, self.nantennas), output=True,
            doc = "Integrated noise power per sample."),
        integrated_pulse_power_wide=dict(default = lambda self: cr.hArray(float, self.nantennas), output=True,
            doc = "Integrated pulse power (twice as wide window)."),
        integrated_noise_power_double_wide=dict(default = lambda self: cr.hArray(float, self.nantennas), output=True,
            doc = "Integrated noise power per sample (four times as wide window)."),
        integrated_pulse_power_double_wide=dict(default = lambda self: cr.hArray(float, self.nantennas), output=True,
            doc = "Integrated pulse power."),
        integrated_noise_power_wide=dict(default = lambda self: cr.hArray(float, self.nantennas), output=True,
            doc = "Integrated noise power per sample."),
        stokes=dict(default = lambda self: cr.hArray(float, dimensions=(self.nantennas, 4)), output=True,
            doc="Stokes parameters I, Q, U and V for each antenna."),
        polarization_angle=dict(default=lambda self: cr.hArray(float, self.nantennas), output=True,
            doc="Polarization angle ..math::`\phi=0.5 \atan(U/Q)` for each antenna."),
        maxpos=dict(default = lambda self: cr.hArray(int, self.nantennas), output=True,
            doc = "Position of pulse maximum relative to *pulse_start*."),
        maxpos_full=dict(default = lambda self: cr.hArray(int, self.nantennas), output=True,
            doc = "Position of pulse maximum relative to *pulse_start*."),
        best=dict(default = lambda self: cr.hMaxPos(self.snr), output=True,
            doc = "Index (into input array) of antenna with highest signal to noise."),
        bestpos=dict(default = lambda self: self.maxpos[self.best], output=True,
            doc = "Position of pulse maximum relative to *pulse_start* in antenna with highest signal to noise."),
        maxdiff=dict(default = lambda self: cr.hMaxDiff(cr.hArray([self.maxpos[i] for i in self.antennas_with_significant_pulses]))[0], output=True,
            doc = "Maximum spread in position (in samples)."),
        pulse_maximum_time=dict(default = lambda self: cr.hArray(float, self.nantennas), output=True,
            doc = "Position of pulse maximum relative to data start in seconds."),
        refant=dict(default = lambda self: self.snr.maxpos().val(), output=True,
            doc = "Reference antenna for delays, taken as antenna with highest signal to noise."),
        delays=dict(default = lambda self: cr.hArray(float, self.nantennas), output=True,
            doc = "Delays corresponding to the position of the maximum of the envelope relative to the first antenna."),
        nsigma=dict(default = 3, output=False,
            doc = "Number of standard deviations that pulse needs to be above noise level in order to be accepted as found."),
        antennas_with_significant_pulses=dict(default = lambda self: [i for i in range(self.nantennas) if self.snr[i] > self.nsigma], output=False,
            doc = "List of antennas (as indexes into the input array) with pulses more than nsigma above the noise level."),
        npolarizations=dict(default = 1, output=True,
            doc = "If this parameter > 1 then these are the number of antennas to be considered as one and pulse search is restricted to the one with the strongest SNR on average."),
        strongest_polarization=dict(default = None, output=True,
            doc = "Polarization that is strongest and is used to find pulse strength."),
        save_plots=dict(default = False, output=False,
            doc = "Store plots"),
        plot_prefix=dict(default = "", output=False,
            doc = "Prefix for plots"),
        plot_type=dict(default = "png", output=False,
            doc = "Plot type (e.g. png, jpeg, pdf)"),
        plotlist=dict(default = [], output=False,
            doc = "List of plots"),
        plot_antennas=dict(default = lambda self: range(self.nantennas), output=False,
            doc = "Antennas to create plots for."),
        plot_title=dict(default=True, output=False,
            doc = "Plot title, turn off for publication ready figures."),
        fftwplan=dict(default=lambda self: cr.FFTWPlanManyDftR2c(self.window_width_resampled, 1, 1, 1, 1, 1, cr.fftw_flags.ESTIMATE), output=False, doc="Plan for FFTW forward transform, automatically created if not given"),
        ifftwplan=dict(default=lambda self: cr.FFTWPlanManyDftC2r(self.window_width_resampled, 1, 1, 1, 1, 1, cr.fftw_flags.ESTIMATE), output=False, doc="Plan for FFTW inverse transform, automatically created if not given"),
        extra=dict(default = False, output=False,
            doc = "Calculate non essential extras (integrated pulse power & stokes parameters)."),
        estimate_timing_error=dict(default = False, output=False,
            doc = "Estimate timing error by adding estimate_timing_nsteps random noise blocks to the data and finding the pulse maximum in each case."),
        estimate_timing_nsteps=dict(default = 10, output=False,
            doc = "Number of steps for timing error estimation shifting."),
        estimate_timing_stepsize=dict(default = 1.e-7, output=False,
            doc = "Stepsize for timing error estimation shifting in seconds."),
        estimate_timing_maxpos=dict(default = lambda self: cr.hArray(int, dimensions=(self.estimate_timing_nsteps, self.nantennas)), output=True,
            doc = "Position of pulse maximum relative to *pulse_start* for each of the timing estimates (in samples)."),
        estimate_timing_timediff=dict(default = lambda self: cr.hArray(float, dimensions=(self.estimate_timing_nsteps, self.nantennas)), output=True,
            doc = "Position of pulse maximum relative to data start in seconds for each of the timing estimates."),
        estimate_timing_std=dict(default = None, output=False, doc="Estimated timing error as the standard deviation of estimate_timing_timediff per antenna."),
    )

    def run(self):
        """Run the task.
        """

        # Force recalculation of all task parameters
        self.update(True)

        # Resample singal
        cr.hFFTWResample(self.timeseries_data_resampled[...], self.timeseries_data[..., self.window_start:self.window_end])

        # Compute FFT
        cr.hFFTWExecutePlan(self.fft_data[...], self.timeseries_data_resampled[...], self.fftwplan)

        if self.estimate_timing_error:

            noise_block = cr.hArray(float, dimensions=self.timeseries_data_resampled.shape())
            width = self.window_end - self.window_start
            cr.hFFTWResample(noise_block[...], self.timeseries_data[..., self.window_end:self.window_end + width])

            fft_data_copy = self.fft_data.new()
            timeseries_data_resampled_copy = self.timeseries_data_resampled.new()

            fft_data_copy2 = self.fft_data.new()
            cr.hFFTWExecutePlan(fft_data_copy2[...], noise_block[...], self.fftwplan)

            frequencies = cr.hArray(float, dimensions=(self.fft_data.shape()[1]))
            cr.hFFTFrequencies(frequencies, 200.e6, 1)

            delays = cr.hArray(float, dimensions=(frequencies.shape()[0]))
            weights = cr.hArray(complex, dimensions=(frequencies.shape()[0]))

            for i in range(self.estimate_timing_nsteps):

                delays.fill(i * self.estimate_timing_stepsize * self.resample_factor)

                cr.hDelayToWeight(weights, frequencies, delays)

                fft_data_copy2.mul(weights)

                fft_data_copy.copy(self.fft_data)
                fft_data_copy.add(fft_data_copy2)

                timeseries_data_resampled_copy.fill(0.)
                cr.hFFTWExecutePlan(timeseries_data_resampled_copy[...], fft_data_copy[...], self.ifftwplan)

                timeseries_data_resampled_copy /= self.window_width_resampled

                # Apply Hilbert transform
                cr.hApplyHilbertTransform(fft_data_copy[...])

                # Get inverse FFT
                cr.hFFTWExecutePlan(self.hilbertt[...], fft_data_copy[...], self.ifftwplan)
                self.hilbertt /= self.window_width_resampled

                # Get envelope
                self.envelope.fill(0)
                cr.hSquareAdd(self.envelope, self.hilbertt)
                cr.hSquareAdd(self.envelope, timeseries_data_resampled_copy)
                cr.hSqrt(self.envelope)

                # Find signal to noise ratio, maximum, position of maximum and rms
                cr.hMaxSNR(self.snr[...], self.mean[...], self.rms[...], self.peak_amplitude[...], self.estimate_timing_maxpos[i,...], self.envelope[...], (self.pulse_start - self.window_start) * int(self.resample_factor), (self.pulse_end - self.window_start) * int(self.resample_factor))

            self.estimate_timing_timediff.copy(self.estimate_timing_maxpos)
            self.estimate_timing_timediff /= (self.sampling_frequency * self.resample_factor)

            self.estimate_timing_std = np.std(self.estimate_timing_timediff.toNumpy(), axis=0)

        # Apply Hilbert transform
        cr.hApplyHilbertTransform(self.fft_data[...])

        # Get inverse FFT
        cr.hFFTWExecutePlan(self.hilbertt[...], self.fft_data[...], self.ifftwplan)
        self.hilbertt /= self.window_width_resampled

        # Get envelope
        self.envelope.fill(0)
        cr.hSquareAdd(self.envelope, self.hilbertt)
        cr.hSquareAdd(self.envelope, self.timeseries_data_resampled)
        cr.hSqrt(self.envelope)

        # Find signal to noise ratio, maximum, position of maximum and rms
        cr.hMaxSNR(self.snr[...], self.mean[...], self.rms[...], self.peak_amplitude[...], self.maxpos[...], self.envelope[...], (self.pulse_start - self.window_start) * int(self.resample_factor), (self.pulse_end - self.window_start) * int(self.resample_factor))

        if self.npolarizations > 1:

            # Figure out which polarization has the strongest pulse signal on average
            self.strongest_polarization = int(np.argmax(self.snr.toNumpy().reshape((self.nantennas / self.npolarizations, self.npolarizations)).mean(axis=0)))

            # Update snr and peak amplitude using position of maximum found in strongest polarization
            start = (self.pulse_start - self.window_start) * int(self.resample_factor)
            for i in range(self.nantennas / self.npolarizations):
                for j in range(self.npolarizations):
                    k = i*self.npolarizations
                    n = k + j
                    self.maxpos[n] = self.maxpos[k+self.strongest_polarization]
                    self.peak_amplitude[n] = self.envelope[n, start + self.maxpos[k+self.strongest_polarization]]
                    if self.rms[n] == 0.:
                        self.snr[n] = 0.
                    else:
                        self.snr[n] = self.peak_amplitude[n] / self.rms[n]

        # Convert to delay
        self.delays[:] = self.maxpos[:]
        self.delays /= (self.sampling_frequency * self.resample_factor)

        # Calculate pulse maximum in seconds since start of datafile
        self.pulse_maximum_time[:] = self.window_start  # Number of samples before start of upsampled block
        self.pulse_maximum_time /= self.sampling_frequency  # Converted to seconds
        self.pulse_maximum_time += self.delays  # Add the number of seconds of the pulse in the upsampled block

        # Don't shift delays to be relative to reference antenna - get absolute time within given window
        #self.delays -= self.delays[self.refant]

        # Calculate integrated pulse power
        if self.extra:

            self.maxpos_full[...].copy(self.maxpos[...])
            self.maxpos_full /= self.resample_factor

            start = cr.hArray(int, self.maxpos_full.shape()[0])
            start.copy(self.maxpos_full)
            start += self.pulse_start - 5

            end = cr.hArray(int, self.maxpos_full.shape()[0])
            end.copy(self.maxpos_full)
            end += self.pulse_start + 5

            cr.hIntegratedPulsePower(self.integrated_pulse_power[...], self.integrated_noise_power[...], self.timeseries_data[...], start[...], end[...])

            start -= 5
            end += 5

            cr.hIntegratedPulsePower(self.integrated_pulse_power_wide[...], self.integrated_noise_power_wide[...], self.timeseries_data[...], start[...], end[...])

            start -= 10
            end += 10

            cr.hIntegratedPulsePower(self.integrated_pulse_power_double_wide[...], self.integrated_noise_power_double_wide[...], self.timeseries_data[...], start[...], end[...])

            self.integrated_pulse_power /= self.sampling_frequency
            self.integrated_noise_power /= self.sampling_frequency
            self.integrated_pulse_power_wide /= self.sampling_frequency
            self.integrated_noise_power_wide /= self.sampling_frequency
            self.integrated_pulse_power_double_wide /= self.sampling_frequency
            self.integrated_noise_power_double_wide /= self.sampling_frequency

            if self.Efield_unit == True:
                Z0 = 376.730 # Ohm, vacuum impedance
                self.integrated_pulse_power /= Z0
                self.integrated_noise_power /= Z0
                self.integrated_pulse_power_wide /= Z0
                self.integrated_noise_power_wide /= Z0
                self.integrated_pulse_power_double_wide /= Z0
                self.integrated_noise_power_double_wide /= Z0

#            # Calculate Stokes parameters
#            start.copy(self.maxpos)
#            start += (self.pulse_start - self.window_start) * int(self.resample_factor)
#            start -= 5 * self.resample_factor
#            end.copy(self.maxpos)
#            end += (self.pulse_start - self.window_start) * int(self.resample_factor)
#            end += 5 * self.resample_factor
#
#            cr.hStokesParameters(self.stokes[...], self.timeseries_data_resampled[0:3 * self.nantennas:3, ...], self.timeseries_data_resampled[1:3 * self.nantennas:3, ...], self.hilbertt[0:3 * self.nantennas:3, ...], self.hilbertt[1:3 * self.nantennas:3, ...], start[...], end[...])
#
#            # Calculate polarization angle
#            cr.hAtan2(self.polarization_angle[...], self.stokes[..., 2], self.stokes[..., 1])
#            self.polarization_angle /= 2

        s = self.timeseries_data_resampled.toNumpy()
        self.sign = np.zeros(s.shape[0])
        for i in range(s.shape[0]):
            self.sign[i] = s[i, self.maxpos[i] + (self.pulse_start - self.window_start) * int(self.resample_factor)]
        self.sign /= np.abs(self.sign)

        if self.save_plots:

            y = self.envelope.toNumpy()
            x = 1.e6 * np.arange(y.shape[1]) / (self.sampling_frequency * self.resample_factor)

            # Single pulse envelope of first antenna
            for i in self.plot_antennas:
                plt.clf()

                plt.plot(x, s[i], linestyle='-', color='#68C8F7', label="Signal")
                plt.plot(x, y[i], linestyle='-', color='#B30424', label="Envelope")
                plt.plot(x, np.zeros(y.shape[1]) + self.mean[i] + self.rms[i], 'k--', label="RMS")
                plt.annotate("pulse maximum", xy=(x[self.maxpos[i] + (self.pulse_start - self.window_start) * int(self.resample_factor)], self.peak_amplitude[i]), xytext = (0.13, 0.865), textcoords="figure fraction", arrowprops=dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=90,rad=10"))

                p = self.plot_prefix + "pulse_envelope_envelope-{0:d}.{1}".format(i, self.plot_type)

                plt.xlabel(r"Time ($\mu s$)")
                plt.ylabel("Amplitude (ADU)")
                plt.legend()
                if self.plot_title:
                    plt.title("Pulse envelope for antenna {0:d} {1:f} {2:f}".format(i, self.mean[i], self.rms[i]))
                plt.savefig(p)

                self.plotlist.append(p)
        
                # (AC) same plot, but with small scale, ~ +/- 100 samples from envelope maximum
                # better to make function to avoid duplicate code...
                plt.clf()
                
                zoom_start = np.argmax(y[i]) - 100*self.resample_factor # envelope maximum
                zoom_end = zoom_start + 200*self.resample_factor
                
                plt.plot(x[zoom_start:zoom_end], s[i][zoom_start:zoom_end], linestyle='-', color='#68C8F7', label="Signal")
                plt.plot(x[zoom_start:zoom_end], y[i][zoom_start:zoom_end], linestyle='-', color='#B30424', label="Envelope")
                plt.plot(x[zoom_start:zoom_end], np.zeros(len(y[i][zoom_start:zoom_end])) + self.mean[i] + self.rms[i], 'k--', label="RMS")
                plt.annotate("pulse maximum", xy=(x[self.maxpos[i] + (self.pulse_start - self.window_start) * int(self.resample_factor)], self.peak_amplitude[i]), xytext = (0.13, 0.865), textcoords="figure fraction", arrowprops=dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=90,rad=10"))
                
                p = self.plot_prefix + "pulse_envelope_envelope_zoomed-{0:d}.{1}".format(i, self.plot_type)
                
                plt.xlabel(r"Time ($\mu s$)")
                plt.ylabel("Amplitude (ADU)")
                plt.legend()
                if self.plot_title:
                    plt.title("Close-up: pulse envelope for antenna {0:d} {1:f} {2:f}".format(i, self.mean[i], self.rms[i]))
                plt.savefig(p)
                
                self.plotlist.append(p)


            # All pulse envelopes as stacked plot
            plt.clf()

            offset = 0

            color = ['#B30424', '#68C8F7', '#FF8C19']
            for i in range(y.shape[0]):

                plt.plot(x, y[i] + offset, linestyle='-', color=color[i % self.npolarizations])

                offset += self.peak_amplitude[i]

            p = self.plot_prefix + "pulse_envelope_envelope.{0}".format(self.plot_type)

            plt.gca().autoscale_view(tight=True)
            plt.xlabel(r"Time ($\mu s$)")
            plt.ylabel("Amplitude with offset (ADU)")
            if self.plot_title:
                plt.title("All pulse envelopes")
            plt.savefig(p)

            self.plotlist.append(p)
