"""
Xmax method
===========

.. moduleauthor:: Stijn Buitink <s.buitink@astro.ru.nl>

"""

from pycrtools.tasks import Task
import numpy as np
import pycrtools as cr
import matplotlib.pyplot as plt


class XmaxMethod(Task):
    """Obtain Xmax from a series of simulations.

    .. seealso:: Buitink et al. Phys. Rev. D 90, 082003 - Published 17 October 2014

    """

    parameters = dict(
        eventid=dict(default = None,
            doc = "Event ID"),
        event=dict(default = lambda self : crdb.Event(db=self.db, id=self.eventid),
            doc = "Event object"),
        simulation_directory=dict(default=None,
            doc="Directory containing simulations"),
        save_plots=dict(default = False,
            doc = "Store plots"),
        plot_prefix=dict(default = "",
            doc = "Prefix for plots"),
        plot_type=dict(default = "png",
            doc = "Plot type (e.g. png, jpeg, pdf)"),
        plotlist=dict(default = [],
            doc = "List of plots"),
        plot_title=dict(default=True,
            doc = "Plot title, turn off for publication ready figures."),
        fit_energy=dict(defaults = None, output = True,
            doc = "Energy (in GeV) determined by fit of models to data."),
        fit_xmax=dict(defaults = None, output = True,
            doc = "Xmax (in g / cm^2) determined by fit of models to data."),
        fit_xmax_uncertainty=dict(defaults = None, output = True,
            doc = "Uncertainty on Xmax (in g / cm^2) determined by fit of models to data."),
        fit_output=dict(defaults = {}, output = True,
            doc = "Fit parameter dictionary.")
    )

    def run(self):
        """Run the task.
        """

        print self.simulation_directory
        print self.plot_prefix

        # Stijn, add your stuff here
#        # Example for adding plot...
#        p = self.plot_prefix + "xmax_method-{0:d}.{1}".format(i, self.plot_type)
#
#        # Make your plot
#        # plt.plot()
#
#        if self.plot_title:
#            plt.title("Insert title")
#
#        if self.save_plots:
#            plt.savefig(p)
#
#            self.plotlist.append(p)

