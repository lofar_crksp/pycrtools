"""Run LORA simulation on events processed with cr_simulation.py (i.e. in state COREAS_DONE)
    """

# ./gdastool --observatory=lofar --utctimestamp=1343951431 --gdaspath=/vol/astro7/lofar/gdas --output=ATMOSPHERE_TESTGDAS.DAT


import logging

# Log everything, and send it to stderr.
logging.basicConfig(level=logging.DEBUG)

import os
import sys
import time
import datetime
import pytmf
import subprocess
import pycrtools as cr
import glob

from pycrtools import crdatabase as crdb
from pycrtools import simhelp

from pycrtools.tasks import xmaxmethod

from optparse import OptionParser
from contextlib import contextmanager

class PipelineError(Exception):
    """Base class for pipeline exceptions."""
    
    def __init__(self, message, category="OTHER"):
        self.message = message
        self.category = category

class EventError(PipelineError):
    """Raised when an unhandlable error occurs at event level."""
    pass

class Skipped(PipelineError):
    """Base class for everything that needs to lead to a skipped state."""
    pass

class EventSkipped(Skipped):
    """Raised when event is skipped."""
    pass

@contextmanager
def process_event(event):
    start = time.clock()
    
    print "-- event {0}".format(event._id)
    
    try:
        yield event
    except EventSkipped as e:
        logging.info("event skipped because: {0}".format(e.message))
        
        #event.simulation_status = "SKIPPED"
        #event.simulation_statusmessage = e.message
    except EventError as e:
        logging.exception(e.message)
        #event.simulation_status = "ERROR"
        #event.simulation_statusmessage = e.message
    except Exception as e:
        logging.exception(e.message)
        #event.simulation_status = "ERROR"
        #event.simulation_statusmessage = e.message
        raise
    except BaseException as e:
        logging.exception(e.message)
        #event.simulation_status = "ERROR"
        #event.simulation_statusmessage = "sigterm recieved"
        raise
    finally:
        #event["simulation_last_processed"] = datetime.datetime.utcnow().strftime("%Y-%m-%d")
        #event.write()
        print "-- event {0} completed in {1:.3f} s".format(event._id, time.clock() - start)

# Parse commandline options
parser = OptionParser()
parser.add_option("-i", "--id", type="int", help="event ID", default=1)
parser.add_option("-d", "--database", default="crdb.sqlite", help="filename of database")
parser.add_option("--host", default=None, help="PostgreSQL host.")
parser.add_option("--user", default=None, help="PostgreSQL user.")
parser.add_option("--password", default=None, help="PostgreSQL password.")
parser.add_option("--dbname", default=None, help="PostgreSQL dbname.")
parser.add_option("--simdir", default="/vol/astro7/lofar/sim/pipeline", help="Simulation output directory")

(options, args) = parser.parse_args()

db_filename = options.database
dbManager = crdb.CRDatabase(db_filename, host=options.host, user=options.user, password=options.password, dbname=options.dbname)
db = dbManager.db

valid_status = ["COREAS_DONE"]

def getLatestIteration(datadir, eventid):
    def RepresentsInt(s): # check if string is convertible to 'int'...
        try:
            int(s)
            return True
        except ValueError:
            return False

    eventdir = os.path.join(datadir, "{0}".format(eventid))
    # Get all available iterations
    all_iterations = glob.glob(os.path.join(eventdir, '*'))
    all_iterations = [int(os.path.split(x)[1]) for x in all_iterations if RepresentsInt(os.path.split(x)[1])] # make integers
    iteration_to_process = max(all_iterations)
    
    return iteration_to_process


# Ignore if simulations are already scheduled
#if options.id in simhelp.running_jobs():
#    print "Event {0} already scheduled, skipping...".format(options.id)
#    sys.exit(0)

# Get event from database and run pipeline on it
with process_event(crdb.Event(db=db, id=options.id)) as event:
    
    if event.simulation_status != "COREAS_DONE":
        raise ValueError("Event does not have state COREAS_DONE")
    
    eventid = int(options.id)
    # Get latest iteration
    iteration = getLatestIteration(options.simdir+'/events', eventid)

    event_output_directory = os.path.join(options.simdir, "events/{0}/{1}/coreas".format(eventid, iteration))
    # read in the list of simulated showers to be processed with LORA_simulation
    particles = ['proton', 'iron']
    nofShowersProcessed = 0

    for particle in particles:
        showers_path = os.path.join(event_output_directory, particle)
        showers = subprocess.check_output('ls -1 ' + showers_path + '/DAT*', shell=True, executable='/bin/bash') # DAT files without extension
        # get only showers without extension
        showers = showers.split('\n')
        shower_list = []
        for name in showers:
            filename, file_extension = os.path.splitext(name)
            if len(file_extension) == 0 and len(name) > 0:
                shower_list.append(os.path.basename(name)) # name without dir
                
        showers = shower_list
                                           
                                           
        scratch_path = '/scratch/cr_lorasim/{0}/{1}/{2}'.format(eventid, iteration, particle)
        print 'Make directory on /scratch'
        returncode = subprocess.call(['mkdir -p ' + scratch_path], shell=True, executable='/bin/bash')
        if returncode != 0:
            raise
        # empty directory if needed
        print 'Empty directory if it already existed'
        returncode = subprocess.call(['rm ' + os.path.join(scratch_path, '*')], shell=True, executable='/bin/bash')
        
        #import pdb; pdb.set_trace()

        # hack for testing
        #showers = [showers[0]]

        for shower in showers:
            # copy shower file to scratch
            print 'Doing shower: %s' % shower
            shower_file = os.path.join(showers_path, shower)
            copy_string = 'cp ' + shower_file + ' ' + scratch_path + '/'
            print 'Copying: %s' % copy_string
            returncode = subprocess.call([copy_string], shell=True, executable='/bin/bash')
            if returncode != 0:
                raise


        for shower in showers:
            # run DAT2TXT to convert to text files for input
            print 'Run DAT2TXT on shower: %s' % shower
            shower_file = os.path.join(scratch_path, shower)

            run_string = '/vol/optcoma/cr-simulations/LORAtools_GeVfix/DAT2txt ' + shower_file + ' ' + shower_file+'.tmp'
            print 'Run DAT2TXT: %s' % run_string
            returncode = subprocess.call([run_string], shell=True, executable='/bin/bash')
            if returncode != 0:
                raise

        processes = []
        for shower in showers:
            # run LORA_simulation on files
            print 'Run LORA_simulation on shower: %s' % shower
            shower_file = os.path.join(scratch_path, shower)
            print 'Shower file = %s' % shower_file
            
            run_string = '/vol/optcoma/cr-simulations/LORAtools_GeVfix/LORA_simulation ' + shower_file + '.tmp ' + shower_file+'_GeVfix.lora'
            
            print 'Run LORA_simulation: %s' % run_string
            process = subprocess.Popen([run_string], shell=True)
            processes.append(process)
            #            returncode = subprocess.call([run_string], shell=True, executable='/bin/bash')
            #if returncode != 0:
            #    print "Returncode was not 0: %d" % returncode
            #    raise
            #nofShowersProcessed += 1
        for i, process in enumerate(processes):
            print 'Waiting for process %d' % i
            process.wait()
            if process.returncode != 0:
                print 'Return code was not 0 !!'
                raise
    
        for shower in showers:
            # run LORA_simulation on files
            print 'Copy file back, shower: %s' % shower
            shower_file = os.path.join(scratch_path, shower) + '_GeVfix.lora'
            data_path = os.path.join(event_output_directory, particle)
            print 'Shower file = %s' % shower_file
            
            copy_string = 'cp '+shower_file + ' ' + data_path + '/'
            
            print 'Copy result file back: %s' % copy_string
            returncode = subprocess.call([copy_string], shell=True, executable='/bin/bash')
            if returncode != 0:
                raise

    event.simulation_status = "LORASIM_DONE"
    event.write()

rmstring = 'rm -rf /scratch/cr_lorasim/{0}'.format(eventid)
print 'Removing scratch path... %s' % rmstring
# NB. Assuming that processing happens on a per-event basis. Change this if multiple iterations and/or proton/iron may happen in separate threads!
returncode = subprocess.call([rmstring], shell=True, executable='/bin/bash')
if returncode != 0:
    raise

print 'cr_lorasimulation finished: processed %d showers' % nofShowersProcessed

