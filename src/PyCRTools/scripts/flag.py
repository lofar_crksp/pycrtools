import psycopg2
import optparse
from pycrtools import crdatabase as crdb

"""
This script can be used to easily flag events in the database. This is needed when automatic flagging does not work and one does not want to look up the SQL syntax every time. 
"""

# Parse commandline options
parser = optparse.OptionParser()
parser.add_option("--host", default='coma00.science.ru.nl', help="PostgreSQL host.")
parser.add_option("--user", default='crdb', help="PostgreSQL user.")
parser.add_option("--password", default='crdb', help="PostgreSQL password.")
parser.add_option("--dbname", default='crdb', help="PostgreSQL dbname.")

parser.add_option("-s", "--station", default=None, help="Station")
parser.add_option("-p", "--polarization", default=None, help="Polarization")
parser.add_option("-r", "--reason", default=None, help="Reason")
parser.add_option("-c", "--clear", action='store_true', help="Clear flag")

(options, args) = parser.parse_args()

conn = psycopg2.connect(host=options.host, user=options.user, password=options.password, dbname=options.dbname)

c = conn.cursor()

if options.clear:
    condition = "FALSE"
    reason = ""
else:
    condition = "TRUE"
    reason = options.reason

for eventID in args:

    if options.station and options.polarization:
        c.execute("UPDATE polarizations SET flagged={condition} FROM events, event_datafile, datafile_station, stations, station_polarization WHERE (events.eventID='{0}') AND events.eventID=event_datafile.eventID AND event_datafile.datafileID=datafile_station.datafileID AND datafile_station.stationID=stations.stationID AND stations.stationname='{1}' AND stations.stationID=station_polarization.stationID AND polarizations.direction='{2}'".format(eventID, options.station, options.polarization, condition=condition))
        c.execute("UPDATE polarizations SET flagged_reason='{3}' FROM events, event_datafile, datafile_station, stations, station_polarization WHERE (events.eventID='{0}') AND events.eventID=event_datafile.eventID AND event_datafile.datafileID=datafile_station.datafileID AND datafile_station.stationID=stations.stationID AND stations.stationname='{1}' AND stations.stationID=station_polarization.stationID AND polarizations.direction='{2}'".format(eventID, options.station, options.polarization, crdb.pickle_parameter(reason)))

    elif options.station:
        c.execute("UPDATE stations SET flagged={condition} FROM events, event_datafile, datafile_station WHERE (events.eventID='{0}') AND events.eventID=event_datafile.eventID AND event_datafile.datafileID=datafile_station.datafileID AND datafile_station.stationID=stations.stationID AND stations.stationname='{1}'".format(eventID, options.station, condition=condition))
        c.execute("UPDATE stations SET flagged_reason='{2}' FROM events, event_datafile, datafile_station WHERE (events.eventID='{0}') AND events.eventID=event_datafile.eventID AND event_datafile.datafileID=datafile_station.datafileID AND datafile_station.stationID=stations.stationID AND stations.stationname='{1}'".format(eventID, options.station, crdb.pickle_parameter(reason)))

    elif not options.station and not options.polarization:
        c.execute("UPDATE events SET flagged={condition} WHERE (events.eventID='{0}')".format(eventID, condition=condition))
        c.execute("UPDATE events SET flagged_reason='{1}' WHERE (events.eventID='{0}')".format(eventID, crdb.pickle_parameter(reason)))

    else:
        print options.help()

conn.commit()

conn.close()

