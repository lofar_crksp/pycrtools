"""
    Set the simulation status in the database of simulated events, e.g. when re-populating a database from scratch. Manual procedure.
    """

#import pycrtools as cr
import numpy as np
import re
#import time
from datetime import datetime
import psycopg2
import sys
import os
import glob
from pycrtools.crdatabase import pickle_parameter
from pycrtools import crdatabase as crdb

#from pycrtools import crdatabase as crdb
#from pycrtools.crdatabase import pickle_parameter, unpickle_parameter

def getLatestIteration(eventid, simulation_dir):
    def RepresentsInt(s): # check if string is convertible to 'int'...
        try:
            int(s)
            return True
        except ValueError:
            return False

    eventdir = os.path.join(simulation_dir, "{0}".format(eventid))
    # Get all available iterations
    all_iterations = glob.glob(os.path.join(eventdir, '*'))
    all_iterations = [int(os.path.split(x)[1]) for x in all_iterations if RepresentsInt(os.path.split(x)[1])] # make integers
    iteration_to_process = max(all_iterations)
    
    return iteration_to_process


def update_event_SQL(eventid, latest_iteration, energy, az, el, date_last_processed):
    # Produce SQL commands to update the given event, and do update
    sql_list = []

    sql = "UPDATE eventparameters SET simulation_current_iteration={0} WHERE eventid='{1}';".format(pickle_parameter(latest_iteration), eventid)
    sql_list.append(sql)
    sql = "UPDATE eventparameters SET simulation_energy={0} WHERE eventid='{1}';".format(pickle_parameter(energy), eventid)
    sql_list.append(sql)
    # etc... Full update is easier after all.

def update_event_full(eventid, latest_iteration, energy, az, el, date_last_processed):
    dbManager = crdb.CRDatabase("crdb", host="coma00.science.ru.nl", user="crdb", password="crdb", dbname="crdb")
    db = dbManager.db
    print 'Reading event data for eventid %d ...' % eventid
    event = crdb.Event(db=db, id=eventid)
    print 'done'

    # Set parameters
    event.simulation_status = "COREAS_DONE"

    event['simulation_current_iteration'] = latest_iteration
    event['simulation_energy'] = energy
    event['simulation_energy_reason'] = "Reconstructed from CoREAS steering file"
    event['simulation_xmax'] = -1
    event['simulation_xmax_reason'] = "Could not reconstruct (empty database)"
    event['simulation_direction'] = (az, el)
    event['simulation_direction_reason'] = "Reconstructed from CoREAS steering file"
    event['simulation_last_processed'] = date_last_processed
    event['simulation_nof_scheduled'] = 0

    print 'Writing new data for event...'
    event.write()
    print 'Done.'

# Options
#try:
#    eventlist = sys.argv[1]
#except:
#    "Give eventlist file, python set_eventlist_as_new.py <eventlist.txt>"

# List contents of CR simulation directory, read out simulated event ids
# From list of event ids, check further if both Conex and Coreas are there
# and if a minimum # of simulations are there

minimum_nofshowers = 10
simulation_dir = '/vol/astro7/lofar/sim/pipeline/events'
events = glob.glob(simulation_dir+"/*")
eventids = [int(eventdir.split('/')[-1]) for eventdir in events]
lorafile_suffix = '_GeVfix'
eventids_failed = []

for eventid in eventids:
    latest_iteration = getLatestIteration(eventid, simulation_dir)
   
    # in principle, could set simulation_current_iteration = latest_iteration
    # but want to do a few checks, & read in simulation energy and direction etc.
    
    dirs=glob.glob(simulation_dir + "/{0}/{1}/coreas/*".format(eventid, latest_iteration))
    showercount = 0
    if len(dirs) == 0:
        raise ValueError("No directories with simulations found for event %d" % eventid)
    for d in dirs:
        showerfiles=glob.glob(d+"/DAT??????")
        for showerfile in showerfiles:
            showerno=int(showerfile[-6:])
            #check if all files are ready
            # Note: including .lora files, processed by LORA_simulation. May want to disable if CoREAS simulated but LORA_simulation not done.
            if (os.path.isdir(d+"/SIM{0}_coreas".format(str(showerno).zfill(6))) and
                os.path.isfile(d+"/DAT{0}{1}.lora".format(str(showerno).zfill(6), lorafile_suffix)) and
                (os.stat(d+"/DAT{0}.long".format(str(showerno).zfill(6))).st_size>0) and
                os.path.isfile(d+"/DAT{0}.long".format(str(showerno).zfill(6))) and
                os.path.isfile(d+"/steering/RUN{0}.inp".format(str(showerno).zfill(6))) and
                os.path.isfile(d+"/steering/SIM{0}.list".format(str(showerno).zfill(6))) ):

                showercount += 1
            else:
                print "!!! Incomplete shower found in event %d: %s" % (eventid, showerfile)

    print '--- Event %d has %d completely simulated showers' % (eventid, showercount)
    if showercount >= minimum_nofshowers:
        # read simulation energy and direction from one CoREAS steering file
        infile = open(d+"/steering/RUN{0}.inp".format(str(showerno).zfill(6)), "r")
        lines = infile.readlines()
        infile.close()
        steering_string = '-'.join(lines) # make it one big string
        energy = steering_string.split('ERANGE')[1].split(' ')[1]
        energy = 1.0e9 * float(energy)
        print 'Iteration number is %d' % latest_iteration
        print 'Energy for event %d is %1.3e' % (eventid, energy)
        
        zenith_angle = float(steering_string.split('THETAP')[1].split(' ')[1])
        azimuth_angle = float(steering_string.split('PHIP')[1].split(' ')[1])
        # convert to LOFAR convention
        elev = 90.0 - zenith_angle
        lofar_az = -180.0 - azimuth_angle # conversion from Auger to LOFAR convention, degrees
        print 'Az, el = %3.2f, %3.2f' % (lofar_az, elev)
        
        # Get modification date for this file, and use this as 'last processed' time
        timestamp = os.path.getmtime(d+"/steering/RUN{0}.inp".format(str(showerno).zfill(6)))
        timestring = datetime.utcfromtimestamp(timestamp).strftime("%Y-%m-%d")
        print 'Date last processed = %s' % timestring
    
        # Update event parameters in database
        #update_event_full(eventid, latest_iteration, energy, lofar_az, elev, date_last_processed)
        # Update done

    else:
        eventids_failed.append(eventid)
        print '!!! Ignoring event %d, too few showers' % eventid
        continue

print ' '
print 'Failed events:'
print eventids_failed

"""







# Create cursor
conn = psycopg2.connect(host='coma00.science.ru.nl', user='crdb', password='crdb', dbname='crdb')

c = conn.cursor()

for line in open(eventlist, 'r'):
    thisEventID = int(line)
    sql = "UPDATE events SET status='NEW' WHERE eventid='%d'" % thisEventID
    print sql
    c.execute(sql)

conn.commit()
c.close()
conn.close()
"""