"""
Set a list of event entries to status NEW. Manual procedure.
"""

#import pycrtools as cr
import numpy as np
import re
import time
import psycopg2
import sys
#from pycrtools import crdatabase as crdb
#from pycrtools.crdatabase import pickle_parameter, unpickle_parameter

# Options
try:
    eventlist = sys.argv[1]
except:
    "Give eventlist file, python set_eventlist_as_new.py <eventlist.txt>"
    
# Create cursor
conn = psycopg2.connect(host='coma00.science.ru.nl', user='crdb', password='crdb', dbname='crdb')

c = conn.cursor()

for line in open(eventlist, 'r'):
    thisEventID = int(line)
    sql = "UPDATE events SET simulation_status='GDAS_BUG' WHERE eventid='%d'" % thisEventID
    print sql
    c.execute(sql)

conn.commit()
c.close()
conn.close()

