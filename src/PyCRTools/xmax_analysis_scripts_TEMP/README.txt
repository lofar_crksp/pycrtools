This directory contains the scripts in the version used to produce 
Chapter 5 in A. Corstanje's thesis (March 2019).

It is temporary i.e. the files are to be re-distributed over the 
pipelines directory and some additional (sub)directories so the scripts 
will be runnable from there instead of from /vol/astro3/lofar/sim/pipeline 
as they are now.


