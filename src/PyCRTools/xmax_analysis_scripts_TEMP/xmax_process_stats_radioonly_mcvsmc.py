import cPickle
import os
import numpy as np

import matplotlib
#matplotlib.use('Agg')

from matplotlib import rcParams
rcParams['mathtext.default'] = 'regular'
rcParams.update({'font.size': 16})
rcParams.update({'figure.autolayout': True})
import matplotlib.pyplot as plt
plt.rc('legend',**{'fontsize':14})

import ldf_anna_vs_reco
import conex_xmax_selection_check

plot_publishable = True
save_plot_dir = '/vol/astro2/users/acorstanje/composition_paper_plots'
np.random.seed(2018) # Fix random seed for bootstrapping

plt.ion()

def getDataArrays(event_array):

    eventids = event_array[:, 0]
    zeniths = event_array[:, 1]
    azimuths = event_array[:, 2]
    d_ratios = event_array[:, 3]
    p_ratios = event_array[:, 4]
    energies = event_array[:, 5]
    xrecos   = event_array[:, 6]
    xrecos_radio_only = event_array[:, 7]
    sigma_x  = event_array[:, 8]
    sigma_e  = event_array[:, 9]
    sigma_logE_radio = event_array[:, 10]
    std_core = event_array[:, 11]
    sigma_x_combined = event_array[:, 12]
    sigma_e_combined = event_array[:, 13]
    std_core_combined = event_array[:, 14]

    combchi2 = event_array[:, 21]
    radiochi2 = event_array[:, 22]
    
    p_passed = event_array[:, 18]
    r_passed = event_array[:, 19]
    q_passed = event_array[:, 20]

    return (eventids, zeniths, azimuths, d_ratios, p_ratios, energies, xrecos, xrecos_radio_only, sigma_x, sigma_e, sigma_logE_radio, std_core, sigma_x_combined, sigma_e_combined, std_core_combined, combchi2, radiochi2, p_passed, r_passed, q_passed)

infile = open("/home/acorstanje/xmax_statistics_final_99_0pct_5nov2018_RMS_sigmaX_USED_IN_THESIS.dat", "r")

#infile = open("/home/acorstanje/xmax_statistics_final_99_0pct_5nov2018_RMS_sigmaX.dat", "r") #_reiterate_xmax_98_5pct.dat", "r")

#infile = open("/home/acorstanje/xmax_statistics_final_99_0pct_25nov2018_newpbiastest.dat", "r") #_reiterate_xmax_98_5pct.dat", "r")
#infile = open("/home/acorstanje/xmax_statistics_final_99_0pct_12nov2018_noLORAstation2_16_16.dat", "r") #_reiterate_xmax_98_5pct.dat", "r")
event_dict = cPickle.load(infile)

events_passed_all = event_dict['events_passed_all']
events_failed_reco = event_dict['events_failed_reco']
events_failed_biastests = event_dict['events_failed_biastests']

all_events = np.concatenate( (events_passed_all, events_failed_reco, events_failed_biastests))

eventlist_file = '/vol/astro3/lofar/sim/pipeline/eventlist_Nature.txt'
with open(eventlist_file) as f:
    Nature_events = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
Nature_eventlist = [int(x) for x in Nature_events]
Nature_eventlist = np.array(Nature_eventlist)

# Get events from Nature set, from the total event set
indices = np.in1d(all_events[:, 0].astype(int), Nature_eventlist) # of the eventids in all_events also in Nature_eventlist
#all_events = all_events[indices]

eventset_to_process = np.array([ 46822700,  48361669,  48505509,  52168749,  59203421,  59489572,
                                60398398,  60403235,  61280496,  62804598,  62839779,  63246671,
                                64780823,  64951422,  64960703,  65214973,  65431099,  65490891,
                                65827380,  66011236,  66133151,  67362765,  69752374,  69799253,
                                70360902,  70952191,  70988116,  72094854,  72409848,  74019513,
                                74486916,  80495081,  81135035,  81409140,  82312457,  82321543,
                                82432517,  83467151,  83645409,  83826949,  84343882,  86049608,
                                86122409,  86129434,  87181246,  87892283,  88963892,  88979934,
                                89691425,  91754803,  92380604,  94175691,  94294418,  94305032,
                                94347790,  95065594,  95166806,  95193025,  95217701,  95228015,
                                95818679,  96337258,  98436446,  98790022,  99372529,  99383753,
                                101122335, 101140976, 104432286, 104872119, 105147553, 105446090,
                                105465463, 105890593, 109687805, 110147934, 110395704, 111394500,
                                112823955, 113012173, 113663543, 113916058, 118956923, 118957292,
                                118971024, 120768260, 121029645, 121036994, 121086557, 121469103,
                                122146757, 122900449, 123274853, 123291367, 123439073, 124237346,
                                126346457, 126350302, 126356524, 126364120, 126484310, 126485694,
                                126655769, 127108374, 127116620, 127131034, 127163030, 127417834,
                                127670411, 127679389, 127808515, 127862229, 127944584, 128651211,
                                130702848, 130734127, 131043138, 131168450, 131503503, 132649890,
                                133125660, 133264362, 133682287, 133807261, 135102536, 135108067,
                                135111311, 141173952, 148663780, 148673810, 148933412, 148940276,
                                149009957, 149455975, 149565281, 150115145, 150695529, 151062243,
                                151898182, 152517591, 152760079, 153763491, 154029264, 154365352,
                                154386887, 154460397, 154653327, 156278417, 156706320, 156709252,
                                156964925, 157129793, 157571174, 157890023, 158050599, 158064938,
                                158978461, 160198915, 160388966, 160498387, 160673071, 161112298,
                                161326774, 161607531, 162181149, 162192921, 164228080, 164741090,
                                165811310, 165813495, 166570874, 167252541, 167287137, 168482627,
                                168835711, 171909173, 172055925, 172056545, 174634099, 174699876,
                                174841777, 174895044, 175038176, 175485680, 176396082, 177109395,
                                177295087, 177302047, 177546663, 178377733, 181115064, 181699538,
                                181699926, 181702003, 181793052, 181863477, 183392473, 183980193,
                                183983113, 185810673, 186342599, 186365825, 186394895, 186498981,
                                187744257, 188181226, 188735056, 188894224, 190918126, 190923613,
                                191011587, 191230895, 191524276, 191534566, 191558895, 191660905,
                                191905727, 192006137, 192409194, 192665522, 192827003, 193057293,
                                193214722, 193286518, 195878457, 196164498, 196178017, 196325549,
                                196782042, 196790866, 196796518, 197098745, 197420330, 198421440,
                                198445118, 198564642, 198575473, 198651253, 199053317, 199117487,
                                200724160, 201072907, 201143394, 202265278, 202366728, 203043496,
                                203450802, 205634899, 206932524, 207606132, 211274458, 212089170,
                                212636270, 216660305,  59449325,  80516678,  80763503,  81147431,
                                86132542,  87202232,  90630801, 105245903, 120628652, 120807891,
                                120812426, 126296983, 127176859, 127818680, 132729033, 133183721,
                                134522987, 148380084, 149538803, 152763723, 153878589, 154054440,
                                154385690, 161607981, 162063213, 180418078, 181512972, 183985109,
                                196809685, 197585707, 207028458, 211084564, 212398914, 212522097])

#indices = np.in1d(all_events[:, 0].astype(int), eventset_to_process)
#all_events = all_events[indices]
sortingIndices = np.argsort(all_events[:, 0])
all_events = all_events[sortingIndices]
# Do only / exactly this set


#event_entry = [eventid, zenith, azimuth, d_ratio, p_ratio, energy, xreco, xreco_radio_only, sigma_x, sigma_e, std_core, sigma_x_combined, sigma_e_combined, std_core_combined, min_chance_of_hit, stations_with_sufficient_pulses, nfailed, particle_biastest_passed, radio_biastest_passed, quality_check_passed]

(eventids, zeniths, azimuths, d_ratios, p_ratios, energies, xrecos, xrecos_radio_only, sigma_x, sigma_e, sigma_logE_radio, std_core, sigma_x_combined, sigma_e_combined, std_core_combined, combchi2, radiochi2, p_passed, r_passed, q_passed) = getDataArrays(all_events)


###
sigma_x = np.sqrt(sigma_x**2 + 4.0**2 + 2.0**2)
###

################
p_ratios[np.where(p_ratios<0)] = 1.0
energies *= np.sqrt(p_ratios) # REMOVED p_ratios/2 (Feb 28, 2019)
#energies *= 0.8

manually_flag_events = [67362765, 69752374, 133807261, 207606132, 216812747, 198421440, 198575473, 198651253, 192665522, 199386296]

manual_flags = np.logical_not(np.in1d(eventids, manually_flag_events))
# returns per eventid 0 if flagged, 1 if not

assert len(manually_flag_events) == len([id for id in eventids if id in manually_flag_events]) # check for typos...
################


sel_indices = np.where( (p_passed == 1) & (r_passed == 1) )

for ind in sel_indices[0]:
    print 'Event %d: particle passed %d, radio passed %d, E = %1.2e, stdX_comb = %3.2f, std_X_radio = %3.2f, stdcore_comb = %3.2f, stdcore_radio = %3.2f' % (eventids[ind], p_passed[ind], r_passed[ind], energies[ind], sigma_x_combined[ind], sigma_x[ind], std_core_combined[ind], std_core[ind])

print ' '
print 'Histogram of all & failed events versus energy'
print 'Radio, particle bias test passed, radio-only mcvsmc stdcore < 15 m'

# 3 steps to discard events for histogram
sel_indices_p_failed = np.where( (p_passed == 0) )

sel_indices_r_failed = np.where( (p_passed == 0) | (r_passed == 0) )

sel_indices_q_failed = np.where( (p_passed == 0) | (r_passed == 0) | (std_core > 10) )

plt.figure()
bins = np.arange(16.0, 19.0, 0.2)
log10_energies = np.log10(energies)
hist, bins, patches = plt.hist(log10_energies, bins=bins)
hist_q_failed, bins, patches = plt.hist(log10_energies[sel_indices_q_failed], bins=bins, color='r', label='Failed stdcore < 15 m test')
hist_r_failed, bins, patches = plt.hist(log10_energies[sel_indices_r_failed], bins=bins, color='g', label='Failed radio bias test')
hist_p_failed, bins, patches = plt.hist(log10_energies[sel_indices_p_failed], bins=bins, color='m', label='Failed particle bias test')
plt.ylabel('# events')
plt.xlabel(r'$log_{10}$ of energy [ eV ]')
if not plot_publishable:
    plt.title('Histogram of events passing / failing each of 3 tests, versus energy')
plt.legend()

plt.figure()

#hist, bins = np.histogram(x, bins = 50)
#max_val = max(hist)
#hist = [ float(n)/max_val for n in hist]

hist_p_failed = 100 * hist_p_failed.astype(float) / hist
hist_r_failed = 100 * hist_r_failed.astype(float) / hist
hist_q_failed = 100 * hist_q_failed.astype(float) / hist

# Plot the resulting histogram
center = (bins[:-1]+bins[1:])/2
width = 0.95*(bins[1]-bins[0])

plt.bar(center, hist_q_failed, align = 'center', width = width, color='r')
plt.bar(center, hist_r_failed, align = 'center', width = width, color='g')
plt.bar(center, hist_p_failed, align = 'center', width = width, color='m')
if not plot_publishable:
    plt.title('Percentage of FAILED events vs energy')
plt.ylabel('Percentage [ % ] ')
plt.xlabel(r'$log_{10}$ of energy [ eV ]')

print ' '
print 'Scatter plot of std_core_combined versus std_core_radioonly'

plt.figure()
plt.scatter(std_core_combined[sel_indices], std_core[sel_indices])
plt.xlabel('Std core combined [ m ]')
plt.ylabel('Std core radio only [ m ]')
plt.plot([0.0, 60.0], [0.0, 60.0], lw=2, c='r')
if not plot_publishable:
    plt.title('Core uncertainty, radio-only versus combined fit')

print 'Scatter plot of std core combined versus std_Xmax'
plt.figure()
plt.scatter(std_core_combined[sel_indices], sigma_x_combined[sel_indices])
plt.xlabel('Std core combined [ m ]')
plt.ylabel('std Xmax [ m ]')
if not plot_publishable:
    plt.title('Xmax uncertainty versus core uncertainty from combined fit')


sel_indices = np.where( (p_passed == 1) & (r_passed == 1) & (xrecos < 10) & (std_core < 10) )

print ' '
print 'Events with NO reconstruction, passing bias tests, sigma_core < 10 m:'

for ind in sel_indices[0]:
    print 'Event %d: particle passed %d, radio passed %d, E = %1.2e, stdX_comb = %3.2f, std_X_radio = %3.2f, stdcore_comb = %3.2f, stdcore_radio = %3.2f' % (eventids[ind], p_passed[ind], r_passed[ind], energies[ind], sigma_x_combined[ind], sigma_x[ind], std_core_combined[ind], std_core[ind])

print eventids[sel_indices]

###
rescale_energies = False
if rescale_energies:
    print 'Rescale energies with sqrt(p_ratio / 2)... (consistent with earlier energy scale)'
    rescale_factor = [np.sqrt(x/2) if (x<18) else 1.0 for x in p_ratios]
    rescale_factor = np.array(rescale_factor)
    energies *= rescale_factor
###

print ' '
print '# Events passing all cuts: '

# Cutout window in time... (Nov 2018)
cutout_start = 2015.70
cutout_end = 2017.150 # from 15-bin test on full data

another_cutout_start = (2018.2 - 2010) * 365.25 * 24 * 3600

eventid_cutout_start = (cutout_start - 2010.0) * 365.25 * 24 * 3600
eventid_cutout_end = (cutout_end - 2010.0) * 365.25 * 24 * 3600

eventid_cutout = ((eventids < eventid_cutout_start) | (eventids >= eventid_cutout_end)) & (eventids < another_cutout_start)

no_cuts_sel_indices = np.where( (p_passed != -1) & (r_passed != -1) & (xrecos > 10) & (std_core < 15000) & (energies > 1.0e15) )
#sel_indices = np.where( (p_passed >= 0) & (r_passed == 1) & (xrecos > 10) & (std_core < 10) & (energies > 1.0e17) )
sel_indices_all_passed = np.where( (p_passed == 1) & (r_passed == 1) & (xrecos > 10) & (std_core < 7.5) & (energies > 1.0e17) & (energies < 1.0e18) & manual_flags & eventid_cutout )
                                  # & (eventids <= np.median(eventids)) )
sel_indices_part_failed = np.where( (p_passed == 0) & (r_passed == 1) & (xrecos > 10) & (std_core < 7.5) & (energies > 1.0e17) & (energies < 1.0e18) & manual_flags)# & eventid_cutout)
sel_indices_radio_failed = np.where( (p_passed == 1) & (r_passed == 0) & (xrecos > 10) & (std_core < 7.5) & (energies > 1.0e17) & (energies < 1.0e18) & manual_flags) #& eventid_cutout)
sel_indices_both_failed = np.where( (p_passed == 0) & (r_passed == 0) & (xrecos > 10) & (std_core < 7.5) & (energies > 1.0e17) & (energies < 1.0e18) & manual_flags) # & eventid_cutout)
sel_indices_all_passed_relaxed_core_cutoff = np.where( (p_passed == 1) & (r_passed == 1) & (xrecos > 10) & (std_core < 50) & (energies > 1.0e17) & (energies < 1.0e18) & manual_flags & eventid_cutout)

###
sel_indices = sel_indices_all_passed # Require all defined criteria to be passed
###

print len(sel_indices[0])
"""
sel_indices_writeout = np.where( (xrecos > 10) & (std_core < 10) & (energies > 1.0e17) & (energies < 1.0e18) & manual_flags)

eventids_writeout = eventids[sel_indices_writeout]
xrecos_writeout = xrecos[sel_indices_writeout]
sigma_x_writeout = sigma_x[sel_indices_writeout]
#std_core_all_passed = std_core[sel_indices]
energies_writeout = energies[sel_indices_writeout]
log10_energies_writeout = np.log10(energies_writeout)
#sigma_e_combined_all_passed = sigma_e_combined[sel_indices]
p_passed_writeout = p_passed[sel_indices_writeout]
r_passed_writeout = r_passed[sel_indices_writeout]

outfile = open('/vol/astro2/users/acorstanje/xmax_dataset_stdcore_10_1e17_1e18_18oct2018.txt', 'w')
for i, id in enumerate(eventids_writeout):
    outstr = '%d %4.3f %4.3f %2.4f %2.4f %d %d\n' % (id, xrecos_writeout[i], sigma_x_writeout[i], log10_energies_writeout[i], 0.12, p_passed_writeout[i], r_passed_writeout[i])
    outfile.write(outstr)
outfile.close()
"""

sel_description = "After radio bias & core precision cut: %d events" % len(sel_indices[0])

eventids_all_passed = eventids[sel_indices].astype(int)
# write this out to file if needed
outfile = open("/vol/astro2/users/acorstanje/xmax_statistics_fullset_passed_196.dat", "w")
for id in eventids_all_passed:
    outfile.write(str(id)+'\n')
outfile.close()

# Check these arrays vs per-event data!!
xrecos_all_passed = xrecos[sel_indices]
sigma_x_all_passed = sigma_x[sel_indices]
sigma_logE_radio_all_passed = sigma_logE_radio[sel_indices]
std_core_all_passed = std_core[sel_indices]
energies_all_passed = energies[sel_indices]
log10_energies_all_passed = np.log10(energies_all_passed)
sigma_e_combined_all_passed = sigma_e_combined[sel_indices]
d_ratios_all_passed = d_ratios[sel_indices]
p_ratios_all_passed = p_ratios[sel_indices]
combchi2_all_passed = combchi2[sel_indices]
radiochi2_all_passed = radiochi2[sel_indices]
zeniths_all_passed = zeniths[sel_indices]
azimuths_all_passed = azimuths[sel_indices]

# HACKKKKKK 5 mrt 2019. Account for xmax bias
#bias_file = open('/home/acorstanje/xmax_bias_dict_5mrt2019_198.dat', 'r')
#bias_dict = cPickle.load(bias_file)
#for i, eventid in enumerate(eventids_all_passed):
#    xrecos_all_passed[i] += bias_dict[eventid]
# End hack. See what comes out.


print 'Scatter plot of std core radio-only versus std_Xmax'
plt.figure()
plt.scatter(std_core[sel_indices_all_passed_relaxed_core_cutoff], sigma_x[sel_indices_all_passed_relaxed_core_cutoff])
plt.plot([7.5, 7.5], [0.0, 80.0], c='r', lw=3, linestyle=':', label='Chosen cutoff for core uncertainty')
plt.xlabel('Core position uncertainty from radio-only fit [ m ]')
plt.ylabel(r'Uncertainty on $X_{max}$ [ $g/cm^2$ ]')
plt.xlim(0, 30)
plt.ylim(0, 80)
plt.legend(loc='best')
if not plot_publishable:
    plt.title('Xmax uncertainty versus core uncertainty from radio-only fit')
else:
    plt.savefig(os.path.join(save_plot_dir, 'Xmax_vs_core_uncertainty_radio_only.pdf'))


# Histogram of Xmax
plt.figure()
plt.hist(xrecos_all_passed, bins=12)
plt.title('Histogram of Xmax over %d events' % len(sel_indices[0]))
plt.xlabel(r'$X_{max}$ [ $g/cm^2$ ]')
plt.ylabel('# events')



# Histogram of <Xmax> versus energy
bins = np.arange(17.0, 19.0, 0.25)
y = []
error = []
x = []
nvalues = []
for i, bin in enumerate(bins):
    lowerbound = bin
    upperbound = bin + (bins[1] - bins[0])
    thisSelection = np.where( (log10_energies_all_passed > lowerbound) & (log10_energies_all_passed <= upperbound) )
    
    thisXmaxSet = xrecos_all_passed[thisSelection]
    nofValues = len(thisXmaxSet)
    mean_xmax = np.mean(thisXmaxSet)
    unc_xmax = np.std(thisXmaxSet) / np.sqrt(nofValues)
    
    if i < 3:
        plt.figure()
        plt.hist(thisXmaxSet)
        plt.title('Energy between %2.3f and %2.3f' % (lowerbound, upperbound))
    
    print 'Energy between %2.3f and %2.3f:' % (lowerbound, upperbound)
    print 'Mean Xmax = %3.3f' % mean_xmax
    print 'Std error of mean = %3.3f' % unc_xmax
    print '# events in this bin: %d' % (nofValues)

    if nofValues > 0:
        x.append(bin + 0.5 * (bins[1] - bins[0]))
        y.append(mean_xmax)
        error.append(unc_xmax)
        nvalues.append(nofValues)


plt.figure()

OldLOFAR_E = np.array([17.125, 17.375, 17.625 + 0.015])
OldLOFAR_X = np.array([634.0, 652.0, 666.0])
OldLOFAR_X_err = 2.5 * np.array([3.0, 4.0, 7.0]) # !! (AC, Nov 2018) Error bars scaled by ~ 2.5, to have ~ 50 g/cm2 distribution stddev instead of measurement uncertainty ~ 20 g/cm2

oldlofar = plt.errorbar(OldLOFAR_E, OldLOFAR_X, yerr=OldLOFAR_X_err, ecolor='k', marker='o', markersize=10, elinewidth=2, color='k', linestyle='', label='LOFAR (2016)')


plt.errorbar(x, y, yerr=error, ecolor='b', elinewidth=2, linestyle='', marker='o', markersize=10, color='b', label='LOFAR (2019)')

# Linear fit through LOFAR(2019) points
"""
x = np.array(x)
y = np.array(y)
error = np.array(error)
fit, cov = np.polyfit(x, y, 1, w=(1.0 / error), cov=True)
fine_lgE_range = np.arange(17.0, 18.0, 0.01)
linfit_line = np.polyval(fit, fine_lgE_range)

plt.plot(fine_lgE_range, linfit_line, lw=3, c='r')

error_fit = np.sqrt(np.diag(cov))
slope_value = fit[0]
error_slope = error_fit[0]
print 'Linear fit to LOFAR <Xmax>, slope = %2.3f, intercept = %4.3f' % (slope_value, fit[1])
"""
# End linear fit


plt.ylim(640, 720)
for i, value in enumerate(nvalues):
    plt.annotate(str(value), (x[i] + 0.025, y[i]))

models_E=np.array([17,17.5,18,18.5,19, 19.5, 20])
QGSJetII04_p_X=np.array([682.2,704.7,733.5,763.0,784.2,815.6,843.2])
QGSJetII04_Fe_X=np.array([574.3,605.7,636.4,665.3,694.8,720.4,747.4])
EPOSLHC_p_X=np.array([689.2,720.6,745.0,775.8,805.3,834.7,861.7])
EPOSLHC_Fe_X=np.array([583.2,617.2,649.2,679.3,710.1,737.7,766.5])
Sibyll2_1_p_X = np.array([679.49, 710.24, 740.28, 740, 740, 740, 740])
Sibyll2_1_Fe_X = np.array([583.4, 612.3, 641.1, 640, 640, 640, 640])

qp=plt.plot(models_E,QGSJetII04_p_X,c='g', lw=2, marker="o", ls="-")
ep=plt.plot(models_E,EPOSLHC_p_X,c='g', lw=2, marker="o", ls="--")
sp=plt.plot(models_E, Sibyll2_1_p_X, c='g', lw=2, marker="o", ls=":")

qi=plt.plot(models_E,QGSJetII04_Fe_X,c='r', lw=2, marker="o", ls="-")
ei=plt.plot(models_E,EPOSLHC_Fe_X,c='r', lw=2, marker="o", ls="--")
si=plt.plot(models_E, Sibyll2_1_Fe_X, c='r', lw=2, marker="o", ls=":")


Auger2017_E = np.arange(17.25, 17.97, 0.1) # fp roundoff
Auger2017_E[0] += 0.01 # 17.26 for some reason
Auger2017_X = np.array([665.16, 679.78, 681.8, 688.53, 694.45, 704.65, 711.78, 719.88])
Auger2017_X_err = np.array([2.61, 2.08, 1.61, 1.61, 1.3, 1.65, 0.58, 0.77])

auger2017 = plt.errorbar(Auger2017_E, Auger2017_X, yerr=Auger2017_X_err, ecolor='gray', marker='*', markersize=12, elinewidth=2, color='gray', linestyle='', label='Pierre Auger (2017)')

# Linear fit through Auger data, not counting first point
"""
fit, cov = np.polyfit(Auger2017_E[1::], Auger2017_X[1::], 1, w=(1.0 / Auger2017_X_err[1::]), cov=True)
fine_lgE_range = np.arange(17.0, 18.0, 0.01)
linfit_line = np.polyval(fit, fine_lgE_range)

plt.plot(fine_lgE_range, linfit_line, lw=3, c='r')

error_fit = np.sqrt(np.diag(cov))
slope_value = fit[0]
error_slope = error_fit[0]
print 'Linear fit to Auger-2017 <Xmax>: slope = %2.3f, intercept = %4.3f' % (slope_value, fit[1])
"""
# End linear fit


HiresMia_2000_E=np.array([4.92, 5.08, 5.25, 5.43, 5.61, 5.77, 5.95, 6.13 ]) # from preprint
HiresMia_2000_X=np.array([610.5,621.65,651.4,663.95,669.5,703.98,718.65,716.4])
HiresMia_2000_X_err=np.array([6.0,5.05,4.6,5.45,5.65,9.0,19.4,22.2])

# T. Abu-Zayyad et al. [HiRes/MIA Coll.], Astrophys. J. 557 (2001) 686.


Hires_2010_E=np.array([18.3,18.5,18.7,18.9,19.1,19.3,19.5,19.7])
Hires_2010_X=np.array([717.0,730.3,751,751,761,765.5,765.5,785])+26 # Delta = 26 (Kampert & Ungert)
Hires_2010_X_err=np.array([4.4,3.2,4.8,5.9,6.7,7.7,15.3,15.5])
# HiRes 2010 : R. Abbasi et al. [HiRes Coll.], Phys. Rev. Lett. 104 (2010) 161101.

#Tunka_2013_E=np.array([3.85,3.95,4.05,4.15,4.25,4.35,4.45,4.55,4.65,4.75,4.85,4.95,5.12,5.40,5.74])
#Tunka_2013_X=np.array([558.8,562.4,565.9,570.9,572.4,577.4,578.8,584.6,593.9,596.0,603.9,608.2,623.9,648.2,666.8])
#Tunka_2013_X_err=np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,4,16])

Tunka_2015_E = np.array([17.09, 17.29, 17.49, 17.69, 17.89])
Tunka_2015_X = np.array([627.4, 644.2, 657.4, 664.5, 688.3])
Tunka_2015_X_err = np.array([2.0, 2.0, 4.0, 13.0, 17.0])

Yakutsk_2015_E = np.array([17.00, 17.12, 17.24, 17.30, 17.48, 17.66, 17.77, 17.94])
Yakutsk_2015_X = np.array([620.6, 632.6, 630.9, 646.7, 653.3, 668.4, 685.1, 683.0])
Yakustk_2015_X_err = np.array([8.0, 7.0, 6.0, 6.0, 7.0, 7.0, 8.0, 8.0])

#Yakutsk_E=np.array([1.5e17, 2.0e17, 3.0e17, 4.5e17, 5.8e17, 8.6e17, 1.2e18, 1.7e18, 2.4e18, 3.5e18, 5.0e18, 8.0e18, 1.1e19, 1.6e19, 2.4e19, 3.5e19, 5.1e19])
#Yakutsk_X=np.array([632,648,655,670,687,685,700,719,723,738,765,761,786,780,778,785,808])
#Yakutsk_X_err=np.array([5,5,6,6,7,7,7,7,8,8,9,11,13,18,21,26,25])



if plot_publishable:
    mia=plt.scatter(HiresMia_2000_E+12,HiresMia_2000_X,marker="v", c='gray', s=40, label='HiRes/Mia')
    plt.errorbar(HiresMia_2000_E+12,HiresMia_2000_X,yerr=HiresMia_2000_X_err,marker=None, fmt=None, ecolor='gray')
    #    yak=plt.scatter(np.log10(Yakutsk_E), Yakutsk_X, marker="s", c='gray', s=40, label='Yakutsk')
    #plt.errorbar(np.log10(Yakutsk_E), Yakutsk_X, yerr=Yakutsk_X_err, marker=None, fmt=None, ecolor='r')
    yak = plt.errorbar(Yakutsk_2015_E, Yakutsk_2015_X, yerr=Yakustk_2015_X_err, marker='s', c='gray', label='Yakutsk', linestyle='')
    tunk = plt.errorbar(Tunka_2015_E, Tunka_2015_X, yerr=Tunka_2015_X_err, marker='>', c='gray', label='Tunka', linestyle='')
    #    tunk=plt.scatter(Tunka_2013_E+12, Tunka_2013_X, marker=">", c='gray', s=40, label='Tunka')
    #plt.errorbar(Tunka_2013_E+12, Tunka_2013_X, yerr=Tunka_2013_X_err, marker=None, fmt=None, ecolor='g')

# r"$X_{\mathrm{max}}$ (g/$\mathrm{cm}^2$)"
plt.ylabel(r'$X_{max}$ [ $g/cm^2$ ]')
plt.xlabel('lg (E/eV)')
plt.xlim(17.0, 18.0)
plt.ylim(550, 750)
plt.legend(loc='best', scatterpoints=1, numpoints=1)
plt.grid()
if not plot_publishable:
    plt.title(sel_description)
else:
    plt.savefig(os.path.join(save_plot_dir, 'Avg_xmax_vs_energy.pdf'))

#1/0
                
# Histogram of sigma_Xmax versus energy
bins = np.linspace(17.0, 17.75, 4)
y = []
y_error = []
x = []
nvalues = []
for i, bin in enumerate(bins):
    lowerbound = bin
    upperbound = bin + (bins[1] - bins[0])
    thisSelection = np.where( (log10_energies_all_passed > lowerbound) & (log10_energies_all_passed <= upperbound) )
    thisXmaxSet = xrecos_all_passed[thisSelection]
    if len(thisXmaxSet) == 0:
        continue
    sigma_xmax = np.sqrt(len(thisXmaxSet) / (len(thisXmaxSet) - 1.5)) * np.std(thisXmaxSet)
    # See Wikipedia, unbiased estimate of standard deviation (taking into account small sample size), hence the 1 / (N - 1.5) instead of Numpy's 1/N.

    # Subtract average variance from measurement
    avg_variance = np.average(sigma_x_all_passed[thisSelection]**2)
    sigma_xmax = np.sqrt(sigma_xmax**2 - avg_variance)

    #unc_xmax = np.std(xrecos[thisSelection]) / np.sqrt(len(xrecos[thisSelection]))
    x.append(bin + 0.5 * (bins[1] - bins[0]))
    y.append(sigma_xmax)

    # proceed to do bootstrap resampling to get uncertainty on standard deviation
    # Do bootstrap resampling to get estimate on uncertainty on sample mean and sample stddev
    n = len(thisXmaxSet)
    nvalues.append(n)
    reps = 10000
    bootstrap_xmax_sets = np.random.choice(thisXmaxSet, (n, reps)) # sampling with replacement, create 'reps' mock datasets of size n, from original dataset x with size n.

    # Now use these to get 10000 estimates of the sample mean. The spread on this (stddev) gives an uncertainty on the original sample mean
    mean_bootstrap = bootstrap_xmax_sets.mean(axis=0)
    #mean_bootstrap.sort()

    # And, 10000 estimates of sample stddev. The spread on this is the uncertainty on the original sample stddev.
    std_bootstrap = bootstrap_xmax_sets.std(axis=0)
    sigma_xmax_bootstrap = std_bootstrap**2 - avg_variance
    sigma_xmax_bootstrap = sigma_xmax_bootstrap[sigma_xmax_bootstrap >= 0]
    # It can become < 0 sometimes, and the sqrt gives NaN... What to do about this? Is this the proper error bar, i.e. is the measurement variance subtracted correctly?
    sigma_xmax_bootstrap = np.sqrt(sigma_xmax_bootstrap)
    #std_bootstrap.sort()
    
    error_on_stddev_xmax = np.std(sigma_xmax_bootstrap)
    print 'Error on stddev Xmax: %3.3f' % error_on_stddev_xmax
    print 'Error on unprocessed stddev: %3.3f' % np.std(std_bootstrap)
    
    y_error.append(error_on_stddev_xmax)
    #error.append(unc_xmax)

plt.figure()
plt.errorbar(x, y, yerr=y_error, marker='o', linestyle=' ', elinewidth=2, markersize=10)
plt.plot([17.0, 18.0], [70.6, 65.4], c='g', lw=2, ls="-", label='Protons (QGSJetII-04)')
plt.plot([17.0, 18.0], [24.62, 23.44], c='r', lw=2, ls="-")#, label = 'Iron (QGSJetII-04)')

plt.plot([17.0, 18.0], [62.2, 58.3], c='g', lw=2, ls=":", label='Protons (Sibyll2.1)')
plt.plot([17.0, 18.0], [27.7, 24.5], c='r', lw=2, ls=":")#, label = 'Iron (Sibyll2.1)')


Auger2017_sigmaX = np.array([61.29, 65.38, 59.01, 59.79, 56.93, 61.13, 59.02, 59.71])
Auger2017_sigmaX_err = np.array([3.94, 2.98, 2.51, 2.52, 1.96, 2.63, 0.49, 0.24])

auger2017 = plt.errorbar(Auger2017_E, Auger2017_sigmaX, yerr=Auger2017_sigmaX_err, ecolor='g', marker='v', markersize=10, elinewidth=2, color='k', linestyle='', label='Pierre Auger (2017)')


plt.plot([17.0, 18.0], [66.6, 62.6], c='g', lw=2, ls="--", label='Protons (EPOS-LHC)')
plt.plot([17.0, 18.0], [19.27, 17.91], c='r', lw=2, ls="--")#, label = 'Iron (EPOS-LHC)')

for i, value in enumerate(nvalues):
    plt.annotate(str(value), (x[i] + 0.025, y[i]))

#plt.errorbar(x, y, yerr=error, ecolor='g', elinewidth=2, linestyle='', marker='o', markersize=10, label='LOFAR prelim. (2018)')

plt.xlim(17.0, 18.0)
plt.xlabel(r'lg (E/eV)')
plt.ylabel(r'Standard deviation of $X_{max}$ [ $g/cm^2$ ]')
plt.legend(loc='best', scatterpoints=1, numpoints=1)
# add lines for stddev for p and Fe... add Auger data?
if not plot_publishable:
    plt.title('stddev of Xmax')
else:
    plt.savefig(os.path.join(save_plot_dir, 'Stddev_xmax_vs_energy.pdf'))
#plt.ylim(600, 800)

plt.figure()
plt.hist(d_ratios_all_passed[d_ratios_all_passed < 3])
plt.title('Histogram of d_ratios; there were %d outliers > 3' % (np.sum(d_ratios_all_passed >= 3)))

plt.figure()
plt.hist(np.sqrt(0.5*p_ratios_all_passed[(p_ratios_all_passed > 0) & (0.5*p_ratios_all_passed < 9)]))
plt.title('Histogram of sqrt(p_ratios/2); there were %d outliers > sqrt(9)' % (np.sum(0.5*p_ratios_all_passed >= 9)))


plt.figure()
plt.hist(sigma_x_all_passed, bins=20)
plt.xlabel(r'Uncertainty on $X_{max}$ [ $g/cm^2$ ]')
plt.ylabel('Counts')

plt.figure()
plt.hist(radiochi2_all_passed, bins=25)
plt.xlabel('Reduced chi-squared, radio-only fit')
plt.ylabel('Counts')

#plt.scatter(d_ratios_all_passed, p_ratios_all_passed)

# Check for weird error, p_ratio < 0 and still passing all cuts... ???!!
if min(p_ratios_all_passed) < 0:
    print 'Error! Events with p_ratio < 0:'
    print eventids_all_passed[np.where(p_ratios_all_passed < 0)]
else:

    plt.figure()
    plt.scatter(np.log10(d_ratios_all_passed), np.log10(p_ratios_all_passed))
    plt.figure()
    plt.hist(np.log10(d_ratios_all_passed / np.sqrt(p_ratios_all_passed)), bins=15)
    plt.title('Histogram of log10(d_ratio / p_ratio)')
    plt.xlabel('log10(d_ratio / p_ratio')
    plt.ylabel('# events')

print 'Writing event list passing all cuts...'
outfile = open('/vol/astro2/users/acorstanje/eventlist_allpassed_oct4.txt', 'w')
for eventid in eventids_all_passed:
    outfile.write('%d\n' % int(eventid))
outfile.close()

# Write Xmax, E dataset to txt file
# Sort by energy
sortingIndices = np.argsort(log10_energies_all_passed)

sorted_eventids = eventids_all_passed[sortingIndices]
sorted_xmax = xrecos_all_passed[sortingIndices]
sorted_sigma_x = sigma_x_all_passed[sortingIndices]
sorted_log_energies = log10_energies_all_passed[sortingIndices]
sorted_sigma_logE_radio = sigma_logE_radio_all_passed[sortingIndices]

#sorted_sigma_e = np.ones(len(sorted_xmax)) * np.log10(1.33) # Take constant, factor 1.33
# Now inserting actual sigma_logE based on radio (Nov 5, 2018)

print 'Mean   sigma_X = %2.1f' % np.average(sorted_sigma_x)
print 'Median sigma_X = %2.1f' % np.median(sorted_sigma_x)
print 'Writing Xmax, E dataset passing all cuts...'
outfile = open('/home/acorstanje/xmax_dataset_allpassed_1e17_1e18_6mrt2019_196.txt', 'w')
for i, id in enumerate(sorted_eventids):
    outstr = '%d %4.3f %4.3f %2.4f %2.4f\n' % (id, sorted_xmax[i], sorted_sigma_x[i], sorted_log_energies[i], sorted_sigma_logE_radio[i])
    outfile.write(outstr)
outfile.close()
print ' '

print '## Two-bin tests ##'

# Get two-bin uncertainty / p-values for random shuffling

Ntrials = 10000
trial_splits = np.zeros(Ntrials)
Nevents = len(eventids_all_passed)
indices = np.arange(Nevents)
for i in range(Ntrials):
    np.random.shuffle(indices)
    avg1 = np.average(xrecos_all_passed[indices][0:Nevents/2])
    avg2 = np.average(xrecos_all_passed[indices][Nevents/2::])
    split = avg2 - avg1
    trial_splits[i] = split

trial_splits = np.sort(trial_splits)
print '### %d shuffled two-bin evaluations:' % Ntrials
print ' '
print 'RMS split        : %3.2f g/cm2' % np.std(trial_splits)
print 'avg split, no abs: %3.2f g/cm2' % np.average(trial_splits)
print '2 pct split      : %3.2f g/cm2' % trial_splits[Ntrials / 50]
print '5 pct split      : %3.2f g/cm2' % trial_splits[Ntrials / 20]
print '95 pct split     : %3.2f g/cm2' % trial_splits[19*Ntrials/20]
print '98 pct split     : %3.2f g/cm2' % trial_splits[49*Ntrials/50]
print ' '
print '###'

median_zenith_angle = np.median(zeniths_all_passed)
print 'Median zenith angle: %3.2f' % (median_zenith_angle * 180.0 / np.pi)
low_bin = xrecos_all_passed[zeniths_all_passed < median_zenith_angle]
high_bin = xrecos_all_passed[zeniths_all_passed >= median_zenith_angle]

low_avg = np.average(low_bin)
high_avg = np.average(high_bin)
unc_low = np.std(low_bin) / np.sqrt(len(low_bin))
unc_high = np.std(high_bin) / np.sqrt(len(high_bin))

print 'Average Xmax low zenith angles : %3.2f' % low_avg
print 'Average Xmax high zenith angles: %3.2f' % high_avg
print 'Split: %3.2f' % np.abs(high_avg - low_avg)
print ' '
print 'uncertainty low bin : %3.2f' % unc_low
print 'uncertainty high bin: %3.2f' % unc_high

print ' '
median_event_id = np.median(eventids_all_passed)
print 'Median event id = %d' % median_event_id
low_bin = xrecos_all_passed[eventids_all_passed < median_event_id]
high_bin = xrecos_all_passed[eventids_all_passed >= median_event_id]

low_avg = np.average(low_bin)
high_avg = np.average(high_bin)
unc_low = np.std(low_bin) / np.sqrt(len(low_bin))
unc_high = np.std(high_bin) / np.sqrt(len(high_bin))

print 'Average Xmax early times: %3.2f' % low_avg
print 'Average Xmax later times: %3.2f' % high_avg
print 'Split: %3.2f' % np.abs(high_avg - low_avg)
print ' '
print 'uncertainty low bin : %3.2f' % unc_low
print 'uncertainty high bin: %3.2f' % unc_high

# Read in pressure per event
pressures = []
g = 9.80665 # m/s^2 gravity acceleration
for id in eventids_all_passed:
    filename = '/vol/astro7/lofar/sim/pipeline/atmosphere_files/ATMOSPHERE_%d.DAT' % id
    with open(filename) as f:
        header = f.readline()
        layers = f.readline()
        a_line = f.readline()
        b_line = f.readline()
        c_line = f.readline()

    a_values = a_line.split()
    b_values = b_line.split()
    Xground = float(a_values[0]) + float(b_values[0])
    #print 'event %d: Xground = %4.3f' % (id, Xground)

    pressures.append(Xground * g / 10)

pressures = np.array(pressures)
median_pressure = np.median(pressures)

low_bin = xrecos_all_passed[pressures < median_pressure]
high_bin = xrecos_all_passed[pressures >= median_pressure]

low_avg = np.average(low_bin)
high_avg = np.average(high_bin)
unc_low = np.std(low_bin) / np.sqrt(len(low_bin))
unc_high = np.std(high_bin) / np.sqrt(len(high_bin))
print ' '
print 'Median pressure = %4.3f' % median_pressure
print ' '
print 'Average Xmax low pressure : %3.2f' % low_avg
print 'Average Xmax high pressure: %3.2f' % high_avg
print 'Split: %3.2f' % np.abs(high_avg - low_avg)
print ' '
print 'uncertainty low bin : %3.2f' % unc_low
print 'uncertainty high bin: %3.2f' % unc_high

print ' '

zeniths_all_passed *= 180.0 / np.pi
zeniths *= 180.0 / np.pi
median_zenith_angle *= 180.0 / np.pi
plt.figure()
plt.scatter(zeniths, std_core)
plt.ylim(0.0, 50.0)

std_core_low_zen = std_core_all_passed[zeniths_all_passed < median_zenith_angle]
std_core_high_zen = std_core_all_passed[zeniths_all_passed >= median_zenith_angle]

low_avg = np.average(std_core_low_zen)
high_avg = np.average(std_core_high_zen)

low_above_5 = np.sum(std_core_low_zen > 5)
low_above_10 = np.sum(std_core_low_zen > 10)
high_above_5 = np.sum(std_core_high_zen > 5)
high_above_10 = np.sum(std_core_high_zen > 10)

print 'Average std_core low zenith angles : %2.2f' % low_avg
print 'Average std_core high zenith angles: %2.2f' % high_avg
print '# above 5 m std_core, low zenith   : %d' % low_above_5
print '# above 10 m std_core, low zenith  : %d' % low_above_10
print '# above 5 m std_core, high zenith  : %d' % high_above_5
print '# above 10 m std_core, high zenith : %d' % high_above_10

print ' '
print '3 zenith angle bins'
sorted_zenith_angles = np.sort(zeniths_all_passed)
perc1 = 40.0
perc2 = 75.0
zenith1 = sorted_zenith_angles[int(perc1/100 * np.round(len(sorted_zenith_angles)))]
zenith2 = sorted_zenith_angles[int(perc2/100 * np.round(len(sorted_zenith_angles)))]
bin0 = xrecos_all_passed[zeniths_all_passed < zenith1]
bin1 = xrecos_all_passed[(zeniths_all_passed >= zenith1) & (zeniths_all_passed < zenith2)]
bin2 = xrecos_all_passed[zeniths_all_passed >= zenith2]

print 'Average zenith bin 0 (%2.1f percentile, up to %3.2f degrees): %4.2f g/cm2' % (perc1, zenith1, np.average(bin0))
print 'Average zenith bin 1 (%2.1f percentile, up to %3.2f degrees): %4.2f g/cm2' % (perc2, zenith2, np.average(bin1))
print 'Average zenith bin 2 (>= %3.2f degrees)                     : %4.2f g/cm2' % (zenith2, np.average(bin2))

# Graph of <Xmax> in k zenith bins

xrecos_corrected_all_passed = xrecos_all_passed - 55.0 * (log10_energies_all_passed - 17.4)

nofbins = 6
bins = np.arange(5.0, 53.0+8.0, 8.0)
#bins = np.linspace(min(eventids_all_passed), max(eventids_all_passed), nofbins+1)
plt.figure()
n, bins, patches = plt.hist(zeniths_all_passed, bins=bins)
xmax_zenith_averages = []
xmax_zenith_uncert = []
xmax_corrected_zenith_averages = []
xmax_corrected_uncert = []
for i in range(nofbins):
    thisXmaxdata = xrecos_all_passed[(zeniths_all_passed >= bins[i]) & (zeniths_all_passed < bins[i+1])]
    #thisXmaxdata = xrecos_all_passed[(eventids_all_passed >= bins[i]) & (eventids_all_passed < bins[i+1])]
    thisAverage = np.average(thisXmaxdata)
    thisUnc = np.std(thisXmaxdata) / np.sqrt(len(thisXmaxdata))
    xmax_zenith_averages.append(thisAverage)
    xmax_zenith_uncert.append(thisUnc)

    thisXmax_corrected = xrecos_corrected_all_passed[(zeniths_all_passed >= bins[i]) & (zeniths_all_passed < bins[i+1])]
    #thisXmax_corrected = xrecos_corrected_all_passed[(eventids_all_passed >= bins[i]) & (eventids_all_passed < bins[i+1])]
    
    thisAverage = np.average(thisXmax_corrected)
    thisUnc = np.std(thisXmax_corrected) / np.sqrt(len(thisXmax_corrected))

    xmax_corrected_zenith_averages.append(thisAverage)
    xmax_corrected_uncert.append(thisUnc)

binspacing = bins[1] - bins[0]
xmax_zenith_averages = np.array(xmax_zenith_averages)
xmax_zenith_uncert = np.array(xmax_zenith_uncert)

xmax_corrected_zenith_averages = np.array(xmax_corrected_zenith_averages)
xmax_corrected_uncert = np.array(xmax_corrected_uncert)


plt.figure()
plt.errorbar(bins[0:-1] + 0.5 * binspacing, xmax_corrected_zenith_averages, yerr=xmax_corrected_uncert, marker='o', markersize=10, elinewidth=2, linestyle=' ', label=r'Avg $Y_{max}$ (corrected for energy)')
plt.scatter(bins[0:-1] + 0.5 * binspacing, xmax_zenith_averages, c='gray', s=100, label=r'Avg $X_{max}$')
#plt.errorbar(bins[0:-1] + 0.5 * binspacing, xmax_zenith_averages, yerr=xmax_zenith_uncert, marker='o', markersize=10, elinewidth=2, linestyle=' ')
plt.xlabel('Zenith angle [ deg ]')
#plt.xlabel('Event id [ sec since 2010 ]')
plt.ylabel(r'<$X_{max}$>, <$Y_{max}$> [ $g/cm^2$ ]')
#plt.grid()
plt.xlim(bins[0], bins[nofbins])
plt.ylim(620, 760)
#if not plot_publishable:
#    plt.title('Average Xmax in %d zenith angle bins' % nofbins)
#else:
#    plt.savefig(os.path.join(save_plot_dir, 'Avg_xmax_6bins_all_passed.pdf'))

#plt.figure()
#plt.scatter(bins[0:-1] + 0.5 * binspacing, xmax_zenith_averages, s=100)
#plt.xlabel('Zenith angle [ deg ]')
#plt.ylabel(r'<Y> [ $g/cm^2$ ]')
plt.xlim(bins[0], bins[nofbins])
plt.ylim(620, 760)
plt.grid()

# Do linear fit & see what the coefficients do

#fit, cov = np.polyfit(bins[0:-2] + 0.5 * binspacing, xmax_corrected_zenith_averages[0:5], 1, w=(1.0 / xmax_corrected_uncert[0:5]), cov=True)
fit, cov = np.polyfit(bins[0:-1] + 0.5 * binspacing, xmax_corrected_zenith_averages, 1, w=(1.0 / xmax_corrected_uncert), cov=True)

fine_theta_range = np.arange(0.0, 55.0, 0.1)
linfit_line = np.polyval(fit, fine_theta_range)

avg_xmax_corrected = np.average(xrecos_corrected_all_passed)
plt.plot(fine_theta_range, linfit_line, lw=2, c='k', label=r'Linear fit to avg. $Y_{max}$')
plt.plot([0.0, 55.0], [avg_xmax_corrected, avg_xmax_corrected], lw=2, c='r', linestyle='--', label=r'Overall average of $Y_{max}$')

error_fit = np.sqrt(np.diag(cov))
slope_value = fit[0]
error_slope = error_fit[0]
print 'Linear fit to <Ymax> vs zenith: slope = %2.3f +/- %2.3f' % (slope_value, error_slope)

plt.legend(loc='best')

# Constant fit with error
fit, cov = np.polyfit(bins[0:-1] + 0.5 * binspacing, xmax_corrected_zenith_averages, 0, w=(1.0 / xmax_corrected_uncert), cov=True)
print ' '
print 'Constant fit to Y vs zenith angle: <Y> = %3.2f +/- %3.2f g/cm2' % (fit[0], np.sqrt(cov[0][0]))
print ' '

if not plot_publishable:
    plt.title('Average Y in %d zenith angle bins' % nofbins)
else:
    plt.savefig(os.path.join(save_plot_dir, 'Avg_xmax_6bins_all_passed_corrected_energy.pdf'))


# Scatter plot of Xmax versus zenith angle.
# Include all-passed, part. bias failed, radio bias failed, both failed (in all cases, core quality check must be passed)

xrecos_part_failed = xrecos[sel_indices_part_failed]
xrecos_radio_failed = xrecos[sel_indices_radio_failed]
xrecos_both_failed = xrecos[sel_indices_both_failed]

zeniths_part_failed = zeniths[sel_indices_part_failed]
zeniths_radio_failed = zeniths[sel_indices_radio_failed]
zeniths_both_failed = zeniths[sel_indices_both_failed]

zeniths_relaxed_core_cutoff = zeniths[sel_indices_all_passed_relaxed_core_cutoff]
xrecos_relaxed_core_cutoff = xrecos[sel_indices_all_passed_relaxed_core_cutoff]

sigma_x_relaxed_core_cutoff = sigma_x[sel_indices_all_passed_relaxed_core_cutoff]

sel_indices_all_passed_high_sigmaX = np.where(sigma_x_all_passed > 50)
xrecos_sigma_above_50m = xrecos_all_passed[sel_indices_all_passed_high_sigmaX]
sigma_x_above_50m = sigma_x_all_passed[sel_indices_all_passed_high_sigmaX]
zeniths_above_50m = zeniths_all_passed[sel_indices_all_passed_high_sigmaX]

# HACK
#zeniths_all_passed = 1.0 - np.cos(zeniths_all_passed * np.pi/180)
#zeniths_above_50m = 1.0 - np.cos(zeniths_above_50m * np.pi/180)
#zeniths_part_failed = 1.0 - np.cos(zeniths_part_failed * np.pi/180)
#zeniths_radio_failed = 1.0 - np.cos(zeniths_radio_failed * np.pi/180)
#zeniths_both_failed = 1.0 - np.cos(zeniths_both_failed * np.pi/180)
# END HACK
plt.figure()
sc_plot = plt.scatter(zeniths_all_passed, xrecos_all_passed, c=log10_energies_all_passed, cmap="jet", marker='o', s=60,label='all criteria passed')
if not plot_publishable:
    plt.errorbar(zeniths_above_50m, xrecos_sigma_above_50m, yerr=sigma_x_above_50m, marker=None, linestyle=' ', ecolor='gray', label=r'$\sigma_X > 50m$')
# r, m, cyan, try gray colors for failed events...
plt.scatter(zeniths_part_failed, xrecos_part_failed, c='gray', marker='x', s=50, label='flagged (particle bias)')
plt.scatter(zeniths_radio_failed, xrecos_radio_failed, c='gray', marker='+', s=60, label='flagged (radio bias)')
plt.scatter(zeniths_both_failed, xrecos_both_failed, c='gray', marker='s', s=40, label='flagged (both)')
#plt.errorbar(zeniths_relaxed_core_cutoff, xrecos_relaxed_core_cutoff, yerr=sigma_x_relaxed_core_cutoff, c='gray', marker='o', linestyle=' ', markersize=8, label='%d passed events w. std_core 10 to 30 m' % len(zeniths_relaxed_core_cutoff))

cbar = plt.colorbar(sc_plot)
plt.ylim(450, 1050)
plt.xlim(0, 60)
plt.legend(loc='upper left')
plt.xlabel('Zenith angle [ deg ]')
plt.ylabel(r'$X_{max}$ [ $g/cm^2$ ]')
cbar.ax.set_ylabel(r'$log_{10}$ of energy in eV')
if not plot_publishable:
    plt.title('Scatter of Xmax versus zenith angle, %d events' % (len(xrecos_all_passed) + len(xrecos_part_failed) + len(xrecos_radio_failed) + len(xrecos_both_failed)))
else:
    plt.savefig(os.path.join(save_plot_dir, 'xmax_scatter_vs_zenith.pdf'))


# Scatter plot versus time (eventid)

event_times = 2010.0 + eventids / (365.25 * 24 * 3600)
event_times_all_passed = event_times[sel_indices_all_passed]
event_times_part_failed = event_times[sel_indices_part_failed]
event_times_radio_failed = event_times[sel_indices_radio_failed]
event_times_both_failed = event_times[sel_indices_both_failed]

plt.figure()
sc_plot = plt.scatter(event_times_all_passed, xrecos_all_passed, c=log10_energies_all_passed, cmap="jet", marker='o', s=60,label='all criteria passed')
#if not plot_publishable:
#    plt.errorbar(zeniths_above_50m, xrecos_sigma_above_50m, yerr=sigma_x_above_50m, marker=None, linestyle=' ', ecolor='gray', label=r'$\sigma_X > 50m$')
# r, m, cyan as failed colors, make gray?
plt.scatter(event_times_part_failed, xrecos_part_failed, c='gray', marker='x', s=40, label='flagged (particle bias)')
plt.scatter(event_times_radio_failed, xrecos_radio_failed, c='gray', marker='+', s=50, label='flagged (radio bias)')
plt.scatter(event_times_both_failed, xrecos_both_failed, c='gray', marker='s', s=40, label='flagged (both)')
#plt.errorbar(zeniths_relaxed_core_cutoff, xrecos_relaxed_core_cutoff, yerr=sigma_x_relaxed_core_cutoff, c='gray', marker='o', linestyle=' ', markersize=8, label='%d passed events w. std_core 10 to 30 m' % len(zeniths_relaxed_core_cutoff))

cbar = plt.colorbar(sc_plot)
plt.ylim(450, 1050)
#plt.xlim(0, 60)
plt.legend(loc='upper left')
plt.xlabel('Event time [ year ]')
plt.ylabel(r'$X_{max}$ [ $g/cm^2$ ]')
cbar.ax.set_ylabel(r'$log_{10}$ of energy in eV')
#if not plot_publishable:
plt.title('Scatter of Xmax versus event time, %d events' % (len(xrecos_all_passed) + len(xrecos_part_failed) + len(xrecos_radio_failed) + len(xrecos_both_failed)))
#else:
#    plt.savefig(os.path.join(save_plot_dir, 'xmax_scatter_vs_zenith.pdf'))

# k-bin plot vs time
nofbins = 6

bins = np.linspace(2011.0, 2019, nofbins+1)
binspacing = bins[1] - bins[0]

digitized = np.digitize(event_times_all_passed, bins)
bin_means = [xrecos_all_passed[digitized == i].mean() for i in range(1, len(bins))]
bin_std = [xrecos_all_passed[digitized == i].std() / np.sqrt(len(xrecos_all_passed[digitized==i])) for i in range(1, len(bins))]

plt.figure()
plt.errorbar(bins[0:-1] + 0.5 * binspacing, bin_means, yerr=bin_std, marker='o', markersize=10, elinewidth=2, linestyle=' ', label=r'Avg $X_{max}$')
#plt.scatter(bins[0:-1] + 0.5 * binspacing, xmax_zenith_averages, c='gray', s=100, label=r'Avg $X_{max}$')
#plt.errorbar(bins[0:-1] + 0.5 * binspacing, xmax_zenith_averages, yerr=xmax_zenith_uncert, marker='o', markersize=10, elinewidth=2, linestyle=' ')
plt.xlabel('Time [ year ]')
#plt.xlabel('Event id [ sec since 2010 ]')
plt.ylabel(r'<$X_{max}$> [ $g/cm^2$ ]')
#plt.grid()
plt.xlim(bins[0], bins[nofbins])
ax = plt.gca()
from matplotlib.ticker import FormatStrFormatter
ax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
plt.ylim(600, 700)
plt.grid()
plt.legend(loc='best')
if plot_publishable:
    plt.savefig(os.path.join(save_plot_dir, 'xmax_vs_time_6bins.pdf'))

std_core_part_failed = std_core[sel_indices_part_failed]
std_core_radio_failed = std_core[sel_indices_radio_failed]
std_core_both_failed = std_core[sel_indices_both_failed]

plt.figure()
plt.scatter(zeniths_all_passed, std_core_all_passed, c='b', marker='o', s=60, label='all criteria passed')
plt.scatter(zeniths_part_failed, std_core_part_failed, c='r', marker='x', s=40, label='particle test failed')
plt.scatter(zeniths_radio_failed, std_core_radio_failed, c='m', marker='x', s=40, label='radio test failed')
plt.plot([0.0, 70.0], [10.0, 10.0], c='r', lw=3, linestyle=':', label='Cutoff for quality criterion')
plt.title('Core position uncertainty vs zenith angle')

plt.xlim(0.0, 70.0)
plt.ylim(-2.0, 45.0)
plt.xlabel('Zenith angle [ deg ]')
plt.ylabel('Core position uncertainty [ m ]')
plt.legend()

sigma_x_part_failed = sigma_x[sel_indices_part_failed]
sigma_x_radio_failed = sigma_x[sel_indices_radio_failed]
sigma_x_both_failed = sigma_x[sel_indices_both_failed]

plt.figure()

sigma_x_relaxed_core_cutoff = sigma_x[sel_indices_all_passed_relaxed_core_cutoff]
plt.scatter(zeniths_all_passed, sigma_x_all_passed, c='b', marker='o', s=60, label='all criteria passed')
plt.scatter(zeniths_part_failed, sigma_x_part_failed, c='r', marker='x', s=40, label='particle test failed')
plt.scatter(zeniths_radio_failed, sigma_x_radio_failed, c='m', marker='x', s=40, label='radio test failed')
plt.scatter(zeniths_relaxed_core_cutoff, sigma_x_relaxed_core_cutoff, c='lightgray', marker='o', s=40, label='Core uncertainty 10-30 m')

plt.title('Xmax uncertainty vs zenith angle')
plt.xlabel('Zenith angle [ deg ]')
plt.ylabel(r'Uncertainty on $X_{max}$ [ $g/cm^2$ ]')
plt.legend(loc='upper left')
plt.ylim(-5, 100)

# Graph of <lgE> in k zenith bins
nofbins = 6
bins = np.arange(5.0, 53.0+8.0, 8.0)

plt.figure()
n, bins, patches = plt.hist(zeniths_all_passed, bins=bins)
lgE_zenith_averages = []
lgE_zenith_uncert = []
lgE_bincounts = []
for i in range(nofbins):
    thislgEdata = log10_energies_all_passed[(zeniths_all_passed >= bins[i]) & (zeniths_all_passed < bins[i+1])]
    thisBinCount = len(thislgEdata)
    thisAverage = np.average(thislgEdata)
    thisUnc = np.std(thislgEdata) / np.sqrt(len(thislgEdata))
    lgE_zenith_averages.append(thisAverage)
    lgE_zenith_uncert.append(thisUnc)
    lgE_bincounts.append(thisBinCount)

binspacing = bins[1] - bins[0]
lgE_zenith_averages = np.array(lgE_zenith_averages)
lgE_zenith_uncert = np.array(lgE_zenith_uncert)
lgE_bincounts = np.array(lgE_bincounts)

plt.figure()
#plt.scatter(bins[0:-1] + 0.5 * binspacing, xmax_zenith_averages, s=100)
plt.errorbar(bins[0:-1] + 0.5 * binspacing, lgE_zenith_averages, yerr=lgE_zenith_uncert, marker='o', markersize=10, elinewidth=2, linestyle=' ', label=r'Average $lg\,E$')

for i, value in enumerate(lgE_bincounts):
    plt.annotate(str(value), (bins[i] + 0.5 * binspacing + 1.0, lgE_zenith_averages[i]))


plt.xlabel('Zenith angle [ deg ]')
plt.ylabel(r'<$lg\,E$> [ $g/cm^2$ ]')
plt.xlim(bins[0], bins[nofbins])
plt.grid()

fit, cov = np.polyfit(bins[0:-1] + 0.5 * binspacing, lgE_zenith_averages, 1, w=(1.0 / lgE_zenith_uncert), cov=True)
#fit, cov = np.polyfit(bins[0:-1] + 0.5 * binspacing, xmax_corrected_zenith_averages, 1, cov=True) # , w=(1.0 / xmax_corrected_uncert[0:5]), cov=True)

fine_theta_range = np.arange(0.0, 55.0, 0.1)
linfit_line = np.polyval(fit, fine_theta_range)

plt.plot(fine_theta_range, linfit_line, c='k', lw=2, label='Linear fit')
plt.legend(loc='best')

if not plot_publishable:
    plt.title('Average log_10(E) in %d zenith angle bins' % nofbins)
else:
    plt.savefig(os.path.join(save_plot_dir, 'log_energy_vs_zenith_6bins.pdf'))

###
doResimulate = False
###

#ldf_anna_vs_reco.ldf_anna_vs_reco(eventids_all_passed, xrecos_all_passed)
reco_vs_simulation_xmax, selection_xmax_estimate_all = conex_xmax_selection_check.conex_xmax_regions(eventids_all_passed, xrecos_all_passed, resimulate=doResimulate)

conex_outfile = open('/vol/astro2/users/acorstanje/conex_selection_xmax_resimulate_4sep2018.txt', 'w')
cPickle.dump((eventids_all_passed, xrecos_all_passed, reco_vs_simulation_xmax, selection_xmax_estimate_all), conex_outfile)
conex_outfile.close()



print 'Done.'

"""
    
    
    
sel_indices = np.where(xrecos < 10)
eventlist_no_reco = eventids[sel_indices]
eventlist_no_reco = [int(x) for x in eventlist_no_reco]

bias_passed = p_passed[sel_indices] * r_passed[sel_indices] * q_passed[sel_indices]
print '\n%d events with no combined reco: ' % len(eventlist_no_reco)
print eventlist_no_reco
print '%d events passes radio and particle bias tests' % np.sum(bias_passed)


sel_indices = np.where(xrecos_radio_only < 10)
eventlist_no_reco_radioonly = eventids[sel_indices]
eventlist_no_reco_radioonly = [int(x) for x in eventlist_no_reco_radioonly]

bias_passed = p_passed[sel_indices] * r_passed[sel_indices] * q_passed[sel_indices]
print '\n%d events with no RADIO reco: ' % len(eventlist_no_reco_radioonly)
print eventlist_no_reco_radioonly
print '%d events passes radio and particle bias tests' % np.sum(bias_passed)

for ind in sel_indices[0]:
    print 'Event %d: particle passed %d, radio passed %d, std_Xmax = %3.2f, quality stdcore = %3.2f' % (eventids[ind], p_passed[ind], r_passed[ind], sigma_x[ind], std_core[ind])



sel_indices = np.where( (xrecos_radio_only < 10) & (xrecos < 10) )
eventlist_no_reco_both = eventids[sel_indices]
eventlist_no_reco_both = [int(x) for x in eventlist_no_reco_both]

print '\n%d events with NEITHER reco: ' % len(eventlist_no_reco_both)
print eventlist_no_reco_both

# Bias check on failed events






#    event_entry = [eventid, zenith, azimuth, d_ratio, p_ratio, energy, xreco, xreco_radio_only, sigma_x, sigma_e, std_core, min_chance_of_hit, stations_with_sufficient_pulses, nfailed, particle_biastest_passed, radio_biastest_passed, quality_check_passed]
"""