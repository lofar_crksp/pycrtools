import numpy as np

import matplotlib
#matplotlib.use('Agg')

import matplotlib.pyplot as plt
plt.ion()
import glob
import cPickle
from optparse import OptionParser
import subprocess
import os
import shutil
#import atmos_corr as atm
import check_bias

process_volume = 'astro7'
coreas_base_path = '/vol/%s/lofar/sim/pipeline/events/' % process_volume

#analysisDir = '/vol/%s/lofar/sim/pipeline/sep2018_analysis_radio_only' % process_volume
#mcvsmcDir = '/vol/%s/lofar/sim/pipeline/sep2018_mcvsmc_radio_only' % process_volume
analysisDir = '/vol/%s/lofar/sim/pipeline/production_analysis_radio_only_oct2018' % process_volume
mcvsmcDir = '/vol/%s/lofar/sim/pipeline/production_mcvsmc_radio_only_oct2018' % process_volume

def getEventAnalysisFilenames(eventid, mcvsmcDir='/vol/astro7/lofar/sim/pipeline/test_mcvsmc', analysisDir='/vol/astro7/lofar/sim/pipeline/production_analysis'):
    
    recofile = "none"
    methfile = "none"
    for j in [5,4,3,2,1,0]:
        iterationSuffix = '_{0}'.format(j) if j > 0 else ''
        
        methfile = os.path.join(mcvsmcDir, 'meth{0}'.format(eventid) + iterationSuffix + '.dat')
        recofile = os.path.join(analysisDir, 'reco{0}'.format(eventid) + iterationSuffix + '.dat')
        #        if (j==0): recofile = os.path.join(analysisDir, 'reco{0}.dat'.format(ev))
        if os.path.exists(recofile) and os.path.exists(methfile):
            iteration=j
            break
    #    try:
    #print 'Going to open file %d: %s' % (i, recofile)
    if not os.path.exists(recofile) or not os.path.exists(methfile):
        print 'File not found! %s' % recofile
        return None
    else:
        return (recofile, methfile, iteration)

                              
def read_reconstruction_info(eventid, analysisDir='/vol/astro7/lofar/sim/pipeline/production_analysis', mcvsmcDir='/vol/astro7/lofar/sim/pipeline/test_mcvsmc', reco_only=False):
    (recofile, methfile, iteration) = getEventAnalysisFilenames(eventid, analysisDir=analysisDir, mcvsmcDir=mcvsmcDir)
    g=open(recofile)
    reco_info=cPickle.load(g) # contains dict
    
    if not reco_only:
        f=open(methfile)
        mcvsmc_info = cPickle.load(f) # contains tuple
    else:
        mcvsmc_info = None
    
    return (reco_info, mcvsmc_info, iteration)



def particle_bias_test(eventid, iteration, reco_info, ntrials=500, working_detector_flags=None, min_percentage_trigger=95.0, trigger_condition_nof_detectors=13):
    # Decide: compare bias test percentage down in check_bias module, or here?
    zenith = reco_info['zenith']
    azimuth = np.pi/2 - reco_info['azimuth'] + 2 * np.pi
    
    xcore = reco_info['xoff'] + reco_info['core_x']
    ycore = reco_info['yoff'] + reco_info['core_y']

    coreas_path = coreas_base_path + "{0}/{1}/coreas/".format(eventid, iteration)
    (bias_passed, min_chance_of_hit, chance_of_hit_all_sims) = check_bias.find_bias(coreas_path, zenith, azimuth, xcore, ycore, working_detectors=working_detector_flags, ntrials=ntrials, min_percentage_trigger=min_percentage_trigger, trigger_condition_nof_detectors=trigger_condition_nof_detectors)
    
    return (bias_passed, min_chance_of_hit, chance_of_hit_all_sims)

def radio_bias_test(reco_info, min_pulses_per_station=24):
    pulses_per_station = reco_info['pulses_per_station']
    pulses_per_station = np.array(pulses_per_station)
    lowest_nof_pulses = np.min(pulses_per_station)
    
    #radio_bias_passed = np.sum(pulses_per_station > min_pulses_per_station) >= 3
    
    stations_with_sufficient_pulses = np.sum(pulses_per_station > min_pulses_per_station)
    
    return (stations_with_sufficient_pulses, pulses_per_station)


def quality_check(eventid, reco_info, mcvsmc_info, plot_failed_showers=False, particle_passed=False, radio_passed=False):
    (nsimprot, nsimiron, ev, xreco, xreal, xbest, cchi2, rchi2, p_ratio, p_ratio0, p_ratio1, d_ratio, dratio, sim_tot_power, rbf, d150, nch, mr, sa, xoff, yoff, xcore, ycore, en, zen, az, noisepower, antratio, nsim, nsim_prot) = mcvsmc_info

    selection_valid_reconstructions = (xreco > 1)
    nfailed = np.sum(xreco < 1)
    if np.sum(xreco > 1) == 0:
        # no valid mcvsmc-reconstructions!??
        print 'Event %d has NO VALID mcvsmc reconstructions...???!!' % eventid
        return (999, 999, 999, 999, 999, 999)
    sel = selection_valid_reconstructions
    xmax_reco = reco_info['xmaxreco']
    sigma_e = np.std(d_ratio[sel])
    sigma_e_radio = np.std(np.sqrt(p_ratio[sel]))
    sigma_logE_radio = np.std(0.5 * np.log10(p_ratio[sel]))
    #import pdb; pdb.set_trace()
    sigma_x = np.std(xreal[sel]-xreco[sel])
    errx = np.percentile(np.abs(xreal[sel]-xreco[sel]),68)
    # (1 nov 2018: isn't this how you want to define the uncertainty? I.e. RMS deviation, not just stddev, also account for difference in mean...)
    rms_err_x = np.sqrt( np.average( (xreal[sel] - xreco[sel]).ravel()**2 ))

    # (5 nov 2018: Xmax bias, sometimes significantly nonzero)
    xmax_bias = np.median(xreal[sel] - xreco[sel])
    print 'Xmax bias (median): %3.1f g/cm2' % xmax_bias
    std_core = np.std(np.sqrt(xoff[sel]**2 + yoff[sel]**2))

    #plt.figure()
    #plt.scatter(xreal[sel], xreal[sel] - xreco[sel])
    print 'Event %d: Xmax = %4.2f, sigma_X = %4.2f, err_X = %4.2f, *rms_err_x* = %4.2f, sigma_E = %4.2f, sigma_E_radio = %4.2f, sigma_lgE_radio = %4.4f, pct_sig_E = %3.1f, sigma_core = %3.2f, nfailed = %d' % (eventid, xmax_reco, sigma_x, errx, rms_err_x, sigma_e, sigma_e_radio, sigma_logE_radio, (10**sigma_logE_radio) * 100.0 -100.0, std_core, nfailed)
    # HACK!!! Try sigma_x = rms_err_x
    sigma_x = rms_err_x
    import pdb; pdb.set_trace()
    
    if plot_failed_showers: # this is not the right place to do it... but then have to return all info
        sel_valid_showers = (np.sum(xreco, axis=1) > 1)
        sel_no_reco = (np.sum(xreco, axis=1) < 1)
        
        valid_showers = xreal[sel_valid_showers][:, 0] # 3 tests with (obviously) the same xreal
        valid_showers_rchi2 = rchi2[sel_valid_showers]
        valid_showers_rchi2 = np.average(valid_showers_rchi2, axis=1) # Average chi2 over 3 tests

        plt.figure()
        plt.scatter(valid_showers, valid_showers_rchi2, c='b', marker='o', s=80)

        showers_no_reco = xreal[sel_no_reco][:, 0]
        if len(showers_no_reco) > 0:
            showers_no_reco_rchi2 = rchi2[sel_no_reco]
            showers_no_reco_rchi2 = np.average(showers_no_reco_rchi2, axis=1)
            plt.scatter(showers_no_reco, showers_no_reco_rchi2, c='r', marker='s', s=80)
    
        plt.ylim(0.0, max(2.0, np.max(rchi2)))
        plt.xlabel('Xmax of simulated showers [g/cm2]')
        plt.ylabel('Avg. radio red. chi2 over 3 tests')
        plt.text(0.05, 0.9, "Event %d" % ev, fontsize=16, transform=plt.gca().transAxes)
        plt.text(0.05, 0.85, "Particle test %s" % particle_passed, transform=plt.gca().transAxes, color=('k' if particle_passed else 'r'))
        plt.text(0.05, 0.8, "Radio test %s" % radio_passed, transform=plt.gca().transAxes, color=('k' if radio_passed else 'r'))
        plt.text(0.05, 0.75, "sigma_core = %3.1f m" % std_core, transform=plt.gca().transAxes, color=('k' if std_core<10 else 'r'))


        #plt.title('Event %d; particle test %s, radio test %s\n3/3 failed recos in mcvsmc shown red' % (ev, particle_passed, radio_passed))
        plt.savefig('/vol/astro7/lofar/sim/pipeline/diagplots/%d-mcvsmc-failed.pdf' % ev)


    return (sigma_x, sigma_e_radio, sigma_logE_radio, xmax_bias, std_core, nfailed)



"""
def read_mcvsmc_info(eventid):

    f=open(methfile)
    nsimprot, nsimiron, ev, xreco, xreal, xbest, cchi2, rchi2, p_ratio, p_ratio0, p_ratio1, d_ratio, dratio, sim_tot_power, rbf, d150, nch, mr, sa, xoff, yoff, xcore, ycore, en, zen, az, noisepower, antratio, nsim, nsim_prot = cPickle.load(f)
    nfailed[i]=np.sum(xreco<1)
    sel=xreco>1
    stde[i] = np.std(d_ratio[sel])
    stdx[i] = np.std(xreal[sel]-xreco[sel])
    errx[i] = np.percentile(np.abs(xreal[sel]-xreco[sel]),68)
    offset[i]=np.std(np.sqrt(xoff[sel]*xoff[sel]+yoff[sel]*yoff[sel])) # comes from MC-vs-MC which always has core at (0, 0). So any offset from fit is part of the uncertainty here.
"""

np.random.seed(2018) # Fix random number seed for particle bias test


events_coreas_done = subprocess.check_output(['/usr/bin/python -u /vol/optcoma/pycrtools/src/PyCRTools/extras/get_event_ids.py --host coma00.science.ru.nl --user crdb --password crdb --dbname crdb --simulation-status=LORASIM_DONE'], shell=True)
events_coreas_done = events_coreas_done.split("\n")

events_coreas_done = [int(x) for x in events_coreas_done if len(x) > 2]

events_xmaxfit_done = subprocess.check_output(['/usr/bin/python -u /vol/optcoma/pycrtools/src/PyCRTools/extras/get_event_ids.py --host coma00.science.ru.nl --user crdb --password crdb --dbname crdb --simulation-status=XMAXFIT_DONE'], shell=True)
events_xmaxfit_done = events_xmaxfit_done.split("\n")

events_xmaxfit_done = [int(x) for x in events_xmaxfit_done if len(x) > 2]

eventlist = events_xmaxfit_done

# HACK
#eventlist = events_coreas_done + events_xmaxfit_done

# HACK
#eventlist = [52168749, 60016870, 61211451, 66061507, 73283548, 80357305, 82432517, 94175691, 95149835, 123291367, 125928679, 126181559, 158062679, 162192921, 173797298, 175494398, 177113844, 177302047, 188320127, 196178017, 196782042, 198490827]


#eventlist = events_coreas_done

#eventlist_file = '/vol/astro3/lofar/sim/pipeline/eventlist_Nature.txt'
#eventlist_file = '/home/acorstanje/eventlist_DRYRUN_simulated_30nov2017.txt'
eventlist_file = '/home/acorstanje/eventlist_xmaxfit_done_701.txt'
eventlist_file = '/vol/astro2/users/acorstanje/xmax_statistics_fullset_passed_211.dat'
with open(eventlist_file) as f:
    events_Nature = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
events_Nature = [int(x) for x in events_Nature if len(x) > 2]

eventlist = events_Nature

"""eventlist_file = '/home/acorstanje/eventlist_DRYRUN_simulated_30nov2017.txt'
with open(eventlist_file) as f:
    events_already_done = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
events_already_done = [int(x) for x in events_already_done if len(x) > 2]
"""
#import pdb; pdb.set_trace()
# HACK
#eventlist = [id for id in events_xmaxfit_done if id not in events_Nature]
#eventlist = [id for id in events_Nature if id not in events_already_done]



"""
#eventlist_file = '/vol/astro2/users/acorstanje/events_3_stations.txt'
eventlist_file = '/vol/astro3/lofar/sim/pipeline/eventlist_Nature.txt'
with open(eventlist_file) as f:
    events_Nature = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
events_Nature = [int(x) for x in events_Nature if len(x) > 2]

#eventlist = [int(x) for x in events if int(x) in events_coreas_done]
# Events NOT in Nature set:
eventlist = [int(x) for x in events_coreas_done if int(x) not in events_Nature]


# Remove events in status GDAS_BUG
#eventlist = [int(x) for x in events if int(x) not in events_gdas_bug]
"""

#eventlist = [60016870, 80289586, 80365751, 123291367, 130704017, 168608146, 173389761, 175494398, 188181226, 198490827, 207606132] # failed reco combined 29 nov 2017

#eventlist = [123291367, 188181226, 198490827, 207606132]

#eventlist = [60016870, 95021378, 161466556]

#eventlist = [ 86122409, 183980193, 200724160]
eventlist.sort()

#eventlist = [82429699]
#eventlist = [160498387]
#eventlist = [177113884] # No mcvsmc??
#eventlist = [237934158]

#eventlist = eventlist[0:40]
l=len(eventlist)

#eventlist = np.array([60398398, 98812643])
#eventlist = [92380604, 81409140]
#print eventlist
print 'Going to process %d events' % l

#eventlist = [67362765]

# Check events on particle bias, radio bias and reconstruction quality
nof_passed = 0
nof_failed = 0
particle_nof_failed = 0
radio_nof_failed = 0
quality_nof_failed = 0
events_passed_all = []
events_failed_reco = []
events_failed_biastests = []
events_filenotfound = []
for i, eventid in enumerate(eventlist):
    print 'Processing event %d / %d, id = %d' % (i+1, len(eventlist), eventid)
    try:
        (reco_info_radio_only, mcvsmc_info_radio_only, iteration) = read_reconstruction_info(eventid, analysisDir=analysisDir, mcvsmcDir=mcvsmcDir)
    except:
        print 'Skipped event %d due to missing radio only mc-vs-mc' % eventid
        events_filenotfound.append(eventid)
        continue
    """
    try:
        (reco_info, mcvsmc_info, iteration) = read_reconstruction_info(eventid) # combined fit
    except:
        print 'Skipped event %d due to missing combined fit' % eventid
        events_filenotfound.append(eventid)
        continue
    """
    reco_info = reco_info_radio_only
    mcvsmc_info = mcvsmc_info_radio_only

    (nsimprot, nsimiron, ev, xreco, xreal, xbest, cchi2, rchi2, p_ratio, p_ratio0, p_ratio1, d_ratio, dratio, sim_tot_power, rbf, d150, nch, mr, sa, xoff, yoff, xcore, ycore, en, zen, az, noisepower, antratio, nsim, nsim_prot) = mcvsmc_info # Combined

    print 'Combined fit analysis & mcvsmc'
    (sigma_x_combined, sigma_e_combined, sigma_logE_radio, xmax_bias, std_core_combined, nfailed) = quality_check(eventid, reco_info, mcvsmc_info)
    
    (nsimprot, nsimiron, ev, xreco, xreal, xbest, cchi2_radio_only, rchi2_radio_only, p_ratio, p_ratio0, p_ratio1, d_ratio, dratio, sim_tot_power, rbf, d150, nch, mr, sa, xoff, yoff, xcore, ycore, en, zen, az, noisepower, antratio, nsim, nsim_prot) = mcvsmc_info_radio_only # Radio only
        #import pdb; pdb.set_trace()
    
    # Quick test by disabling LORA station 2
    working_detector_flags = np.ones(20)
    #working_detector_flags[4:8] *= 0.0
    
    (particle_biastest_passed, min_chance_of_hit, chance_of_hit_all_sims) = particle_bias_test(eventid, iteration, reco_info, min_percentage_trigger=99.0, trigger_condition_nof_detectors=13, working_detector_flags=working_detector_flags)
    ### Changed min percentage to 99.0 % (was 95 / default), 5 sep 2018
    
    (stations_with_sufficient_pulses, pulses_per_station) = radio_bias_test(reco_info)
    radio_biastest_passed = (stations_with_sufficient_pulses >= 3)

    print 'Radio-only fit analysis & mcvsmc'
    (sigma_x, sigma_e, sigma_logE_radio, xmax_bias, std_core, nfailed) = quality_check(eventid, reco_info_radio_only, mcvsmc_info_radio_only, plot_failed_showers=False, particle_passed=particle_biastest_passed, radio_passed=radio_biastest_passed)
    # Set plot_failed_showers to False (4 mrt 2019)
    # Hack: want to find out quality criterium later
    quality_check_passed = True # (std_core <= 5.0)

    if not particle_biastest_passed: particle_nof_failed += 1
    if not radio_biastest_passed: radio_nof_failed += 1
    if not quality_check_passed: quality_nof_failed += 1

    zenith = reco_info['zenith']
    azimuth = reco_info['azimuth']
    d_ratio = reco_info['d_ratio']
    p_ratio = reco_info['p_ratio']
    energy = reco_info['energy'] * 1.0e9 # * d_ratio * 1.0e9
    # NB !! No d_ratio multiplication as the value is not trusted (yet? as per March 29, 2018)
    
    #npulses=info['pulses_per_station']
    #npulses=np.array(npulses)
    xreco = reco_info['xmaxreco']
    xreco_radio_only = reco_info_radio_only['xmaxreco']

#    print 'Event %d: Xmax = %3.2f, E_reco = %1.2e;' % (eventid, xreco, energy)
#    print 'Combined fit   sigma_X = %3.2f, sigma_E = %1.2e, std_core = %3.2f' % (sigma_x_combined, sigma_e_combined, std_core_combined)
#    print 'Radio-only fit sigma_X = %3.2f, sigma_E = %1.2e, std_core = %3.2f' % (sigma_x, sigma_e, std_core)

    comb_chi2 = reco_info['combchi2']
    radio_chi2 = reco_info['radiochi2']
                           
    event_entry = [eventid, zenith, azimuth, d_ratio, p_ratio, energy, xreco, xreco_radio_only, sigma_x, sigma_e, sigma_logE_radio, std_core, sigma_x_combined, sigma_e_combined, std_core_combined, min_chance_of_hit, stations_with_sufficient_pulses, nfailed, particle_biastest_passed, radio_biastest_passed, quality_check_passed, comb_chi2, radio_chi2]

    print 'Event %d: combined chi2: %2.3f, radio chi2: %2.3f' % (eventid, comb_chi2, radio_chi2)
    print 'Event %d: particle %s, radio %s, quality %s' % (eventid, particle_biastest_passed, radio_biastest_passed, quality_check_passed)

    valid_reco = (xreco > 10) #and (xreco_radio_only > 10)
    
    verdict = particle_biastest_passed and radio_biastest_passed and quality_check_passed
    if not verdict:
        nof_failed += 1
        events_failed_biastests.append(event_entry)
        continue
    
    if not valid_reco:
        print 'Event %d has NO valid reconstruction!' % eventid
        events_failed_reco.append(event_entry)

    if verdict and valid_reco:
        events_passed_all.append(event_entry)
        nof_passed += 1
        print 'Event %d passed all criteria' % eventid

events_passed_all = np.array(events_passed_all)
events_failed_reco = np.array(events_failed_reco)
events_failed_biastests = np.array(events_failed_biastests)

outputDict = dict(events_passed_all = events_passed_all, events_failed_reco=events_failed_reco, events_failed_biastests=events_failed_biastests)



print 'Eventlist processed with %d events' % len(eventlist)
print 'Nof failed: %d' % nof_failed
print 'Nof passed: %d' % nof_passed
print 'No valid reco: %d' % len(events_failed_reco)
print ' '
print 'Particle test failed  : %d' % particle_nof_failed
print 'Radio bias test failed: %d' % radio_nof_failed
print 'Quality check failed  : %d' % quality_nof_failed

print ' '
print 'Events with analysis file not found:'
print events_filenotfound
#outfile = open("/home/acorstanje/xmax_statistics_final_99_0pct_12nov2018_noLORAstation2_16_16.dat", "w")
cPickle.dump(outputDict, outfile)
outfile.close()



"""










stde=np.zeros([l])
stdx=np.zeros([l])
errx=np.zeros([l])
radiochi2 = np.zeros([l])
combinedchi2 = np.zeros([l])
offset=np.zeros([l])
nfailed=np.zeros([l])
xreco11=np.zeros([l])
xcorrected11=np.zeros([l])
zenith=np.zeros([l])
azimuth=np.zeros([l])
energy=np.zeros([l])
p_ratio11=np.zeros([l])
d_ratio11=np.zeros([l])
no_lora_bias=np.zeros([l],dtype=bool)

sel1=np.zeros([l],dtype=bool)
sel2=np.zeros([l],dtype=bool)
xcore = np.zeros(l)
ycore = np.zeros(l)

coreas_base_path = '/vol/astro7/lofar/sim/pipeline/events/'
eventlist_bias_failed = []
eventlist_bias_passed = []
nof_failed = 0
eventlist_processed = []
reco_energies = []
bias_passed_all_processed = []
for i in np.arange(l):
    ev=eventlist[i]
    it=-1
    methfile="none"
    recofile="none"
    for j in [2,1,0]:
        iterationSuffix = '_{0}'.format(j) if j > 0 else ''
        
        methfile = os.path.join(mcvsmcDir, 'meth{0}'.format(ev)+iterationSuffix+'.dat')
        recofile = os.path.join(analysisDir, 'reco{0}'.format(ev)+iterationSuffix+'.dat')
        #        if (j==0): recofile = os.path.join(analysisDir, 'reco{0}.dat'.format(ev))
        if os.path.exists(recofile):
            it=j
            break
    #    try:
    print 'Going to open file %d: %s' % (i, recofile)
    if not os.path.exists(recofile):
        print 'File not found! %s' % recofile
        continue
    #    f=open(methfile)
    #    nsimprot, nsimiron, ev, xreco, xreal, xbest, cchi2, rchi2, p_ratio, p_ratio0, p_ratio1, d_ratio, dratio, sim_tot_power, rbf, d150, nch, mr, sa, xoff, yoff, xcore, ycore, en, zen, az, noisepower, antratio, nsim, nsim_prot = cPickle.load(f)
    #nfailed[i]=np.sum(xreco<1)
    #sel=xreco>1
    #stde[i] = np.std(d_ratio[sel])
    #stdx[i] = np.std(xreal[sel]-xreco[sel])
    #errx[i] = np.percentile(np.abs(xreal[sel]-xreco[sel]),68)
    #offset[i]=np.std(np.sqrt(xoff[sel]*xoff[sel]+yoff[sel]*yoff[sel]))

    eventlist_processed.append(ev)
    g=open(recofile)
    info=cPickle.load(g)
    npulses=info['pulses_per_station']
    npulses=np.array(npulses)
    xreco11[i]=info['xmaxreco']
    d_ratio11[i]=info['d_ratio']
    p_ratio11[i]=info['p_ratio']
    energy[i]=info['energy']*d_ratio11[i]
    ldb=info['lora_dens_best']
    ldw=info['lora_dens_worst']

radiochi2[i] = info['radiochi2']
    combinedchi2[i] = info['combchi2']
    if ev == 62839779:
        import pdb; pdb.set_trace()
station_list = info['station_name']
    pulses_per_station = info['pulses_per_station']
    pulses_per_station = np.array(pulses_per_station)
    lowest_nof_pulses = np.min(pulses_per_station)
    
    radio_bias_passed = np.sum(pulses_per_station > 24) >= 3
    bias_passed_all_processed.append(radio_bias_passed)
    reco_energies.append(1.0e9 * energy[i])
    print 'Event %d: lowest number of pulses %d; nof stations > 24 pulses: %d; radio bias passed: %s' % (ev, lowest_nof_pulses, np.sum(pulses_per_station > 24), radio_bias_passed)
    
    if radio_bias_passed:
        eventlist_bias_passed.append(ev)
    else:
        eventlist_bias_failed.append(ev)
        nof_failed += 1

"""